package com.onefam.gcm;


import android.app.Activity;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.onefam.R;
import com.onefam.activitys.FamilyActivity;
import com.onefam.activitys.FamilyTreeActivity;
import com.onefam.activitys.ProfileActivity;
import com.onefam.applications.MyApplication;

import java.util.Random;

import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";
    //    int count =0;
    SharedPreferences sp;

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("message");
        Log.d(TAG, "From: " + from);
        String nodeID = data.getString("mediaId");
        Log.d(TAG, "Message: " + message);
        sp = getSharedPreferences("OneFam", 0);

        SharedPreferences.Editor edit = sp.edit();
        edit.putInt("Badger", sp.getInt("Badger", 0) + 1);
        edit.commit();
//        if (sp.getInt("Badger",0)==0)
//        {
//
//        }



        ActivityManager am = (ActivityManager) getApplicationContext().
                getSystemService(Activity.ACTIVITY_SERVICE);

        if (!am.getRunningTasks(1).get(0).topActivity.getClassName().equalsIgnoreCase("com.onefam.activitys.NotificatioActivity")) {
            if (FamilyTreeActivity.FamilyTreeActivityObj != null) {
                FamilyTreeActivity.FamilyTreeActivityObj.changeNotificationImage();
                MyApplication.showUnreadNotificationStatus(true);
            }
        }


        ShortcutBadger.applyCount(getApplicationContext(), sp.getInt("Badger", 0));


        if (from.startsWith("/topics/")) {
            // message received from some topic.
        } else {
            // normal downstream message.
        }

        // [START_EXCLUDE]
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */
        sendNotification(message, nodeID);
        // [END_EXCLUDE]
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(String message, String id) {
        Intent intent = new Intent(this, ProfileActivity.class);//new Intent(this, MainActivity.class);
        intent.putExtra("id", id);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis() /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);



        Notification.Builder notificationBuilder = new Notification.Builder(this)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(message)
                .setStyle(new Notification.BigTextStyle().bigText(message))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);


        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.drawable.ic_stat_notif);
            notificationBuilder.setColor(this.getResources().getColor(R.color.board_blue_color));
        } else {
            notificationBuilder.setSmallIcon(R.drawable.icon);
        }


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Random random = new Random();
        int m = random.nextInt(9999 - 1000) +
                1000;
        notificationManager.notify(m /* ID of notification */, notificationBuilder.build());
    }
}
