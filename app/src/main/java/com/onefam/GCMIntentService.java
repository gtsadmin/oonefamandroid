package com.onefam;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;


import com.google.android.gcm.GCMBaseIntentService;
import com.onefam.activitys.ProfileActivity;
import com.onefam.applications.MyApplication;

import java.util.Date;
import java.util.Random;


public class GCMIntentService extends GCMBaseIntentService {
	SharedPreferences file;
	SharedPreferences.Editor edit;
	String content="";
	public GCMIntentService() {
		super(MyApplication.GOOGLE_SENDER_ID);
	}

	Handler handler = new Handler() {
		public void handleMessage(Message msg) {

			if (msg.what == 1) {

			}}};
	private NotificationManager mNotificationManager;

	// private String tag;

	@Override
	protected void onError(Context arg0, String arg1) {

		try {
			Log.d("error", "gcm intent call error " + arg1);
			Message msg = Message.obtain();
			msg.what = 1;
			msg.obj = arg1;
			handler.sendMessage(msg);

		} catch (Exception exp) {

			Log.d("sumit", exp.getMessage(), exp);

			exp.printStackTrace();
		}

	}

	@Override
	protected void onMessage(Context arg0, Intent intent) {

		try {

			String action = intent.getAction();

			Log.d("sumit", action + " on mesage");
			Bundle extras = intent.getExtras();

			String message = intent.getExtras().getString("message");
//			String message = intent.getExtras().getString("message");
//			String message = intent.getExtras().getString("message");
//			if ("com.google.android.c2dm.intent.RECEIVE".equals(action)) {
//				String message = intent.getStringExtra("message");
////				 content = intent.getStringExtra("content");
//				String actionatr = intent.getStringExtra("action");

				System.out.println("message"+message);
			//	if (MyApplication.getInstance().myNotifictoin().equalsIgnoreCase("Yes")||MyApplication.getInstance().myNotifictoin().equalsIgnoreCase("")) {
					notification(message, arg0);
			//	}

//			}

		} catch (Exception exp) {
			Log.e("gcm", exp.getMessage(), exp);
			exp.printStackTrace();
		}

	}

	@Override
	protected void onRegistered(Context arg0, String registrationId) {
		try {
		} catch (Exception exp) {
			// Log.e("gcm", exp.getMessage(), exp);
			exp.printStackTrace();
		}

	}

	@Override
	protected void onUnregistered(Context arg0, String arg1) {

	}

	@SuppressWarnings({ "deprecation", "deprecation", "deprecation",
			"deprecation" })
	private void notification(String message, Context arg0) {
		try {


			mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

			int icon = R.mipmap.ic_launcher;
			long when = new Date().getTime();

			Notification notification = new Notification(icon, message, when);

			// notification.defaults = Notification.DEFAULT_SOUND
			// | Notification.DEFAULT_VIBRATE;

			notification.defaults |= Notification.DEFAULT_VIBRATE;
			// notification.sound= Uri.parse("android.resource://" +
			// getPackageName() + "/" + R.raw.futuresound);

			notification.flags |= Notification.FLAG_AUTO_CANCEL;
			notification.defaults = Notification.DEFAULT_ALL;
			PendingIntent contentIntent = null;

			Log.d("hii", message);
			// if(login_str.equalsIgnoreCase("login"))
			// {
//			 if (MyApplication.myMobileno().equalsIgnoreCase("na") || MyApplication.myMobileno().length() < 3) {
//				MyApplication.getInstance().sendPhonenumber(arg0);
//
//			}else {

				 Intent i = new Intent(getApplicationContext(), ProfileActivity.class).putExtra("gcm","gcm");
				 contentIntent = PendingIntent.getActivity(getApplicationContext(),
						 (int) System.currentTimeMillis(), i,
						 PendingIntent.FLAG_UPDATE_CURRENT);
//			 }



			notification.setLatestEventInfo(this, "OneFam",
					message, contentIntent);

			Random random = new Random();
			int m = random.nextInt(9999 - 1000) +
					    1000;

			mNotificationManager.notify(m, notification);

		} catch (Exception exp) {

			exp.printStackTrace();

		}
	}

}
