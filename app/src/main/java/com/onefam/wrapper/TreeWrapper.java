package com.onefam.wrapper;

import java.io.Serializable;

/**
 * Created by suarebits on 12/1/16.
 */
public class TreeWrapper implements Serializable{
    String TeamId ="";
    String level="";
    String ParentTeam="";
    String NodeId="";
    String Name="";
    String Gender="";
    String ImageUrl="";
    String Relation="";
    String DOB="";
    String relationLevel="";
    String CanDelete ="";

    public String getCanInvite() {
        return CanInvite;
    }

    public void setCanInvite(String canInvite) {
        CanInvite = canInvite;
    }

    String CanInvite="";
    public String getCanDelete() {
        return CanDelete;
    }

    public void setCanDelete(String canDelete) {
        CanDelete = canDelete;
    }



    public String getRelationLevel() {
        return relationLevel;
    }

    public void setRelationLevel(String relationLevel) {
        this.relationLevel = relationLevel;
    }
    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    boolean isSelected = false;

    public String getBirthAddress() {
        return BirthAddress;
    }

    public void setBirthAddress(String birthAddress) {
        BirthAddress = birthAddress;
    }

    public String getNodeId() {
        return NodeId;
    }

    public void setNodeId(String nodeId) {
        NodeId = nodeId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getRelation() {
        return Relation;
    }

    public void setRelation(String relation) {
        Relation = relation;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    String BirthAddress="";

    public String getTeamId() {
        return TeamId;
    }

    public void setTeamId(String teamId) {
        TeamId = teamId;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getParentTeam() {
        return ParentTeam;
    }

    public void setParentTeam(String parentTeam) {
        ParentTeam = parentTeam;
    }

}
