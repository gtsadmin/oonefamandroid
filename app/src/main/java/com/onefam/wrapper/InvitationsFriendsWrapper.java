package com.onefam.wrapper;

/**
 * Created by nitin on 20/10/16.
 */
public class InvitationsFriendsWrapper {
    String NodeId = "", Name = "", Image = "", Gender = "", Email = "", IsInvited = "", InvitationSentOn = "", Relation = "", Father = "", Mother = "", Brother = "", Sister = "", Husband = "";

    public String getToNodeId() {
        return ToNodeId;
    }

    public void setToNodeId(String toNodeId) {
        ToNodeId = toNodeId;
    }

    String ToNodeId="";
    public String getNodeId() {
        return NodeId;
    }

    public void setNodeId(String nodeId) {
        NodeId = nodeId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getIsInvited() {
        return IsInvited;
    }

    public void setIsInvited(String isInvited) {
        IsInvited = isInvited;
    }

    public String getInvitationSentOn() {
        return InvitationSentOn;
    }

    public void setInvitationSentOn(String invitationSentOn) {
        InvitationSentOn = invitationSentOn;
    }

    public String getRelation() {
        return Relation;
    }

    public void setRelation(String relation) {
        Relation = relation;
    }

    public String getFather() {
        return Father;
    }

    public void setFather(String father) {
        Father = father;
    }

    public String getMother() {
        return Mother;
    }

    public void setMother(String mother) {
        Mother = mother;
    }

    public String getBrother() {
        return Brother;
    }

    public void setBrother(String brother) {
        Brother = brother;
    }

    public String getSister() {
        return Sister;
    }

    public void setSister(String sister) {
        Sister = sister;
    }

    public String getHusband() {
        return Husband;
    }

    public void setHusband(String husband) {
        Husband = husband;
    }
}
