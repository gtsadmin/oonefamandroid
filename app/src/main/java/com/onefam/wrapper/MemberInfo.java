package com.onefam.wrapper;

import java.util.ArrayList;
import java.util.HashMap;


public class MemberInfo {
    String NodeId="",FirstName="",LastName="",EmailId="",DateOfBirth="",BirthPlace="",IsAlive="",DateOfDeath="",DeathPlace="",
            DeathCause="",SelectedFather="",SelectedMother="",SelectedSpouse="";

    ArrayList<String> fathersArrayList = new ArrayList<>();
    ArrayList<String> mothersArrayList = new ArrayList<>();
    ArrayList<String> spousesArrayList = new ArrayList<>();
    HashMap<String,String> fathersHashMap = new HashMap<>();
    HashMap<String,String> mothersHashMap = new HashMap<>();
    HashMap<String,String> spousesHashMap = new HashMap<>();

    public ArrayList<String> getFathersArrayList() {
        return fathersArrayList;
    }

    public void setFathersArrayList(ArrayList<String> fathersArrayList) {
        this.fathersArrayList = fathersArrayList;
    }

    public ArrayList<String> getMothersArrayList() {
        return mothersArrayList;
    }

    public void setMothersArrayList(ArrayList<String> mothersArrayList) {
        this.mothersArrayList = mothersArrayList;
    }

    public ArrayList<String> getSpousesArrayList() {
        return spousesArrayList;
    }

    public void setSpousesArrayList(ArrayList<String> spousesArrayList) {
        this.spousesArrayList = spousesArrayList;
    }

    public HashMap<String, String> getFathersHashMap() {
        return fathersHashMap;
    }

    public void setFathersHashMap(HashMap<String, String> fathersHashMap) {
        this.fathersHashMap = fathersHashMap;
    }

    public HashMap<String, String> getMothersHashMap() {
        return mothersHashMap;
    }

    public void setMothersHashMap(HashMap<String, String> mothersHashMap) {
        this.mothersHashMap = mothersHashMap;
    }

    public HashMap<String, String> getSpousesHashMap() {
        return spousesHashMap;
    }

    public void setSpousesHashMap(HashMap<String, String> spousesHashMap) {
        this.spousesHashMap = spousesHashMap;
    }

    public String getNodeId() {
        return NodeId;
    }

    public void setNodeId(String nodeId) {
        NodeId = nodeId;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getEmailId() {
        return EmailId;
    }

    public void setEmailId(String emailId) {
        EmailId = emailId;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        DateOfBirth = dateOfBirth;
    }

    public String getBirthPlace() {
        return BirthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        BirthPlace = birthPlace;
    }

    public String getIsAlive() {
        return IsAlive;
    }

    public void setIsAlive(String isAlive) {
        IsAlive = isAlive;
    }

    public String getDateOfDeath() {
        return DateOfDeath;
    }

    public void setDateOfDeath(String dateOfDeath) {
        DateOfDeath = dateOfDeath;
    }

    public String getDeathPlace() {
        return DeathPlace;
    }

    public void setDeathPlace(String deathPlace) {
        DeathPlace = deathPlace;
    }

    public String getDeathCause() {
        return DeathCause;
    }

    public void setDeathCause(String deathCause) {
        DeathCause = deathCause;
    }

    public String getSelectedFather() {
        return SelectedFather;
    }

    public void setSelectedFather(String selectedFather) {
        SelectedFather = selectedFather;
    }

    public String getSelectedMother() {
        return SelectedMother;
    }

    public void setSelectedMother(String selectedMother) {
        SelectedMother = selectedMother;
    }

    public String getSelectedSpouse() {
        return SelectedSpouse;
    }

    public void setSelectedSpouse(String selectedSpouse) {
        SelectedSpouse = selectedSpouse;
    }
}
