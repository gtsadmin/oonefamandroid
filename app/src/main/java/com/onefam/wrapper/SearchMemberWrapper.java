package com.onefam.wrapper;


public class SearchMemberWrapper {
   String NodeId="";
    String Name="";
    String Image="";

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    String date="";

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    String gender="";

    public String getNodeId() {
        return NodeId;
    }

    public void setNodeId(String nodeId) {
        NodeId = nodeId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }
}
