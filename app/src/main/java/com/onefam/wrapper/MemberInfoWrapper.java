package com.onefam.wrapper;

/**
 * Created by suarebits on 11/1/16.
 */
public class MemberInfoWrapper {
    String NodeId="",Name="",Gender="",ImageUrl="",Relation="",DOB="",BirthAddress="";

    public String getNodeId() {
        return NodeId;
    }

    public void setNodeId(String nodeId) {
        NodeId = nodeId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getRelation() {
        return Relation;
    }

    public void setRelation(String relation) {
        Relation = relation;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getBirthAddress() {
        return BirthAddress;
    }

    public void setBirthAddress(String birthAddress) {
        BirthAddress = birthAddress;
    }
}
