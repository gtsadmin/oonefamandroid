package com.onefam.wrapper;

import java.util.ArrayList;

/**
 * Created by suarebits on 8/12/15.
 */
public class RelativeWrapper {
    MemberWrapper memberWrapper;
    int relation_type;

    public int getGender() {
        return gender;
    }

    public ArrayList<MemberWrapper> getMemberWrappers() {
        return memberWrappers;
    }

    public void setMemberWrappers(ArrayList<MemberWrapper> memberWrappers) {
        this.memberWrappers = memberWrappers;
    }

    ArrayList<MemberWrapper> memberWrappers = new ArrayList<>();

    public void setGender(int gender) {
        this.gender = gender;
    }

    int gender;

    public int getHierarchy_relation() {
        return hierarchy_relation;
    }

    public void setHierarchy_relation(int hierarchy_relation) {
        this.hierarchy_relation = hierarchy_relation;
    }

    int hierarchy_relation;

    public MemberWrapper getMemberWrapper() {
        return memberWrapper;
    }

    public void setMemberWrapper(MemberWrapper memberWrapper) {
        this.memberWrapper = memberWrapper;
    }

    public int getRelation_type() {
        return relation_type;
    }

    public void setRelation_type(int relation_type) {
        this.relation_type = relation_type;
    }
}
