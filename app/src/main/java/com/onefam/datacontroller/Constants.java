package com.onefam.datacontroller;

public class Constants {
    public static final int FATHER = 101;
    public static final int MOTHER = 102;
    public static final int BROTHER = 103;
    public static final int SISTER = 104;
    public static final int SON = 105;
    public static final int DAUGHTER = 106;
    public static final int PARENT = 107;
    public static final int PARALLEL = 108;
    public static final int CHILD = 109;
    public static final int WIFE = 110;
    public static final int HUSBAND =111;
    public static final int MYSALF = 112;

    public static final int FEMALE =0;
    public static final int MALE =1;
    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";

    public static final int PHOTOS =0;
    public static final int VIDEOS =1;
    public static final int UPLOADPHOTO =00;
    public static final int UPLOADVIDEOS =01;
    public static final int UPLOADAUDIO = 02;


    public static String USERID ="USERID";
    public static String EMAILID ="EMAILID";
    public static String PASSWORD ="PASSWORD";
    public static String LANGUAGECODE ="lcode";
    public static String GCMTOKEN ="token";
    public static String PUSHKEY="key";
    public static String FIRSTNAME ="FIRSTNAME";
    public static String UNREADNOTIFICATION="unreadnotification";
    public static String VIEW_FAMILY_ID="view_family_id";

    public static int FINISHCODE = 100;

    public static int PHOTO =3;
    public static int VIDEO =2;
    public static int AUDIO =1;
    public static int DOC =4;
    public static int ALBUM =5;



    public static final int REQUEST_CODE_GALLERY = 0x51;
    public static final int REQUEST_CODE_CROP_IMAGE = 0x53;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x52;
    public static final int REQUEST_VIDEO_CAPTURE = 1;
    public static final int REQUEST_VIDEO_GALLERY=2;
    public static final int REQUEST_AUDIO_GALLERY=3;
    public static final int REQUEST_AUDIO_RECORDING=4;

    public static final int MEDIA_TYPE_VIDEO = 2;

    public static final int PROFILEACTIVITYCODE = 1;
    public static final int ALLPREVIEWACTIVITYCODE =2;



}
