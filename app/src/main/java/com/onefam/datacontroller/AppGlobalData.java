package com.onefam.datacontroller;

import com.onefam.wrapper.MemberInfoWrapper;
import com.onefam.wrapper.TreeWrapper;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by suarebits on 11/1/16.
 */
public class AppGlobalData {
    public static AppGlobalData appGlobalData = null;
    public static Boolean reloadwebViewBoolean = false;
    public static Boolean reloadfamilydataBoolean = false;
    public static Boolean editmemeberBoolean = false;
    public static Boolean getEventBoolean = false;
    ArrayList<String> eventList = new ArrayList<>();
    HashMap<String, String> eventlistMap = new HashMap<>();
    HashMap<String, String> membernamelistMap = new HashMap<>();
    ArrayList<String> albumList = new ArrayList<>();
    HashMap<String, String> albumlistMap = new HashMap<>();
    public static Boolean call_familyBln = true;

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    String languageCode = "";

    public ArrayList<String> getAlbumList() {
        return albumList;
    }

    public void setAlbumList(ArrayList<String> albumList) {
        this.albumList = albumList;
    }

    public HashMap<String, String> getAlbumlistMap() {
        return albumlistMap;
    }

    public void setAlbumlistMap(HashMap<String, String> albumlistMap) {
        this.albumlistMap = albumlistMap;
    }

    public ArrayList<TreeWrapper> getTreeWrapperArrayList() {
        return treeWrapperArrayList;
    }

    public void setTreeWrapperArrayList(ArrayList<TreeWrapper> treeWrapperArrayList) {
        this.treeWrapperArrayList = treeWrapperArrayList;
    }

    ArrayList<TreeWrapper> treeWrapperArrayList = new ArrayList<>();

    public ArrayList<String> getEventList() {
        return eventList;
    }

    public void setEventList(ArrayList<String> eventList) {
        this.eventList = eventList;
    }

    public HashMap<String, String> getEventlistMap() {
        return eventlistMap;
    }

    public void setEventlistMap(HashMap<String, String> eventlistMap) {
        this.eventlistMap = eventlistMap;
    }

    public HashMap<String, String> getMembernamelistMap() {
        return membernamelistMap;
    }

    public void setMembernamelistMap(HashMap<String, String> membernamelistMap) {
        this.membernamelistMap = membernamelistMap;
    }

    HashMap<String, String> mamberListMap = new HashMap<>();
    ArrayList<String> memberList = new ArrayList<>();

    public ArrayList<MemberInfoWrapper> getMemberInfoWrappers() {
        return memberInfoWrappers;
    }

    public void setMemberInfoWrappers(ArrayList<MemberInfoWrapper> memberInfoWrappers) {
        this.memberInfoWrappers = memberInfoWrappers;
    }

    ArrayList<MemberInfoWrapper> memberInfoWrappers = new ArrayList<>();

    public HashMap<String, String> getMamberListMap() {
        return mamberListMap;
    }

    public void setMamberListMap(HashMap<String, String> mamberListMap) {
        this.mamberListMap = mamberListMap;
    }

    public ArrayList<String> getMemberList() {
        return memberList;
    }

    public void setMemberList(ArrayList<String> memberList) {
        this.memberList = memberList;
    }


    ArrayList<String> allMemberList = new ArrayList<>();

    public ArrayList<String> getAllMemberList() {
        return allMemberList;
    }

    public void setAllMemberList(ArrayList<String> allMemberList) {
        this.allMemberList = allMemberList;
    }


    ArrayList<String> allMemberListNodeId = new ArrayList<>();
    public ArrayList<String> getAllMemberNodeIdList() {
        return allMemberListNodeId;
    }

    public void setAllMemberNodeIdList(ArrayList<String> allMemberListNodeId) {
        this.allMemberListNodeId = allMemberListNodeId;
    }


    ArrayList<TreeWrapper> tagMemberArrayList = new ArrayList<>();

    public ArrayList<TreeWrapper> getTagMemberArrayList() {
        return tagMemberArrayList;
    }

    public void setTagMemberArrayList(ArrayList<TreeWrapper> tagMemberArrayList) {
        this.tagMemberArrayList = tagMemberArrayList;
    }

    public static AppGlobalData getInstance() {
        if (appGlobalData == null) {
            appGlobalData = new AppGlobalData();

        }
        return appGlobalData;
    }

}
