package com.onefam.activitys;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.onefam.R;
import com.onefam.applications.MyApplication;
import com.onefam.datacontroller.AppGlobalData;
import com.onefam.views.CircularImageView;
import com.onefam.views.EditTextProximaNovaRegular;
import com.onefam.views.TextViewProximaNovaRegular;
import com.onefam.webutility.ParseData;
import com.onefam.webutility.TaskCode;
import com.onefam.webutility.Urls;
import com.onefam.webutility.WebTask;
import com.onefam.webutility.WebTaskComplete;
import com.onefam.wrapper.InvitationsFriendsWrapper;


import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InvitationsFriendsActivity extends AppCompatActivity implements WebTaskComplete {
    @Bind(R.id.backLayout)
    LinearLayout backLayout;
    @Bind(R.id.topbarLayout)
    RelativeLayout topbarLayout;
    @Bind(R.id.familyList)
    ListView familyList;
    @Bind(R.id.addmemberLayout)
    LinearLayout addmemberLayout;
    FamilyAdapter familyAdapter;
    @Bind(R.id.listView)
    SwipeMenuListView listView;
    ArrayList<InvitationsFriendsWrapper> invitationsFriendsWrappers;
    int removePos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.invitations_friends_layout);
        ButterKnife.bind(this);
        languageCode(MyApplication.getLanguagecode());
        MyApplication.getInstance().setLoad_profile(false);
        if (MyApplication.isInternetWorking(InvitationsFriendsActivity.this)) {
            String url = Urls.base_Url + "apiInvitees.ashx?LoginId=" + MyApplication.getUserID();
            new WebTask(InvitationsFriendsActivity.this, url, null, this, TaskCode.GETINVITATIONS).execute();
        }
    }

    @OnClick(R.id.backLayout)
    public void backLayout() {
        onBackPressed();
    }

    @OnClick(R.id.addmemberLayout)
    public void addmemberLayout() {
        MyApplication.getInstance().produceAnimation(addmemberLayout);
        startActivity(new Intent(InvitationsFriendsActivity.this, AddMember.class));
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onComplete(String response, int taskcode) {
        if (response != null && taskcode == TaskCode.GETINVITATIONS) {
            invitationsFriendsWrappers = new ParseData(InvitationsFriendsActivity.this, response, taskcode).getInvitationFriend();
            familyAdapter = new FamilyAdapter(InvitationsFriendsActivity.this, invitationsFriendsWrappers);
            familyList.setAdapter(familyAdapter);
            AppGlobalData.reloadfamilydataBoolean = false;
//            AppGlobalData.appGlobalData.setMemberInfoWrappers(memberInfoWrappers);
        } else if (response != null && taskcode == TaskCode.DELETEMEMBERCODE) {
            try {
                JSONObject responseObj = new JSONObject(response);
                if (responseObj.getInt("StatusCode") == 1) {
                    invitationsFriendsWrappers.remove(removePos);
                    familyAdapter.notifyDataSetChanged();
                } else {

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (AppGlobalData.getInstance().getTreeWrapperArrayList().size() == 0 || AppGlobalData.reloadfamilydataBoolean == true) {
//
//            if (MyApplication.isInternetWorking(InvitationsFriendsActivity.this)) {
//                List<NameValuePair> valuePairs = new ArrayList<>();
//                valuePairs.add(new BasicNameValuePair("NodeId", MyApplication.getUserID()));
//                new WebTask(InvitationsFriendsActivity.this, Urls.getFamalyViewTreeUrl, valuePairs, this, TaskCode.FAMILYVIEWTREECODE).execute();
//            }
//        } else {
//            invitationsFriendsWrappers = new ArrayList<>();
//            invitationsFriendsWrappers.addAll(AppGlobalData.getInstance().getTreeWrapperArrayList());
//            familyAdapter = new FamilyAdapter(InvitationsFriendsActivity.this, memberInfoWrappers);
//            familyList.setAdapter(familyAdapter);
//            AppGlobalData.reloadwebViewBoolean = true;
//        }
    }

    public class FamilyAdapter extends BaseAdapter {
        ArrayList<InvitationsFriendsWrapper> memberInfoWrappers;
        Context context;
        ViewHolder viewHolder = null;

        public FamilyAdapter(Context context, ArrayList<InvitationsFriendsWrapper> memberInfoWrappers) {
            this.context = context;
            this.memberInfoWrappers = memberInfoWrappers;
        }

        @Override
        public int getCount() {
            return memberInfoWrappers.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = inflater.inflate(R.layout.invitations_friends_list_layout, null);
                convertView.setTag(viewHolder);
                viewHolder.titleTxt = (TextViewProximaNovaRegular) convertView.findViewById(R.id.titleTxt);
                viewHolder.nameTxt = (TextViewProximaNovaRegular) convertView.findViewById(R.id.nameTxt);
                viewHolder.lavalTxt = (TextViewProximaNovaRegular) convertView.findViewById(R.id.lavalTxt);
                viewHolder.profileImg = (ImageView) convertView.findViewById(R.id.profileImg);
                viewHolder.dobTxt = (TextViewProximaNovaRegular) convertView.findViewById(R.id.dobTxt);
                viewHolder.sideView = (ImageView) convertView.findViewById(R.id.sideView);
                viewHolder.sendinvitationTxt = (TextViewProximaNovaRegular) convertView.findViewById(R.id.sendinvitationTxt);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent profile_intent = new Intent(InvitationsFriendsActivity.this, ProfileActivity.class).putExtra("id", memberInfoWrappers.get(position).getNodeId());
//                    startActivity(profile_intent);
                }
            });
            viewHolder.nameTxt.setText(memberInfoWrappers.get(position).getName());
//            viewHolder.lavalTxt.setText(memberInfoWrappers.get(position).getRelationLevel());
            if (memberInfoWrappers.get(position).getGender().equalsIgnoreCase("M")) {
                viewHolder.titleTxt.setTextColor(getResources().getColor(R.color.board_blue_color));
                viewHolder.sideView.setBackgroundColor(getResources().getColor(R.color.board_blue_color));
            } else {
                viewHolder.titleTxt.setTextColor(getResources().getColor(R.color.bottom_btn_two));
                viewHolder.sideView.setBackgroundColor(getResources().getColor(R.color.bottom_btn_two));
            }
            viewHolder.sendinvitationTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendInviteDialog(memberInfoWrappers.get(position).getNodeId(), memberInfoWrappers.get(position).getEmail(), memberInfoWrappers.get(position).getName());
                }
            });
            MyApplication.loader.displayImage(Urls.base_Url + memberInfoWrappers.get(position).getImage(), viewHolder.profileImg, MyApplication.options);
            return convertView;
        }

        public class ViewHolder {
            ImageView profileImg;
            ImageView sideView;
            TextViewProximaNovaRegular dobTxt, dodTxt, lavalTxt, titleTxt, nameTxt, sendinvitationTxt;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            languageCode(MyApplication.getLanguagecode());
        } else {
            languageCode(MyApplication.getLanguagecode());
        }
    }

    public void languageCode(String code) {
        Locale locale;
        Configuration lconfig;
        locale = new Locale(code);
        Locale.setDefault(locale);
        lconfig = new Configuration();
        lconfig.locale = locale;
        getBaseContext().getResources().updateConfiguration(lconfig,
                getBaseContext().getResources().getDisplayMetrics());
    }

    Dialog dialog;

    public void sendInviteDialog(final String nodeid, final String EmailAddress, String name) {
        dialog = new Dialog(InvitationsFriendsActivity.this, R.style.Theme_Custom);
        dialog.setContentView(R.layout.send_invitation_popup);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        TextViewProximaNovaRegular cancelTxt = (TextViewProximaNovaRegular) dialog.findViewById(R.id.cancelTxt);
        TextViewProximaNovaRegular inviteTxt = (TextViewProximaNovaRegular) dialog.findViewById(R.id.inviteTxt);
        TextViewProximaNovaRegular nameTxt = (TextViewProximaNovaRegular) dialog.findViewById(R.id.nameTxt);
        TextViewProximaNovaRegular sendinvitationTxt = (TextViewProximaNovaRegular) dialog.findViewById(R.id.inviteTxt);
//        nameTxt.setText(nameTxt.getText().toString());
        nameTxt.setText(name);
        final EditTextProximaNovaRegular emailEdt = (EditTextProximaNovaRegular) dialog.findViewById(R.id.emailEdt);
        ImageView facebookImg = (ImageView) dialog.findViewById(R.id.facebookImg);
        ImageView smsImg = (ImageView) dialog.findViewById(R.id.smsImg);
        ImageView whatsappImg = (ImageView) dialog.findViewById(R.id.whatsappImg);
        ImageView wechatImg = (ImageView) dialog.findViewById(R.id.wechatImg);
        ImageView viberImg = (ImageView) dialog.findViewById(R.id.viberImg);
        viberImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().hideSoftKeyBoard(InvitationsFriendsActivity.this);
                List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                valuePairs.add(new BasicNameValuePair("NodeId", nodeid));
                String url = "";
                if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                    valuePairs.add(new BasicNameValuePair("EmailAddress", EmailAddress));
                    valuePairs.add(new BasicNameValuePair("NewEmailAddress", emailEdt.getText().toString()));
                    url = Urls.sendInvitationApi;
                } else {
                    url = Urls.mobileinviteUrl;
                }
                valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                new WebTask(InvitationsFriendsActivity.this, url, valuePairs, new WebTaskComplete() {
                    @Override
                    public void onComplete(String response, int taskcode) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("StatusCode") == 1) {
                                String inviteUrl = MyApplication.getUserFirstName() + " " + getResources().getString(R.string.invitation_message) + " " + responseObj.getString("InvitationLink");
                                Intent i = new Intent(Intent.ACTION_SEND);
                                i.setPackage("com.viber.voip");
                                i.setType("text/plain");
                                i.putExtra(Intent.EXTRA_TEXT, inviteUrl);
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                            } else {
                                dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, TaskCode.SENDINVITATION).execute();

            }
        });
        wechatImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().hideSoftKeyBoard(InvitationsFriendsActivity.this);
                List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                valuePairs.add(new BasicNameValuePair("NodeId", nodeid));
                String url = "";
                if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                    valuePairs.add(new BasicNameValuePair("EmailAddress", EmailAddress));
                    valuePairs.add(new BasicNameValuePair("NewEmailAddress", emailEdt.getText().toString()));
                    url = Urls.sendInvitationApi;
                } else {
                    url = Urls.mobileinviteUrl;
                }
                valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                new WebTask(InvitationsFriendsActivity.this, url, valuePairs, new WebTaskComplete() {
                    @Override
                    public void onComplete(String response, int taskcode) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("StatusCode") == 1) {
                                String inviteUrl = MyApplication.getUserFirstName() + " " + getResources().getString(R.string.invitation_message) + " " + responseObj.getString("InvitationLink");
                                shareonWeChat(inviteUrl);
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                            } else {
                                dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, TaskCode.SENDINVITATION).execute();
            }
        });
        whatsappImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().hideSoftKeyBoard(InvitationsFriendsActivity.this);
                List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                valuePairs.add(new BasicNameValuePair("NodeId", nodeid));
                String url = "";
                if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                    valuePairs.add(new BasicNameValuePair("EmailAddress", EmailAddress));
                    valuePairs.add(new BasicNameValuePair("NewEmailAddress", emailEdt.getText().toString()));
                    url = Urls.sendInvitationApi;
                } else {
                    url = Urls.mobileinviteUrl;
                }
                valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                new WebTask(InvitationsFriendsActivity.this, url, valuePairs, new WebTaskComplete() {
                    @Override
                    public void onComplete(String response, int taskcode) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("StatusCode") == 1) {
                                String inviteUrl = MyApplication.getUserFirstName() + " " + getResources().getString(R.string.invitation_message) + " " + responseObj.getString("InvitationLink");
                                onClickWhatsApp(inviteUrl);
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                            } else {
                                dialog.dismiss();
                                Toast.makeText(InvitationsFriendsActivity.this, responseObj.getString("Message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, TaskCode.SENDINVITATION).execute();
            }
        });
        smsImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().hideSoftKeyBoard(InvitationsFriendsActivity.this);
                List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();

                valuePairs.add(new BasicNameValuePair("NodeId", nodeid));
                String url = "";
                if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                    valuePairs.add(new BasicNameValuePair("EmailAddress", EmailAddress));
                    valuePairs.add(new BasicNameValuePair("NewEmailAddress", emailEdt.getText().toString()));
                    url = Urls.sendInvitationApi;
                } else {
                    url = Urls.mobileinviteUrl;
                }
                valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                new WebTask(InvitationsFriendsActivity.this, url, valuePairs, new WebTaskComplete() {
                    @Override
                    public void onComplete(String response, int taskcode) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("StatusCode") == 1) {
                                String inviteUrl = MyApplication.getUserFirstName() + " " + getResources().getString(R.string.invitation_message) + " " + responseObj.getString("InvitationLink");
                                PackageManager pm = getPackageManager();
                                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                                sendIntent.setData(Uri.parse("sms:"));
                                sendIntent.putExtra("sms_body", inviteUrl);
                                startActivity(sendIntent);
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                            } else {
                                dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, TaskCode.SENDINVITATION).execute();
            }
        });
        facebookImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MyApplication.getInstance().hideSoftKeyBoard(InvitationsFriendsActivity.this);
                List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                valuePairs.add(new BasicNameValuePair("NodeId", nodeid));
                String url = "";
                if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                    valuePairs.add(new BasicNameValuePair("EmailAddress", EmailAddress));
                    valuePairs.add(new BasicNameValuePair("NewEmailAddress", emailEdt.getText().toString()));
                    url = Urls.sendInvitationApi;
                } else {
                    url = Urls.mobileinviteUrl;
                }
                valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                new WebTask(InvitationsFriendsActivity.this, url, valuePairs, new WebTaskComplete() {
                    @Override
                    public void onComplete(String response, int taskcode) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("StatusCode") == 1) {
                                String inviteUrl = MyApplication.getUserFirstName() + " " + getResources().getString(R.string.invitation_message) + " " + responseObj.getString("InvitationLink");
                                shareFb(inviteUrl, emailEdt.getText().toString());
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                            } else {
                                dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, TaskCode.SENDINVITATION).execute();

            }
        });
        sendinvitationTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                    MyApplication.getInstance().hideSoftKeyBoard(InvitationsFriendsActivity.this);
                    List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                    valuePairs.add(new BasicNameValuePair("NodeId", nodeid));
                    valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                    new WebTask(InvitationsFriendsActivity.this, Urls.mobileinviteUrl, valuePairs, new WebTaskComplete() {
                        @Override
                        public void onComplete(String response, int taskcode) {
                            try {
                                JSONObject responseObj = new JSONObject(response);
                                if (responseObj.getInt("StatusCode") == 1) {
                                    if (dialog != null) {
                                        dialog.dismiss();
                                    }
                                } else {
                                    dialog.dismiss();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, TaskCode.SENDINVITATION).execute();
                } else {
                    Toast.makeText(InvitationsFriendsActivity.this, getString(R.string.please_enter_email), Toast.LENGTH_SHORT).show();
                }
            }
        });
        cancelTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                MyApplication.getInstance().hideSoftKeyBoard(InvitationsFriendsActivity.this);
            }
        });
    }

    public void shareFb(String url, String email) {
        try {
            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
            sendIntent.setType("plain/text");
            sendIntent.setData(Uri.parse(email));
            sendIntent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
            sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.join_your_family_tree));
            sendIntent.putExtra(Intent.EXTRA_TEXT, url);
            startActivity(sendIntent);
        } catch (Exception e) {
            Intent sendIntent = new Intent(Intent.ACTION_SEND);
            sendIntent.setType("plain/text");
//            sendIntent.setData(Uri.parse("mailto:" + email));
            sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.join_your_family_tree));
            sendIntent.putExtra(Intent.EXTRA_TEXT, url);
            startActivity(sendIntent);
        }
    }

    public void onClickWhatsApp(String url) {
        PackageManager pm = getPackageManager();
        try {
            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
            //Check if package exists or not. If not then code
            //in catch block will be called
            waIntent.setPackage("com.whatsapp");
            waIntent.putExtra(Intent.EXTRA_TEXT, url);
            startActivity(Intent.createChooser(waIntent, "Share with"));
        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(this, "WhatsApp not Installed", Toast.LENGTH_SHORT).show();
        }
    }

    public void shareonWeChat(String url) {
//        WXWebpageObject webpage = new WXWebpageObject();
//        webpage.webpageUrl = "http://www.wechat.com";
//        WXMediaMessage msg = new WXMediaMessage(webpage);
//        msg.title = "Wechat homepage";
//        msg.description = url;
        try {
            PackageManager pm = getPackageManager();

            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            PackageInfo info = pm.getPackageInfo("com.tencent.mm", PackageManager.GET_META_DATA);
            //Check if package exists or not. If not then code
            //in catch block will be called
            waIntent.setPackage("com.tencent.mm");
            waIntent.putExtra(Intent.EXTRA_TEXT, url);
            startActivity(Intent.createChooser(waIntent, "Share with"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
