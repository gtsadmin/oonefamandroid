package com.onefam.activitys;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.onefam.R;
import com.onefam.applications.MyApplication;
import com.onefam.datacontroller.AppGlobalData;
import com.onefam.views.EditTextProximaNovaRegular;
import com.onefam.views.RadioButtonProximaNovaRegular;
import com.onefam.views.TextViewProximaNovaRegular;
import com.onefam.webutility.ParseData;
import com.onefam.webutility.TaskCode;
import com.onefam.webutility.Urls;
import com.onefam.webutility.WebTask;
import com.onefam.webutility.WebTaskComplete;
import com.onefam.wrapper.MemberInfo;
import com.onefam.wrapper.TreeWrapper;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.onefam.R.id.emailId;
import static com.onefam.R.string.email;

public class AddMember extends AppCompatActivity implements WebTaskComplete {
    @Bind(R.id.backLayout)
    LinearLayout backLayout;
    @Bind(R.id.topbarLayout)
    RelativeLayout topbarLayout;
    @Bind(R.id.nameEdt)
    EditTextProximaNovaRegular nameEdt;
    @Bind(R.id.lastnameEdt)
    EditTextProximaNovaRegular lastnameEdt;
    @Bind(R.id.dateEdt)
    TextViewProximaNovaRegular dateEdt;
    @Bind(R.id.selectfamilymamberSpinner)
    Spinner selectfamilymamberSpinner;
    @Bind(R.id.selectrelationshipSpinner)
    Spinner selectrelationshipSpinner;
    @Bind(R.id.addmemberLayout)
    LinearLayout addmemberLayout;
    public int year;
    public int month;
    public int day;
    private final int DATE_DIALOG_ID = 1;
    int selectdate = 0;
    @Bind(R.id.maleRadioBtn)
    RadioButtonProximaNovaRegular maleRadioBtn;
    @Bind(R.id.femaleRadioBtn)
    RadioButtonProximaNovaRegular femaleRadioBtn;
    String gender_str = "M", relation_str = "";
    String nodeID = "";
    @Bind(R.id.birthplaceEdt)
    EditTextProximaNovaRegular birthplaceEdt;
    @Bind(R.id.livingRadioBtn)
    RadioButtonProximaNovaRegular livingRadioBtn;
    @Bind(R.id.deceaseRadioBtn)
    RadioButtonProximaNovaRegular deceaseRadioBtn;
    @Bind(R.id.emailEdt)
    EditTextProximaNovaRegular emailEdt;
    @Bind(R.id.dodEdt)
    TextViewProximaNovaRegular dodEdt;
    @Bind(R.id.titleTxt)
    TextViewProximaNovaRegular titleTxt;
    @Bind(R.id.submitTxt)
    TextViewProximaNovaRegular submitTxt;
    int isAlive = 1;
    @Bind(R.id.iconaddEditmember)
    ImageView iconaddEditmember;
    private Calendar calendar;
    String currentdateString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_member_layout);
        ButterKnife.bind(this);
        Log.v("editmemeberBoolean ", AppGlobalData.editmemeberBoolean + " " + MyApplication.getTokenID()+" Token"+MyApplication.getTokenID());

        final float scale = this.getResources().getDisplayMetrics().density;
        livingRadioBtn.setPadding((int) getResources().getDimension(R.dimen.dim_20),
                livingRadioBtn.getPaddingTop(),
                livingRadioBtn.getPaddingRight(),
                livingRadioBtn.getPaddingBottom());
        deceaseRadioBtn.setPadding((int) getResources().getDimension(R.dimen.dim_20),
                deceaseRadioBtn.getPaddingTop(),
                deceaseRadioBtn.getPaddingRight(),
                deceaseRadioBtn.getPaddingBottom());
        maleRadioBtn.setChecked(true);
        livingRadioBtn.setChecked(true);
        if (MyApplication.isConnectingToInternet(AddMember.this)) {
//            if (AppGlobalData.getInstance().getMemberList().size() == 0) {
            List<NameValuePair> valuePairs = new ArrayList<>();
            valuePairs.add(new BasicNameValuePair("NodeId", MyApplication.getUserID()));
            new WebTask(AddMember.this, Urls.getFamalyViewTreeUrl, valuePairs, this, TaskCode.FAMILYVIEWTREECODE).execute();
//            }
        }
        ArrayAdapter<String> relation_ship_arr = new ArrayAdapter<String>(this, R.layout.spinner_layout, getResources().getStringArray(R.array.relation_ship_arr)) {
            public View getView(int position, View convertView,
                                ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                if (position == 0) {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.gray_color));
                    ((TextView) v).setTextSize(getResources().getDimension(R.dimen.dim_6));
                } else {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.white));
                }

                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView,
                        parent);
                ((TextView) v).setTextColor(Color.BLACK);
                return v;
            }
        };

        selectrelationshipSpinner.setAdapter(relation_ship_arr);
        selectrelationshipSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 1) {
                    relation_str = "FA";
                } else if (position == 2) {
                    relation_str = "MO";
                } else if (position == 3) {
                    relation_str = "BR";
                } else if (position == 4) {
                    relation_str = "SI";
                } else if (position == 5) {
                    relation_str = "DA";
                } else if (position == 6) {
                    relation_str = "SO";
                } else if (position == 7) {
                    relation_str = "WI";
                } else if (position == 8) {
                    relation_str = "HU";
                } else {
                    relation_str = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
//        AppGlobalData.getInstance().getMemberList().add("Select member");
//        Collections.reverse(AppGlobalData.getInstance().getMemberList());
        ArrayAdapter<String> member_ship_arr = new ArrayAdapter<String>(this, R.layout.spinner_layout, AppGlobalData.getInstance().getMemberList()) {
            public View getView(int position, View convertView,
                                ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == 0) {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.gray_color));
                    ((TextView) v).setTextSize(getResources().getDimension(R.dimen.dim_6));
                } else {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.white));
                }


                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView,
                        parent);


                ((TextView) v).setTextColor(Color.BLACK);


                return v;
            }
        };
        selectfamilymamberSpinner.setAdapter(member_ship_arr);
//        selectfamilymamberSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
//                Toast.makeText(AddMember.this, selectfamilymamberSpinner.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
//                if (!selectfamilymamberSpinner.getSelectedItem().toString().contains(AddMember.this.getResources().getString(R.string.select_member))) {
////                    nodeID = AppGlobalData.getInstance().getMamberListMap().get(selectfamilymamberSpinner.getSelectedItem().toString());
//                    nodeID = AppGlobalData.getInstance().getMamberListMap().get(selectfamilymamberSpinner.getSelectedItem().toString());
//                }
//                Log.v("nodeId ", nodeID);
//            }

//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });

        try {
//            nodeID = getIntent().getExtras().getString("nodeid");
            if (!AppGlobalData.editmemeberBoolean) {

//                selectfamilymamberSpinner.setSelection(AppGlobalData.getInstance().getMemberList().indexOf(AppGlobalData.getInstance().getMembernamelistMap().get(nodeID)));
                selectfamilymamberSpinner.setSelection(AppGlobalData.getInstance().getAllMemberList().indexOf(AppGlobalData.getInstance().getMembernamelistMap().get(nodeID)));
//                selectfamilymamberSpinner.setEnabled(false);
            } else {

                selectfamilymamberSpinner.setEnabled(false);
                selectrelationshipSpinner.setEnabled(false);

                selectfamilymamberSpinner.setVisibility(View.INVISIBLE);
                selectrelationshipSpinner.setVisibility(View.INVISIBLE);

//                selectfamilymamberSpinner.setSelection(AppGlobalData.getInstance().getMemberList().indexOf(AppGlobalData.getInstance().getMembernamelistMap().get(nodeID)));
                selectfamilymamberSpinner.setSelection(AppGlobalData.getInstance().getAllMemberList().indexOf(AppGlobalData.getInstance().getMembernamelistMap().get(nodeID)));
//                selectfamilymamberSpinner.setVisibility(View.GONE);
//                selectrelationshipSpinner.setVisibility(View.GONE);
            }
        } catch (Exception ex) {
            nodeID = MyApplication.getUserID();
        }

        maleRadioBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    gender_str = "M";
                }
            }
        });
        femaleRadioBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    gender_str = "F";
                }
            }
        });
        livingRadioBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
//                    gender_str ="M";
                    isAlive = 1;
                    dodEdt.setVisibility(View.GONE);

                }
            }
        });
        deceaseRadioBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
//                    gender_str ="F";
                    isAlive = 0;
                    dodEdt.setVisibility(View.VISIBLE);

                }
            }
        });
        if (AppGlobalData.editmemeberBoolean == true) {
            titleTxt.setText(R.string.edit_member);
            iconaddEditmember.setBackgroundResource(R.mipmap.icn_edit_member);
            submitTxt.setText(R.string.update_member);
//            emailEdt.setEnabled(false);
            nodeID = getIntent().getExtras().getString("nodeid");

            selectfamilymamberSpinner.setEnabled(false);
//            selectrelationshipSpinner.setEnabled(false);
            if (MyApplication.isConnectingToInternet(AddMember.this)) {
                new WebTask(AddMember.this, Urls.getMember + nodeID, null, this, TaskCode.GETMEMBERCODE).execute();
            }
        } else {

            iconaddEditmember.setBackgroundResource(R.mipmap.icn_add_member);
        }
        calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        String dayString = day + "", monthString = (month + 1) + "";

        if ((month + 1) < 10) {
            monthString = "0" + (month + 1);
        }
        if (day < 10) {
            dayString = "0" + day;
        }
//        SimpleDateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        currentdateString = dayString + "-" + monthString + "-" + year;


        initiateTextWatcher();

    }

    @OnClick(R.id.dateEdt)
    public void dateEdt() {
        showDialog(DATE_DIALOG_ID);
        selectdate = 1;
    }

    @OnClick(R.id.dodEdt)
    public void dodEdt() {
        showDialog(DATE_DIALOG_ID);
        selectdate = 2;
    }

    @OnClick(R.id.backLayout)
    public void backLayout() {
        onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppGlobalData.editmemeberBoolean = false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        AppGlobalData.editmemeberBoolean = false;
    }

    @OnClick(R.id.addmemberLayout)
    public void addmemberLayout() {
        MyApplication.myApplication.produceAnimation(addmemberLayout);
        if (AppGlobalData.editmemeberBoolean == true) {
            nodeID = getIntent().getExtras().getString("nodeid");
        }
        if (TextUtils.isEmpty(nameEdt.getText().toString())) {
            nameEdt.setError(getResources().getString(R.string.first_name));
            nameEdt.requestFocus();
        } else if (TextUtils.isEmpty(lastnameEdt.getText().toString())) {
            lastnameEdt.setError(getResources().getString(R.string.last_name));
            lastnameEdt.requestFocus();
        } else if (emailEdt.getText().toString().length() > 0 && !MyApplication.isValidEmail(emailEdt.getText().toString())) {
            emailEdt.setError(getResources().getString(R.string.Pleaseentervalidemailid));
            emailEdt.requestFocus();
        } else if (TextUtils.isEmpty(nodeID)) {
            Toast.makeText(AddMember.this, R.string.select_member, Toast.LENGTH_SHORT).show();
        } else if (!AppGlobalData.editmemeberBoolean) {
            if (TextUtils.isEmpty(relation_str)) {
                Toast.makeText(AddMember.this, getResources().getString(R.string.specify_relationship), Toast.LENGTH_SHORT).show();
            } else {
                addEditMember();
            }
        } else {
            addEditMember();
        }
    }

    private void addEditMember() {
        List<NameValuePair> valuePairs = new ArrayList<>();
        valuePairs.add(new BasicNameValuePair("Relation", relation_str));
        valuePairs.add(new BasicNameValuePair("FirstName", nameEdt.getText().toString()));
        valuePairs.add(new BasicNameValuePair("LastName", lastnameEdt.getText().toString()));
        valuePairs.add(new BasicNameValuePair("IsAlive", "" + isAlive));
        valuePairs.add(new BasicNameValuePair("EmailID", emailEdt.getText().toString()));
        valuePairs.add(new BasicNameValuePair("FatherId", ""));
        valuePairs.add(new BasicNameValuePair("MotherId", ""));
        valuePairs.add(new BasicNameValuePair("DOB", serverformateDate(dateEdt.getText().toString())));
        valuePairs.add(new BasicNameValuePair("BirthPlace", birthplaceEdt.getText().toString()));
        valuePairs.add(new BasicNameValuePair("DOD", serverformateDate(dodEdt.getText().toString())));
        valuePairs.add(new BasicNameValuePair("DeathCause", ""));
        valuePairs.add(new BasicNameValuePair("DeathPlace", ""));
        valuePairs.add(new BasicNameValuePair("MarriageDate", ""));
        valuePairs.add(new BasicNameValuePair("MarriagePlace", ""));
        valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
        if (MyApplication.isInternetWorking(AddMember.this)) {
            MyApplication.hideKeyBoard(AddMember.this);
            if (AppGlobalData.editmemeberBoolean) {
                valuePairs.add(new BasicNameValuePair("NodeId", nodeID));
                new WebTask(AddMember.this, Urls.editmemberapiMember, valuePairs, this, TaskCode.EDITMEMBERAPI).execute();
            } else {
                valuePairs.add(new BasicNameValuePair("EditMemberId", nodeID));
                new WebTask(AddMember.this, Urls.addmemberUrl, valuePairs, this, TaskCode.ADDMEMBERCODE).execute();
            }
        }
    }


    private String formateDate(String dateString) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat("dd MMMM yyyy");
            Date date = originalFormat.parse(dateString);
            String formattedDate = targetFormat.format(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String serverformateDate(String dateString) {
        try {
            if (!TextUtils.isEmpty(dateString)) {
                SimpleDateFormat originalFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.ENGLISH);
                SimpleDateFormat targetFormat = new SimpleDateFormat("dd MMMM yyyy");
                Date date = targetFormat.parse(dateString);
                String formattedDate = originalFormat.format(date);
                return formattedDate;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String formateDate1(String dateString) {
        try {
//            SimpleDateFormat originalFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
            SimpleDateFormat originalFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat("dd MMMM yyyy");
            Date date = originalFormat.parse(dateString);
            String formattedDate = targetFormat.format(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        Calendar c = Calendar.getInstance(Locale.ENGLISH);

        switch (id) {
            case DATE_DIALOG_ID:
                // set date picker as current date

                return new DatePickerDialog(this, datePickerListener,
                        c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));


        }
        return null;
    }


    private String dateString = "";
    private String doddateString = "";
    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {


        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            // set selected date into textview
            String dayString = day + "", monthString = (month + 1) + "";
            if ((month + 1) < 10) {
                monthString = "0" + (month + 1);
            }
            if (day < 10) {
                dayString = "0" + day;
            }
//            SimpleDateFormat originalFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.ENGLISH);
            SimpleDateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
            Date bodDate = null, currentDate = null;


            if (selectdate == 1) {
                dateString = dayString + "-" + monthString + "-" + year;
                try {
                    bodDate = originalFormat.parse(dateString);
                    currentDate = originalFormat.parse(currentdateString);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (!bodDate.after(currentDate)) {
                    dateEdt.setText(formateDate(dateString));
                } else {
                    dateEdt.setText("");
//                    dateEdt.setError(getResources().getString(R.string.valid_dob));
                    Toast.makeText(AddMember.this, getResources().getString(R.string.valid_dob), Toast.LENGTH_SHORT).show();
                }
            }
            if (selectdate == 2) {
                doddateString = dayString + "-" + monthString + "-" + year;
                try {
                    bodDate = originalFormat.parse(doddateString);
                    currentDate = originalFormat.parse(currentdateString);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (bodDate.before(currentDate)) {
                    dodEdt.setText(formateDate(doddateString));
                }
            }

        }
    };

    @Override
    public void onComplete(String response, int taskcode) {
        if (response != null && taskcode == TaskCode.ADDMEMBERCODE) {
            System.out.println("response" + response);
            try {
//                {"StatusCode":1,"Message":"success","NodeId":0}
                JSONObject responseObj = new JSONObject(response);
                if (responseObj.getInt("StatusCode") == 1) {
//                    finish();
                    AppGlobalData.reloadwebViewBoolean = true;
                    AppGlobalData.reloadfamilydataBoolean = true;
                    if (MyApplication.isInternetWorking(AddMember.this)) {
                        getTagMemberList();
                    }

                } else if (responseObj.getInt("StatusCode") == 0 && responseObj.getString("Message").contains("Cannot insert the value NULL")) {
                    finish();
                    AppGlobalData.reloadwebViewBoolean = true;
                    AppGlobalData.reloadfamilydataBoolean = true;

//                    Toast.makeText(AddMember.this, responseObj.getString("Message"), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(AddMember.this, responseObj.getString("Message"), Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (response != null && taskcode == TaskCode.FAMILYVIEWTREECODE) {

            ArrayList<TreeWrapper> memberInfoWrappers = new ParseData(AddMember.this, response, taskcode).getTreeMember();
//            AppGlobalData.getInstance().getMemberList().add(getResources().getString(R.string.select_member));
//            Collections.reverse(AppGlobalData.getInstance().getMemberList());
//            ArrayAdapter<String> member_ship_arr = new ArrayAdapter<String>(this, R.layout.spinner_layout, AppGlobalData.getInstance().getMemberList()) {
            Log.v("all_member_list ", AppGlobalData.getInstance().getAllMemberList().size() + "");
            ArrayAdapter<String> member_ship_arr = new ArrayAdapter<String>(this, R.layout.spinner_layout, AppGlobalData.getInstance().getAllMemberList()) {
                public View getView(int position, View convertView,
                                    ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);

                    if (position == 0) {

                        ((TextView) v).setTextColor(getResources()
                                .getColorStateList(R.color.gray_color));
                        ((TextView) v).setTextSize(getResources().getDimension(R.dimen.dim_6));

                    } else {

                        ((TextView) v).setTextColor(getResources()
                                .getColorStateList(R.color.white));
                    }

                    return v;
                }

                public View getDropDownView(int position, View convertView,
                                            ViewGroup parent) {
                    View v = super.getDropDownView(position, convertView,
                            parent);

                    ((TextView) v).setTextColor(Color.BLACK);
                    return v;
                }
            };

            selectfamilymamberSpinner.setAdapter(member_ship_arr);
            selectfamilymamberSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Log.v("selcted_relation ", selectfamilymamberSpinner.getSelectedItem().toString());
                    if (!selectfamilymamberSpinner.getSelectedItem().toString().contains(AddMember.this.getResources().getString(R.string.select_member))) {
//                        nodeID = AppGlobalData.getInstance().getMamberListMap().get(selectfamilymamberSpinner.getSelectedItem().toString());
                        nodeID = AppGlobalData.getInstance().getAllMemberNodeIdList().get(selectfamilymamberSpinner.getSelectedItemPosition());
                        Log.v("selcted_relation ", selectfamilymamberSpinner.getSelectedItem().toString() + " " + nodeID);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            try {
                nodeID = getIntent().getExtras().getString("nodeid");
//                selectfamilymamberSpinner.setSelection(AppGlobalData.getInstance().getMemberList().indexOf(AppGlobalData.getInstance().getMembernamelistMap().get(nodeID)));
                selectfamilymamberSpinner.setSelection(AppGlobalData.getInstance().getAllMemberList().indexOf(AppGlobalData.getInstance().getMembernamelistMap().get(nodeID)));
            } catch (Exception ex) {
                nodeID = MyApplication.getUserID();
            }
        } else if (response != null && taskcode == TaskCode.GETMEMBERCODE) {
            Log.v("response_edit_profile ", response);
            MemberInfo memberInfo = new ParseData(AddMember.this, response, TaskCode.GETMEMBERCODE).getMemberInfo();
            nameEdt.append(memberInfo.getFirstName());
            lastnameEdt.append(memberInfo.getLastName());
            emailEdt.append(memberInfo.getEmailId());
            if (memberInfo.getIsAlive().equalsIgnoreCase("True")) {
                livingRadioBtn.setChecked(true);
            } else {
                livingRadioBtn.setChecked(false);
            }
            if (memberInfo.getIsAlive().equalsIgnoreCase("false")) {
                deceaseRadioBtn.setChecked(true);
            } else {
                deceaseRadioBtn.setChecked(false);
            }
            String[] dateSpilt = memberInfo.getDateOfBirth().split(" ");
            dateEdt.setText(formateDate1(dateSpilt[0]));
            birthplaceEdt.append(memberInfo.getBirthPlace());
            String[] dateSpilt1 = memberInfo.getDateOfDeath().split(" ");
            dodEdt.setText(formateDate1(dateSpilt1[0]));
//            if (!memberInfo.getSelectedFather().equalsIgnoreCase("0")) {
//                selectfamilymamberSpinner.setSelection(AppGlobalData.getInstance().getMemberList().indexOf(memberInfo.getSelectedFather()));
//            } else if (!memberInfo.getSelectedMother().equalsIgnoreCase("0")) {
//                selectfamilymamberSpinner.setSelection(AppGlobalData.getInstance().getMemberList().indexOf(memberInfo.getSelectedMother()));
//
//            } else {
//                selectfamilymamberSpinner.setSelection(AppGlobalData.getInstance().getMemberList().indexOf(memberInfo.getSelectedSpouse()));
//
//            }

        } else if (response != null && taskcode == TaskCode.EDITMEMBERAPI) {
//            {"StatusCode":1,"Message":"Success","NodeId":151}
            try {
//                {"StatusCode":1,"Message":"success","NodeId":0}
                JSONObject responseObj = new JSONObject(response);
                if (responseObj.getInt("StatusCode") == 1) {
                    Toast.makeText(AddMember.this, getResources().getString(R.string.MemberEdited), Toast.LENGTH_SHORT).show();
                    finish();
                    AppGlobalData.reloadwebViewBoolean = true;
                    AppGlobalData.reloadfamilydataBoolean = true;
                } else if (responseObj.getInt("StatusCode") == 0) {
                    Toast.makeText(AddMember.this, responseObj.getString("Message"), Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (response != null && taskcode == TaskCode.TAGMEMBER) {
            Log.v("tagMemberList ", response);
            parseAllMemberData(response);
            finish();
        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
//            MyApplication.getInstance().setLanguage(AppGlobalData.getInstance().getLanguageCode());
            dodEdt.setBackgroundResource(R.mipmap.input_birthdate_landscape);
            dateEdt.setBackgroundResource(R.mipmap.input_birthdate_landscape);
            selectrelationshipSpinner.setBackgroundResource(R.mipmap.drop_down_landscape);
            selectfamilymamberSpinner.setBackgroundResource(R.mipmap.drop_down_landscape);
            languageCode(MyApplication.getLanguagecode());
        } else {
//            MyApplication.getInstance().setLanguage(AppGlobalData.getInstance().getLanguageCode());
            dodEdt.setBackgroundResource(R.mipmap.input_birthdate);
            dateEdt.setBackgroundResource(R.mipmap.input_birthdate);
            selectrelationshipSpinner.setBackgroundResource(R.mipmap.drop_down);
            selectfamilymamberSpinner.setBackgroundResource(R.mipmap.drop_down);
            languageCode(MyApplication.getLanguagecode());
        }
    }

    public void languageCode(String code) {
        Locale locale;
        Configuration lconfig;
        locale = new Locale(code);
        Locale.setDefault(locale);
        lconfig = new Configuration();
        lconfig.locale = locale;
        getBaseContext().getResources().updateConfiguration(lconfig,
                getBaseContext().getResources().getDisplayMetrics());
    }

    private void initiateTextWatcher() {
        nameEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    nameEdt.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        lastnameEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    lastnameEdt.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        emailEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    emailEdt.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    private void getTagMemberList() {
        List<NameValuePair> valuePairs = new ArrayList<>();
        valuePairs.add(new BasicNameValuePair("NodeId", MyApplication.getUserID()));
        valuePairs.add(new BasicNameValuePair("name", ""));
        if (MyApplication.isInternetWorking(AddMember.this)) {
            new WebTask(AddMember.this, Urls.tagmember, valuePairs, AddMember.this, TaskCode.TAGMEMBER, true).execute();
        }
    }

    ArrayList<String> memberList = new ArrayList<>();
    ArrayList<String> memberListNodeId = new ArrayList<>();
    ArrayList<TreeWrapper> tagMemberArraylist = new ArrayList<>();

    private void parseAllMemberData(String response) {

        if (response != null) {
            try {
                JSONObject responseObj = new JSONObject(response);
                if (responseObj.getString("StatusCode").equalsIgnoreCase("1")) {
                    JSONArray MembersArray = responseObj.getJSONArray("Members");
                    if (MembersArray.length() != 0) {
                        for (int j = 0; j < MembersArray.length(); j++) {
                            TreeWrapper treeWrapper = new TreeWrapper();
                            JSONObject nodesObj = MembersArray.getJSONObject(j);
                            treeWrapper.setNodeId(nodesObj.getString("NodeId"));
                            treeWrapper.setName(nodesObj.getString("Name"));
                            treeWrapper.setImageUrl(nodesObj.getString("Image"));
                            treeWrapper.setGender(nodesObj.getString("Gender"));
                            treeWrapper.setDOB(nodesObj.getString("DateOfBirth"));
                            tagMemberArraylist.add(treeWrapper);

                            memberList.add(nodesObj.getString("Name"));
                            memberListNodeId.add(nodesObj.getString("NodeId"));

                        }
                        AppGlobalData.getInstance().getAllMemberList().clear();
                        AppGlobalData.getInstance().getAllMemberNodeIdList().clear();

                        AppGlobalData.getInstance().setAllMemberList(memberList);
                        AppGlobalData.getInstance().setAllMemberNodeIdList(memberListNodeId);

                        AppGlobalData.getInstance().getTagMemberArrayList().clear();
                        AppGlobalData.getInstance().setTagMemberArrayList(tagMemberArraylist);

                        AppGlobalData.getInstance().getAllMemberList().add(0, AddMember.this.getResources().getString(R.string.select_member));
                        AppGlobalData.getInstance().getAllMemberNodeIdList().add(0, "");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

}
