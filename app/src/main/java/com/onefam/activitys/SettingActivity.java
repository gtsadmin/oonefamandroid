package com.onefam.activitys;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.onefam.BuildConfig;
import com.onefam.R;
import com.onefam.applications.MyApplication;
import com.onefam.datacontroller.AppGlobalData;
import com.onefam.views.TextViewProximaNovaRegular;
import com.onefam.webutility.TaskCode;
import com.onefam.webutility.Urls;
import com.onefam.webutility.WebTask;
import com.onefam.webutility.WebTaskComplete;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SettingActivity extends AppCompatActivity implements WebTaskComplete {
    @Bind(R.id.topbarLayout)
    RelativeLayout topbarLayout;
    @Bind(R.id.choocelanguageSpinner)
    Spinner choocelanguageSpinner;
    @Bind(R.id.termsTxt)
    TextViewProximaNovaRegular termsTxt;
    @Bind(R.id.privacyTxt)
    TextViewProximaNovaRegular privacyTxt;
    @Bind(R.id.logoutLayout)
    LinearLayout logoutLayout;
    @Bind(R.id.backLayout)
    LinearLayout backLayout;
    @Bind(R.id.notoficatioToggleBtn)
    ToggleButton notoficatioToggleBtn;
    @Bind(R.id.invitationsTxt)
    TextViewProximaNovaRegular invitationsTxt;
    @Bind(R.id.pendinginvitationsTxt)
    TextViewProximaNovaRegular pendinginvitationsTxt;

    @Bind(R.id.versoinTxt)
    TextViewProximaNovaRegular versoinTxt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_layout);
        ButterKnife.bind(this);
        uiData();
    }

    public void languageCode(String code) {
        Locale locale;
        Configuration lconfig;
        locale = new Locale(code);
        Locale.setDefault(locale);
        lconfig = new Configuration();
        lconfig.locale = locale;
        getBaseContext().getResources().updateConfiguration(lconfig,
                getBaseContext().getResources().getDisplayMetrics());
    }

    public void languageCode1(String code) {
        Locale locale;
        Configuration lconfig;
        locale = new Locale(code);
        Locale.setDefault(locale);
        lconfig = new Configuration();
        lconfig.locale = locale;
        getBaseContext().getResources().updateConfiguration(lconfig,
                getBaseContext().getResources().getDisplayMetrics());
    }

    @OnClick(R.id.privacyTxt)
    public void privacyTxt() {
        startActivity(new Intent(SettingActivity.this, PrivacyActivity.class));
    }

    @OnClick(R.id.termsTxt)
    public void termsTxt() {
        startActivity(new Intent(SettingActivity.this, TermsConditionsActivity.class));
    }

    @OnClick(R.id.logoutLayout)
    public void logoutLayout() {
        MyApplication.myApplication.produceAnimation(logoutLayout);
        logoutAlert();
    }

    @OnClick(R.id.invitationsTxt)
    public void invitationsTxt() {
        startActivity(new Intent(SettingActivity.this, InvitationsFriendsActivity.class));
    }

    @OnClick(R.id.pendinginvitationsTxt)
    public void pendinginvitationsTxt() {
        startActivity(new Intent(SettingActivity.this, PaddingInvitationActivity.class));
    }

    @OnClick(R.id.backLayout)
    public void backLayout() {
        onBackPressed();
    }

    private void logoutAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SettingActivity.this);
        builder.setTitle("" + getResources().getString(R.string.app_name));
        builder.setMessage(getResources().getString(R.string.Areyousureyouwanttologout))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        logOutUser();
                    }
                });
        builder.show();
    }

    private void logOutUser() {
        runOnUiThread(new Runnable() {
            public void run() {
                int currentapiVersion = Build.VERSION.SDK_INT;
                CookieManager cookieManager = CookieManager.getInstance();
                if (currentapiVersion >= 21) {
                    cookieManager.removeAllCookies(null);
                } else {
                    cookieManager.removeAllCookie();
                }
                MyApplication.saveUserId("");
                MyApplication.saveEmailId("");
                MyApplication.savePassword("");
                MyApplication.setViewFamilyTreeId("");
                AppGlobalData.getInstance().getTreeWrapperArrayList().clear();
                AppGlobalData.getInstance().getEventList().clear();
                AppGlobalData.getInstance().getMamberListMap().clear();
                AppGlobalData.getInstance().getMemberInfoWrappers().clear();
                AppGlobalData.getInstance().getMembernamelistMap().clear();
                AppGlobalData.getInstance().getMemberList().clear();
//                AppGlobalData.getInstance().get
                setResult(RESULT_OK);
                Intent i = new Intent(SettingActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (!AppGlobalData.getInstance().call_familyBln) {
            finish();
            startActivity(new Intent(SettingActivity.this, FamilyTreeActivity.class));
        } else {
            finish();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
//            languageCode(MyApplication.getLanguagecode());
            setContentView(R.layout.setting_layout_land);
            ButterKnife.bind(this);
            uiData();
            languageCode(MyApplication.getLanguagecode());

        } else {
            setContentView(R.layout.setting_layout);
            ButterKnife.bind(this);
            uiData();
            languageCode(MyApplication.getLanguagecode());
        }
    }

    private void uiData() {

        try {
            versoinTxt.setText(getResources().getString(R.string.Version) + " " + BuildConfig.VERSION_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ArrayList<String> languageArr = new ArrayList<>();
        final HashMap<String, String> languageMap = new HashMap<>();
        HashMap<String, String> languageMap1 = new HashMap<>();
        languageArr.add(getResources().getString(R.string.Changelanguage));
        languageArr.add("English");
        languageArr.add("French");
        languageArr.add("Spanish");
        languageArr.add("German");
        languageArr.add("Chinese");
        languageArr.add("Vietnames");
        languageArr.add("Korean");
        languageArr.add("Polish");
        languageArr.add("Japanesse");
        languageArr.add("Russian");
        languageArr.add("Italian");
//        Vietnames
//        Korean
        languageMap.put("English", "en");
        languageMap.put("French", "fr");
        languageMap.put("German", "de");
        languageMap.put("Chinese", "zh");
        languageMap.put("Korean", "ko");
        languageMap.put("Spanish", "es");
        languageMap.put("Vietnames", "vi");
        languageMap.put("Polish", "pl");
        languageMap.put("Japanesse", "ja");
        languageMap.put("Russian", "ru");
        languageMap.put("Italian", "it");
        languageMap1.put("en", "English");
        languageMap1.put("fr", "French");
        languageMap1.put("de", "German");
        languageMap1.put("zh", "Chinese");
        languageMap1.put("ko", "Korean");
        languageMap1.put("es", "Spanish");
        languageMap1.put("vi", "Vietnames");
        languageMap1.put("Polish", "pl");
        languageMap1.put("Japanesse", "ja");
        languageMap1.put("Russian", "ru");
        languageMap1.put("it", "Italian");
        ArrayAdapter<String> languageArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, languageArr) {
            public View getView(int position, View convertView,
                                ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(20);
                ((TextView) v).setTextColor(
                        getResources()
                                .getColorStateList(R.color.white));
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView,
                        parent);


                ((TextView) v).setTextColor(Color.BLACK);


                return v;
            }
        };
        choocelanguageSpinner.setAdapter(languageArrayAdapter);
//        choocelanguageSpinner.setSelection(languageArr.indexOf(languageMap1.get(MyApplication.getLanguagecode())));
        choocelanguageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!choocelanguageSpinner.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.Changelanguage))) {
                    String languageCode = languageMap.get(choocelanguageSpinner.getSelectedItem().toString());
                    MyApplication.saveLanguagecode(languageCode);
                    languageCode(languageCode);

                    List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                    valuePairs.add(new BasicNameValuePair("NodeId", MyApplication.getUserID()));
                    valuePairs.add(new BasicNameValuePair("Token", MyApplication.getTokenID()));
                    valuePairs.add(new BasicNameValuePair("Language", MyApplication.getLanguagecode()));
                    if (MyApplication.isInternetWorking(SettingActivity.this)) {
                        new WebTask(SettingActivity.this, Urls.sendLanguageCode, valuePairs, SettingActivity.this, TaskCode.SENDLANGUAGECODE).execute();
                    } else {
                        AppGlobalData.getInstance().call_familyBln = false;
                        setResult(RESULT_OK);
                        finish();
                        startActivity(new Intent(SettingActivity.this, SettingActivity.class));
                    }
                }
//                   MyApplication.getInstance().setLanguage(languageCode);
//                Intent i = new Intent(SettingActivity.this,SettingActivity.class);
//                startActivity(i);
//                finish();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (MyApplication.getNotification()) {
            notoficatioToggleBtn.setChecked(true);
        } else {
            notoficatioToggleBtn.setChecked(false);
        }
        notoficatioToggleBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    MyApplication.saveNotification(true);
                } else {
                    MyApplication.saveNotification(false);
                }
            }
        });
    }

    @Override
    public void onComplete(String response, int taskcode) {
        if (taskcode == TaskCode.SENDLANGUAGECODE) {
            AppGlobalData.getInstance().call_familyBln = false;
            setResult(RESULT_OK);
            finish();
            startActivity(new Intent(SettingActivity.this, SettingActivity.class));
        }
    }
}
