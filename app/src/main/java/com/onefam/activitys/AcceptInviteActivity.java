package com.onefam.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.onefam.R;
import com.onefam.applications.MyApplication;
import com.onefam.views.TextViewProximaNovaRegular;
import com.onefam.webutility.TaskCode;
import com.onefam.webutility.Urls;
import com.onefam.webutility.WebTask;
import com.onefam.webutility.WebTaskComplete;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AcceptInviteActivity extends AppCompatActivity implements WebTaskComplete {
    @Bind(R.id.backLayout)
    LinearLayout backLayout;
    @Bind(R.id.topbarLayout)
    RelativeLayout topbarLayout;
    @Bind(R.id.profileImage)
    ImageView profileImage;
    @Bind(R.id.userNameTxt)
    TextViewProximaNovaRegular userNameTxt;
    @Bind(R.id.emailtextTxt)
    TextViewProximaNovaRegular emailtextTxt;
    @Bind(R.id.relationTxt)
    TextViewProximaNovaRegular relationTxt;
    @Bind(R.id.namerelationpersonTxt)
    TextViewProximaNovaRegular namerelationpersonTxt;
    @Bind(R.id.acceptTxt)
    TextViewProximaNovaRegular acceptTxt;
    @Bind(R.id.declineTxt)
    TextViewProximaNovaRegular declineTxt;
    String token = "";
    boolean checkFrom = false;
    @Bind(R.id.titleTxt)
    TextViewProximaNovaRegular titleTxt;
    String ToNodeId = "";
    String typeStr = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accept_invitations_layout);
        ButterKnife.bind(this);
        checkFrom = getIntent().getBooleanExtra("bln", false);
        token = getIntent().getExtras().getString("token");
        typeStr = getIntent().getExtras().getString("type");
        if (typeStr.split("/")[0].equalsIgnoreCase("invite")) {
            List<NameValuePair> valuePairs = new ArrayList<>();
            valuePairs.add(new BasicNameValuePair("Token", token));
            valuePairs.add(new BasicNameValuePair("Email", MyApplication.getEmailID()));
            new WebTask(AcceptInviteActivity.this, Urls.registerInviteURL, valuePairs, AcceptInviteActivity.this, TaskCode.REGISTERINVITE).execute();
        } else {
            List<NameValuePair> valuePairs = new ArrayList<>();
            valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
            valuePairs.add(new BasicNameValuePair("Token", token));
            valuePairs.add(new BasicNameValuePair("Email", MyApplication.getEmailID()));
            new WebTask(AcceptInviteActivity.this, Urls.getpersondetailUrl, valuePairs, AcceptInviteActivity.this, TaskCode.PERSONREQUEST).execute();
        }

//        if (!TextUtils.isEmpty(token))
//        {
//            List<NameValuePair> valuePairs = new ArrayList<>();
//            valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
//            valuePairs.add(new BasicNameValuePair("Token", token));
//            valuePairs.add(new BasicNameValuePair("Email", MyApplication.getEmailID()));
//            new WebTask(AcceptInviteActivity.this, Urls.getpersondetailUrl, valuePairs, AcceptInviteActivity.this, TaskCode.PERSONREQUEST).execute();
//        }
    }

    @OnClick(R.id.backLayout)
    public void backLayout() {
        onBackPressed();
    }

    @OnClick({R.id.acceptTxt, R.id.declineTxt})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.acceptTxt:
                List<NameValuePair> valuePairs = new ArrayList<>();
                valuePairs.add(new BasicNameValuePair("NodeId", ToNodeId));
//                valuePairs.add(new BasicNameValuePair("Token", token));
                valuePairs.add(new BasicNameValuePair("FromNodeId", fromNodeId));
                Log.v("creden ", ToNodeId + " " + token);
                new WebTask(AcceptInviteActivity.this, Urls.acceptInvitation, valuePairs, AcceptInviteActivity.this, TaskCode.ACCEPTREQUEST).execute();
                break;
            case R.id.declineTxt:
                List<NameValuePair> valuePairs1 = new ArrayList<>();
                valuePairs1.add(new BasicNameValuePair("NodeId", MyApplication.getUserID()));
                valuePairs1.add(new BasicNameValuePair("FromNodeId", fromNodeId));
                valuePairs1.add(new BasicNameValuePair("EmailId", MyApplication.getEmailID()));
                valuePairs1.add(new BasicNameValuePair("FromEmailId", fromEmailId));
                new WebTask(AcceptInviteActivity.this, Urls.declineInviteUrl, valuePairs1, AcceptInviteActivity.this, TaskCode.DECLINEINVITE).execute();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (checkFrom) {
            startActivity(new Intent(AcceptInviteActivity.this, FamilyTreeActivity.class));
            finish();
        } else {
            finish();
        }
    }

    String fromNodeId = "", fromEmailId = "";

    @Override
    public void onComplete(String response, int taskcode) {
//        {"StatusCode":1,"Message":"Success","Invitees":[{"NodeId":70,"Name":"Jonas Ester","Image":"/source/upload/profile/thumb_male.png","Gender":"M","Email":"miranda3@rhyta.com","IsInvited":1,"InvitationSentOn":"22-10-2016","Relation":"undefined","Father":"Jacob Ester","Mother":"Annie Ester","Brother":"Julien Ester","Sister":"","Husband":"","Wife":"","Son":"","Daughter":""}]}
        if (response != null && taskcode == TaskCode.PERSONREQUEST) {
            try {
                JSONObject responseObj = new JSONObject(response);
                if (responseObj.getInt("StatusCode") == 1) {
                    JSONArray inviteeArray = responseObj.getJSONArray("Invitees");
                    JSONObject detailsObj = inviteeArray.getJSONObject(0);
                    userNameTxt.setText(detailsObj.getString("Name"));
//                    emailtextTxt.setText(detailsObj.getString("Email"));
//                    titleTxt.setText(detailsObj.getString("Name"));
//                   relationTxt.setText();
                    fromNodeId = detailsObj.getString("NodeId");
                    fromEmailId = detailsObj.getString("Email");
                    ToNodeId = detailsObj.getString("ToNodeId");
//                    MyApplication.loader.displayImage(Urls.base_Url + detailsObj.getString("Image"), profileImage, MyApplication.options);
                    MyApplication.loader.displayImage(Urls.base_Url + detailsObj.getString("Image"), profileImage, MyApplication.option4);
                } else {
                    profileImage.setImageResource(R.drawable.logo_mob);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (response != null && taskcode == TaskCode.ACCEPTREQUEST) {

            Log.v("accept_req", response);
            try {
                JSONObject resObj = new JSONObject(response);
                if (resObj.getInt("StatusCode") == 1) {
                    startActivity(new Intent(AcceptInviteActivity.this, FamilyTreeActivity.class));
                    finish();
                } else {
                    Toast.makeText(AcceptInviteActivity.this, resObj.getString("Message"), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (response != null && taskcode == TaskCode.REGISTERINVITE) {
            try {
                JSONObject responseObj = new JSONObject(response);
                if (responseObj.getString("status").equalsIgnoreCase("1")) {
                    List<NameValuePair> valuePairs = new ArrayList<>();
                    valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                    valuePairs.add(new BasicNameValuePair("Token", token));
                    valuePairs.add(new BasicNameValuePair("Email", MyApplication.getEmailID()));
                    new WebTask(AcceptInviteActivity.this, Urls.getpersondetailUrl, valuePairs, AcceptInviteActivity.this, TaskCode.PERSONREQUEST).execute();
                } else {
                    Toast.makeText(AcceptInviteActivity.this, responseObj.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (response != null && taskcode == TaskCode.DECLINEINVITE) {
            finish();
        }
    }
}
