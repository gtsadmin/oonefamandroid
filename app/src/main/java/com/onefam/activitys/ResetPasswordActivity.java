package com.onefam.activitys;
import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.onefam.R;
import com.onefam.applications.MyApplication;
import com.onefam.views.EditTextProximaNovaRegular;
import com.onefam.webutility.TaskCode;
import com.onefam.webutility.Urls;
import com.onefam.webutility.WebTask;
import com.onefam.webutility.WebTaskComplete;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
public class ResetPasswordActivity extends Activity implements WebTaskComplete {
    @Bind(R.id.welcomelogoImg)
    ImageView welcomelogoImg;
    @Bind(R.id.emailId)
    EditTextProximaNovaRegular emailId;
    @Bind(R.id.passwordEdt)
    EditTextProximaNovaRegular passwordEdt;
    @Bind(R.id.loginBtn)
    Button loginBtn;
    @Bind(R.id.loginLayout)
    LinearLayout loginLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.resetpassword_layout);
        ButterKnife.bind(this);
        loginBtn.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE)
                {
                    if (TextUtils.isEmpty(emailId.getText().toString()))
                    {
                        emailId.setError(getResources().getString(R.string.email));
                        emailId.requestFocus();
                    }else
                    {
                        List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                        valuePairs.add(new BasicNameValuePair("EmailID",emailId.getText().toString()));
//                        HashMap<String,String> params = new HashMap<>();
//                        params.put("EmailID",emailId.getText().toString());
                        if (MyApplication.isInternetWorking(ResetPasswordActivity.this)) {
                            new WebTask(ResetPasswordActivity.this, Urls.resetpasswordUrl, valuePairs, ResetPasswordActivity.this, TaskCode.RESETPASSWORDCODE).execute();
                        }
                    }
                    return true;
                }
                return false;
            }
        });
    }

    @OnClick(R.id.loginBtn)
    public void loginBtn() {
        MyApplication.getInstance().produceAnimation(loginBtn);
        if (TextUtils.isEmpty(emailId.getText().toString()))
        {
            emailId.setError(getResources().getString(R.string.email));
            emailId.requestFocus();
        }else
        {
            List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
            valuePairs.add(new BasicNameValuePair("EmailID", emailId.getText().toString()));
            if (MyApplication.isInternetWorking(ResetPasswordActivity.this)) {
                MyApplication.hideKeyBoard(ResetPasswordActivity.this);
                new WebTask(ResetPasswordActivity.this, Urls.resetpasswordUrl, valuePairs, this, TaskCode.RESETPASSWORDCODE).execute();
            }
        }
    }
    @Override
    public void onComplete(String response, int taskcode) {
                if (taskcode==TaskCode.RESETPASSWORDCODE&&response!=null)
                {
                    try {
                        JSONObject responseObj = new JSONObject(response);
                        if (responseObj.getInt("StatusCode")==1)
                        {
                          finish();
                        }else
                        {
                            Toast.makeText(ResetPasswordActivity.this,responseObj.getString("Message"),Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if(newConfig.orientation==Configuration.ORIENTATION_LANDSCAPE){
            languageCode(MyApplication.getLanguagecode());
        }else
        {
            languageCode(MyApplication.getLanguagecode());
        }
    }
    public void languageCode(String code) {
        Locale locale;
        Configuration lconfig;
        locale = new Locale(code);
        Locale.setDefault(locale);
        lconfig = new Configuration();
        lconfig.locale = locale;
        getBaseContext().getResources().updateConfiguration(lconfig,
                getBaseContext().getResources().getDisplayMetrics());
    }
}
