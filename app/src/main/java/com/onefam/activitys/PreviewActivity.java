package com.onefam.activitys;

import android.app.ProgressDialog;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.onefam.R;
import com.onefam.applications.MyApplication;
import com.onefam.datacontroller.Constants;
import com.onefam.views.TextViewProximaNovaRegular;
import com.onefam.views.TouchImageView;
import com.onefam.webutility.HttpDownloadUtility;
import com.onefam.webutility.ParseData;
import com.onefam.webutility.TaskCode;
import com.onefam.webutility.Urls;
import com.onefam.webutility.WebTaskComplete;
import com.onefam.wrapper.GetProfileMediaWrapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class PreviewActivity extends AppCompatActivity implements WebTaskComplete {
    @Bind(R.id.titleTxt)
    TextViewProximaNovaRegular titleTxt;
    @Bind(R.id.backLayout)
    LinearLayout backLayout;
    @Bind(R.id.topbarLayout)
    RelativeLayout topbarLayout;
    @Bind(R.id.videoView)
    VideoView videoView;
    int title_num, position;
    ArrayList<GetProfileMediaWrapper> getProfileMediaWrappers;
    @Bind(R.id.imageViewPager)
    ViewPager imageViewPager;
    MediaPlayer mplayer;
    @Bind(R.id.downloadImg)
    ImageView downloadImg;
    String fileUrl;
    MediaPlayer player;
    @Bind(R.id.thumbImg)
    TouchImageView thumbImg;
    int activity_code;
    @Bind(R.id.loadBar)
    ProgressBar loadBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preview_layout);
        ButterKnife.bind(this);
        try {
            title_num = getIntent().getExtras().getInt("title");
            position = getIntent().getExtras().getInt("pos");
            activity_code = getIntent().getIntExtra("activitycode", 0);
            if (activity_code == Constants.PROFILEACTIVITYCODE) {


                if (title_num == Constants.VIDEO) {
                    getProfileMediaWrappers = (ArrayList<GetProfileMediaWrapper>) getIntent().getSerializableExtra("model");
                    fileUrl = Urls.profilemedia_basevideoUrl + getProfileMediaWrappers.get(position).getFileName();
                    playVideos();
                } else {

                    getProfileMediaWrappers = (ArrayList<GetProfileMediaWrapper>) getIntent().getSerializableExtra("model");
                    fileUrl = Urls.profilemedia_basephotoUrl + getProfileMediaWrappers.get(position).getFileName();
                    MyPagerAdapter myPagerAdapter = new MyPagerAdapter();
                    imageViewPager.setAdapter(myPagerAdapter);
                    imageViewPager.setCurrentItem(position);
                    titleTxt.setText(getResources().getString(R.string.photos));
                }
//                List<NameValuePair> valuePairs = new ArrayList<>();
//                valuePairs.add(new BasicNameValuePair("NodeId", getIntent().getExtras().getString("id")));
//                valuePairs.add(new BasicNameValuePair("AlbumId", ""));
//                valuePairs.add(new BasicNameValuePair("StartIndex", "1"));
//                valuePairs.add(new BasicNameValuePair("PageSize", "25"));
//
//
//
//                if (title_num == Constants.PHOTO) {
//                    titleTxt.setText(getResources().getString(R.string.photos));
////               titlegridTxt.setText(getResources().getString(R.string.photos));
//                    Drawable drawable = getResources().getDrawable(R.mipmap.icn_photos);
////               iconImg.setImageDrawable(drawable);
//                    if (MyApplication.isInternetWorking(PreviewActivity.this)) {
//                        new WebTask(PreviewActivity.this, Urls.ViewAllImages, valuePairs, this, TaskCode.ALLIMAGECODE).execute();
//                    }
//                } else if (title_num == Constants.VIDEO) {
//                    Drawable drawable = getResources().getDrawable(R.mipmap.icn_video);
////               iconImg.setImageDrawable(drawable);
//                    titleTxt.setText(getResources().getString(R.string.videos));
////               titlegridTxt.setText(getResources().getString(R.string.videos));
//                    if (MyApplication.isInternetWorking(PreviewActivity.this)) {
//
//                        new WebTask(PreviewActivity.this, Urls.ViewAllVideos, valuePairs, this, TaskCode.ALLVIDEOSCODE).execute();
//                    }
//
//
//                }
            } else {
                getProfileMediaWrappers = new ArrayList<>();
                getProfileMediaWrappers.addAll(PhotosVideosActivity.getProfileMediaWrappers);
                fileUrl = Urls.profilemedia_basevideoUrl + getProfileMediaWrappers.get(position).getFileName();
                if (title_num == Constants.VIDEO) {
                    playVideos();
                } else {
                    fileUrl = Urls.profilemedia_basephotoUrl + getProfileMediaWrappers.get(position).getFileName();
                    MyPagerAdapter myPagerAdapter = new MyPagerAdapter();
                    imageViewPager.setAdapter(myPagerAdapter);
                    imageViewPager.setCurrentItem(position);
                    titleTxt.setText(getResources().getString(R.string.photos));
                }
            }


//            if (title_num == Constants.VIDEO) {
//                playVideos();
//            } else if (title_num == Constants.PHOTO) {
//                if (activity_code==Constants.PROFILEACTIVITYCODE) {
//                    thumbImg.setVisibility(View.VISIBLE);
//                    MyApplication.loader.displayImage(fileUrl, thumbImg, MyApplication.options);
//
//                    titleTxt.setText(getResources().getString(R.string.photos));
//                } else {
//                    MyPagerAdapter myPagerAdapter = new MyPagerAdapter();
//                    imageViewPager.setAdapter(myPagerAdapter);
//                    imageViewPager.setCurrentItem(position);
//                    titleTxt.setText(getResources().getString(R.string.photos));
//                }
//
//            } else if (title_num == Constants.AUDIO) {
//
//                titleTxt.setText("Audio");
//                try {
//
//                    player = new MediaPlayer();
//                    player.setAudioStreamType(AudioManager.STREAM_MUSIC);
//                    player.setDataSource(fileUrl);
//                    player.prepare();
//                    player.start();
//
//                } catch (Exception e) {
//                    // TODO: handle exception
//                }
//
//
//            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        imageViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                fileUrl = Urls.profilemedia_basephotoUrl + getProfileMediaWrappers.get(position).getFileName();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @OnClick(R.id.backLayout)
    public void backLayout() {
        onBackPressed();
    }

    @OnClick(R.id.downloadImg)
    public void downloadImg() {
        if (MyApplication.isInternetWorking(PreviewActivity.this)) {

            new DownloadTask().execute();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (player != null) {
            if (player.isPlaying()) {
                player.stop();
            }
        }
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (player != null) {
            if (player.isPlaying()) {
                player.stop();
            }
        }
    }

    @Override
    public void onComplete(String response, int taskcode) {
        if (taskcode == TaskCode.ALLIMAGECODE) {
            getProfileMediaWrappers = new ParseData(PreviewActivity.this, response, TaskCode.ALLIMAGECODE).getAllImage();
            MyPagerAdapter myPagerAdapter = new MyPagerAdapter();
            imageViewPager.setAdapter(myPagerAdapter);
            imageViewPager.setCurrentItem(position);
            titleTxt.setText(getResources().getString(R.string.photos));

        } else if (taskcode == TaskCode.ALLVIDEOSCODE) {
            getProfileMediaWrappers = new ParseData(PreviewActivity.this, response, TaskCode.ALLVIDEOSCODE).getAllVideos();

            playVideos();
        }
    }

    private class MyPagerAdapter extends PagerAdapter {


        @Override
        public int getCount() {
            return getProfileMediaWrappers.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {


            TouchImageView imageView = new TouchImageView(PreviewActivity.this);
//            imageView.setImageResource(res[position]);
            final ProgressBar loadBar = new ProgressBar(PreviewActivity.this);
            final RelativeLayout.LayoutParams imageParams = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

            imageParams.addRule(RelativeLayout.CENTER_IN_PARENT);
            loadBar.setLayoutParams(imageParams);
            imageView.setLayoutParams(imageParams);
//            Urls.profilemedia_basephotoUrl + getProfileMediaWrappers.get(1).getFileName()
            MyApplication.loader.displayImage(Urls.profilemedia_basephotoUrl + getProfileMediaWrappers.get(position).getFileName(), imageView, MyApplication.options
                    , new

                            SimpleImageLoadingListener() {
                                @Override
                                public void onLoadingStarted(String imageUri, View view) {
                                    super.onLoadingStarted(imageUri, view);
                                    loadBar.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                    super.onLoadingFailed(imageUri, view, failReason);
                                    loadBar.setVisibility(View.GONE);

                                }

                                @Override
                                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                    super.onLoadingComplete(imageUri, view, loadedImage);
                                    loadBar.setVisibility(View.GONE);
                                }

                                @Override
                                public void onLoadingCancelled(String imageUri, View view) {
                                    super.onLoadingCancelled(imageUri, view);
                                    loadBar.setVisibility(View.GONE);
                                }
                            });
            LinearLayout layout = new LinearLayout(PreviewActivity.this);

            layout.addView(imageView);

            final int page = position;

            container.addView(layout);
            return layout;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }

    }

    public class DownloadTask extends AsyncTask<Void, Void, Void> {
        ProgressDialog dialog;

        @Override
        protected Void doInBackground(Void... params) {
            String PATH = Environment.getExternalStorageDirectory().toString();
            try {
                HttpDownloadUtility.downloadFile(fileUrl, PATH);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dialog.dismiss();
            Toast.makeText(PreviewActivity.this, "Downloaded Successfully", Toast.LENGTH_SHORT).show();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(PreviewActivity.this, "", "Please wait...");
        }
    }

    private void playVideos() {
        Drawable drawable = getResources().getDrawable(R.mipmap.icn_video);
//        final ProgressDialog dialog = ProgressDialog.show(PreviewActivity.this, "", "Loading....");
        loadBar.setVisibility(View.VISIBLE);
        titleTxt.setText(getResources().getString(R.string.videos));
        try {
            if (mplayer == null) {
                mplayer = new MediaPlayer();
            }
            final MediaController mediaController = new MediaController(this);

            final Uri video = Uri.parse(Urls.profilemedia_basevideoUrl + getProfileMediaWrappers.get(position).getFileName());

            videoView.setVideoURI(video);


            videoView.setMediaController(mediaController);
            mediaController.setPrevNextListeners(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getProfileMediaWrappers != null) {
                        if (getProfileMediaWrappers.size() > 0) {
                            if (position < getProfileMediaWrappers.size()) {
                                mplayer.reset();
                                fileUrl = Urls.profilemedia_basevideoUrl + getProfileMediaWrappers.get(position).getFileName();
                                Uri video = Uri.parse(Urls.profilemedia_basevideoUrl + getProfileMediaWrappers.get(position).getFileName());
                                position = position + 1;
                                videoView.setVideoURI(video);
                                videoView.start();
                            }
                        }
                    }
                }
            }, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getProfileMediaWrappers != null) {
                        if (getProfileMediaWrappers.size() > 0) {
                            if (position > 0) {
                                mplayer.reset();
                                position = position - 1;
                                fileUrl = Urls.profilemedia_basevideoUrl + getProfileMediaWrappers.get(position).getFileName();
                                Uri video = Uri.parse(Urls.profilemedia_basevideoUrl + getProfileMediaWrappers.get(position).getFileName());
                                videoView.setVideoURI(video);
                                videoView.start();
                            }
                        }
                    }
                }
            });
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {

                    mplayer = mp;
                    loadBar.setVisibility(View.GONE);
//                    dialog.dismiss();
//
//                        video_load_bar.setVisibility(View.GONE);
                    mp.start();
                    mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                        @Override
                        public void onVideoSizeChanged(MediaPlayer mp, int arg1,
                                                       int arg2) {
                            // TODO Auto-generated method stub
                            mplayer = mp;

                            System.out.println("arg1" + arg1);
                            System.out.println("arg2" + arg2);
//                                video_load_bar.setVisibility(View.GONE);
                            mp.start();
                            mediaController.show(90000000);
                            //  new MyAsync().execute();

                        }
                    });


                }
            });
            videoView.start();
            mediaController.show(90000000);
            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    if (position < getProfileMediaWrappers.size()) {
                        mplayer.reset();
                        Uri video = Uri.parse(Urls.profilemedia_basevideoUrl + getProfileMediaWrappers.get(position).getFileName());
                        position = position + 1;
                        videoView.setVideoURI(video);
                        videoView.start();
                    }
                }
            });
//                videoView.
//                mplayer.set
        } catch (Exception ex) {

        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            languageCode(MyApplication.getLanguagecode());
        } else {
            languageCode(MyApplication.getLanguagecode());
        }
    }

    public void languageCode(String code) {
        Locale locale;
        Configuration lconfig;
        locale = new Locale(code);
        Locale.setDefault(locale);
        lconfig = new Configuration();
        lconfig.locale = locale;
        getBaseContext().getResources().updateConfiguration(lconfig,
                getBaseContext().getResources().getDisplayMetrics());
    }
}
