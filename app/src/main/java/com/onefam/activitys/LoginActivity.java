package com.onefam.activitys;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.onefam.R;
import com.onefam.applications.MyApplication;
import com.onefam.gcm.RegistrationIntentService;
import com.onefam.views.EditTextProximaNovaRegular;
import com.onefam.webutility.TaskCode;
import com.onefam.webutility.Urls;
import com.onefam.webutility.WebTask;
import com.onefam.webutility.WebTaskComplete;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends Activity implements WebTaskComplete {
    @Bind(R.id.welcomelogoImg)
    ImageView welcomelogoImg;
    @Bind(R.id.emailId)
    EditTextProximaNovaRegular emailId;
    @Bind(R.id.passwordEdt)
    EditTextProximaNovaRegular passwordEdt;
    @Bind(R.id.loginBtn)
    Button loginBtn;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);
        ButterKnife.bind(this);
        passwordEdt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    MyApplication.getInstance().hideSoftKeyBoard(LoginActivity.this);
                    if (TextUtils.isEmpty(emailId.getText().toString())) {
                        emailId.setError(getString(R.string.Pleaseentervalidemailid));
                        emailId.requestFocus();
                    } else if (!MyApplication.isValidEmail(emailId.getText().toString())) {
                        emailId.setError(getResources().getString(R.string.Pleaseentervalidemailid));
                        emailId.requestFocus();
                    } else if (TextUtils.isEmpty(passwordEdt.getText().toString())) {
                        passwordEdt.setError(getString(R.string.please_enter_password));
                        passwordEdt.requestFocus();
                    } else {
                        List<NameValuePair> valuePairs = new ArrayList<>();
                        valuePairs.add(new BasicNameValuePair("EmailID", emailId.getText().toString()));
                        valuePairs.add(new BasicNameValuePair("Password", passwordEdt.getText().toString()));
                        valuePairs.add(new BasicNameValuePair("Token", MyApplication.getTokenID()));
                        valuePairs.add(new BasicNameValuePair("Language", MyApplication.getLanguagecode()));
                        if (MyApplication.isInternetWorking(LoginActivity.this)) {
                            new WebTask(LoginActivity.this, Urls.loginUrl, valuePairs, LoginActivity.this, TaskCode.LOGINCODE).execute();
                        }

                    }
                    return true;
                }
                return false;
            }
        });
        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }

        emailId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    emailId.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @OnClick(R.id.loginBtn)
    public void loginBtn() {
        MyApplication.myApplication.produceAnimation(loginBtn);
        if (TextUtils.isEmpty(emailId.getText().toString())) {
            emailId.setError(getResources().getString(R.string.Pleaseentervalidemailid));
            emailId.requestFocus();
        } else if (!MyApplication.isValidEmail(emailId.getText().toString())) {
            emailId.setError(getResources().getString(R.string.Pleaseentervalidemailid));
            emailId.requestFocus();
        } else if (TextUtils.isEmpty(passwordEdt.getText().toString())) {
            passwordEdt.setError(getResources().getString(R.string.please_enter_password));
            passwordEdt.requestFocus();
        } else {
            List<NameValuePair> valuePairs = new ArrayList<>();
            valuePairs.add(new BasicNameValuePair("EmailID", emailId.getText().toString()));
            valuePairs.add(new BasicNameValuePair("Password", passwordEdt.getText().toString()));
            valuePairs.add(new BasicNameValuePair("Token", MyApplication.getTokenID()));
            valuePairs.add(new BasicNameValuePair("Language", MyApplication.getLanguagecode()));
            if (MyApplication.isInternetWorking(LoginActivity.this)) {

                new WebTask(LoginActivity.this, Urls.loginUrl, valuePairs, this, TaskCode.LOGINCODE).execute();
            }
        }


    }

    @Override
    public void onComplete(String response, int taskcode) {
        if (taskcode == TaskCode.LOGINCODE) {
            System.out.println("response" + response);
            if (response != null && taskcode == TaskCode.LOGINCODE) {
                try {
                    JSONObject responseObj = new JSONObject(response);
                    if (responseObj.getInt("StatusCode") == 1) {
                        MyApplication.saveUserId(responseObj.getString("NodeId"));
                        MyApplication.saveEmailId(emailId.getText().toString());
                        MyApplication.savePassword(passwordEdt.getText().toString());
                        MyApplication.saveUserFirstName(responseObj.getString("FirstName"));
                        startActivity(new Intent(LoginActivity.this, FamilyTreeActivity.class));
                        finish();
                    } else {
                        Toast.makeText(LoginActivity.this, responseObj.getString("Message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            languageCode(MyApplication.getLanguagecode());
        } else {
            languageCode(MyApplication.getLanguagecode());
        }
    }

    public void languageCode(String code) {
        Locale locale;
        Configuration lconfig;
        locale = new Locale(code);
        Locale.setDefault(locale);
        lconfig = new Configuration();
        lconfig.locale = locale;
        getBaseContext().getResources().updateConfiguration(lconfig,
                getBaseContext().getResources().getDisplayMetrics());
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
//                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
}
