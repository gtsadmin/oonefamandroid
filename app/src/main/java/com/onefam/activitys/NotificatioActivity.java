package com.onefam.activitys;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.onefam.R;
import com.onefam.applications.MyApplication;
import com.onefam.views.TextViewProximaNovaRegular;
import com.onefam.webutility.ParseData;
import com.onefam.webutility.TaskCode;
import com.onefam.webutility.Urls;
import com.onefam.webutility.WebTask;
import com.onefam.webutility.WebTaskComplete;
import com.onefam.wrapper.NotificationWrapper;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class NotificatioActivity extends AppCompatActivity implements WebTaskComplete {
    @Bind(R.id.backLayout)
    LinearLayout backLayout;
    @Bind(R.id.topbarLayout)
    RelativeLayout topbarLayout;
    @Bind(R.id.notificationListview)
    ListView notificationListview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_layout);
        ButterKnife.bind(this);
        String notificationAPI = Urls.base_Url + "apiNotifications.ashx?UserId=" + MyApplication.getUserID() + "&Language=" + MyApplication.getLanguagecode();
        if (MyApplication.isInternetWorking(NotificatioActivity.this)) {
            new WebTask(NotificatioActivity.this, notificationAPI, null, this, TaskCode.NOTIFICATIONCODE).execute();
        }


    }

    @OnClick(R.id.backLayout)
    public void backLayout() {
        finish();
    }

    @Override
    public void onComplete(String response, int taskcode) {
        if (FamilyTreeActivity.FamilyTreeActivityObj != null) {
            MyApplication.showUnreadNotificationStatus(false);
            FamilyTreeActivity.FamilyTreeActivityObj.changeNotificationImage();
        }

        if (response != null && taskcode == TaskCode.NOTIFICATIONCODE) {

            Log.v("notification_data ", response);
            ArrayList<NotificationWrapper> notificationWrappers = new ParseData(NotificatioActivity.this, response, taskcode).getNotifications();
            NotificationAdapter notificationAdapter = new NotificationAdapter(NotificatioActivity.this, notificationWrappers);
            notificationListview.setAdapter(notificationAdapter);

            if (notificationWrappers.isEmpty()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(NotificatioActivity.this);
                builder.setTitle("" + getResources().getString(R.string.app_name));
                builder.setMessage(getResources().getString(R.string.Youhavenonotification))
                        .setCancelable(false)
                        .setPositiveButton(getResources().getString(R.string.OK),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                builder.show();
            }

        }
    }

    public class NotificationAdapter extends BaseAdapter {
        ArrayList<NotificationWrapper> notificationWrappers;
        Context context;

        public NotificationAdapter(Context context, ArrayList<NotificationWrapper> notificationWrappers) {
            this.context = context;
            this.notificationWrappers = notificationWrappers;
        }

        @Override
        public int getCount() {
            return notificationWrappers.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            ViewHolder viewHolder = null;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = inflater.inflate(R.layout.notification_list_layout, null);
                convertView.setTag(viewHolder);
                viewHolder.nameTxt = (TextViewProximaNovaRegular) convertView.findViewById(R.id.nameTxt);
                viewHolder.messageTxt = (TextViewProximaNovaRegular) convertView.findViewById(R.id.messageTxt);
                viewHolder.profileImg = (ImageView) convertView.findViewById(R.id.profileImg);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            Log.v("profile_pic ", notificationWrappers.get(position).getProfilePic());

            MyApplication.loader.displayImage(Urls.base_Url + notificationWrappers.get(position).getProfilePic(), viewHolder.profileImg, MyApplication.options);
            viewHolder.messageTxt.setText(notificationWrappers.get(position).getNotificationMessage());
            viewHolder.nameTxt.setText(notificationWrappers.get(position).getName());
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent profile_intent = new Intent(NotificatioActivity.this, ProfileActivity.class).putExtra("id", notificationWrappers.get(position).getID());
                    startActivity(profile_intent);
                }
            });
            return convertView;
        }

        public class ViewHolder {
            ImageView profileImg;
            TextViewProximaNovaRegular nameTxt, messageTxt, dobTxt, dodTxt;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            languageCode(MyApplication.getLanguagecode());
        } else {
            languageCode(MyApplication.getLanguagecode());
        }
    }

    public void languageCode(String code) {
        Locale locale;
        Configuration lconfig;
        locale = new Locale(code);
        Locale.setDefault(locale);
        lconfig = new Configuration();
        lconfig.locale = locale;
        getBaseContext().getResources().updateConfiguration(lconfig,
                getBaseContext().getResources().getDisplayMetrics());
    }
}
