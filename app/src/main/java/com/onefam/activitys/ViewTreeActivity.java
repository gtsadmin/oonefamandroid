package com.onefam.activitys;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.onefam.R;
import com.onefam.applications.MyApplication;
import com.onefam.datacontroller.AppGlobalData;
import com.onefam.views.EditTextProximaNovaRegular;
import com.onefam.views.TextViewProximaNovaRegular;
import com.onefam.webutility.TaskCode;
import com.onefam.webutility.Urls;
import com.onefam.webutility.WebTask;
import com.onefam.webutility.WebTaskComplete;
import com.onefam.wrapper.RelativeWrapper;
import com.onefam.wrapper.TreeWrapper;


import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.onefam.R.string.settings;


public class ViewTreeActivity extends AppCompatActivity implements WebTaskComplete {
    @Bind(R.id.backLayout)
    LinearLayout backLayout;
    @Bind(R.id.topbarLayout)
    RelativeLayout topbarLayout;
    @Bind(R.id.searchLayout)
    RelativeLayout searchLayout;
    @Bind(R.id.viewfamilyLayout)
    LinearLayout viewfamilyLayout;
    @Bind(R.id.addmemberLayout)
    LinearLayout addmemberLayout;
    @Bind(R.id.bottomLayout)
    LinearLayout bottomLayout;
    int line_width = 0, view_width = 0;
    LinearLayout inflatehorizontallLayout;

    HashMap<String, ArrayList<RelativeWrapper>> relativeHashMap = new HashMap<>();
    @Bind(R.id.webView)
    WebView webView;
    @Bind(R.id.inflateveticalLayout)
    LinearLayout inflateveticalLayout;
    @Bind(R.id.searchEdt)
    TextViewProximaNovaRegular searchEdt;
    int view_layout = 0;
    @Bind(R.id.progressBar)
    ProgressBar progressBar;
    Dialog dialog;
    RelativeLayout webLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.view_tree_layout);
        AppGlobalData.reloadwebViewBoolean = true;

        ButterKnife.bind(this);
        inflateveticalLayout = (LinearLayout) findViewById(R.id.inflateveticalLayout);
        webLayout = (RelativeLayout) findViewById(R.id.webLayout);

        try {
            FamilyTreeActivity.FamilyTreeActivityObj.changeNotificationImage();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.viewfamilyLayout)
    public void viewfamilyLayout() {
        MyApplication.myApplication.produceAnimation(viewfamilyLayout);
        startActivity(new Intent(ViewTreeActivity.this, FamilyActivity.class));
    }

    @OnClick(R.id.addmemberLayout)
    public void addmemberLayout() {
        MyApplication.myApplication.produceAnimation(addmemberLayout);
        startActivity(new Intent(ViewTreeActivity.this, AddMember.class));
    }

    @OnClick(R.id.searchEdt)
    public void searchEdt() {
        startActivity(new Intent(ViewTreeActivity.this, PeopleSearchActivity.class));
    }

    @OnClick(R.id.backLayout)
    public void backLayout() {
        finish();
    }


    public void deletePopup(final String nodeid) {
        AlertDialog.Builder alert = new AlertDialog.Builder(
                ViewTreeActivity.this);

        alert.setMessage(getResources().getString(R.string.Areyousureyouwanttodelete));
        alert.setPositiveButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert.setNegativeButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                List<NameValuePair> valuePairs = new ArrayList<>();
                valuePairs.add(new BasicNameValuePair("NodeId", nodeid));
                valuePairs.add(new BasicNameValuePair("CanDelete", "1"));
                valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                if (MyApplication.isInternetWorking(ViewTreeActivity.this)) {
                    new WebTask(ViewTreeActivity.this, Urls.deleteMember, valuePairs, ViewTreeActivity.this, TaskCode.DELETEMEMBERCODE).execute();
                }
            }
        });
        alert.show();
    }

    public void showAlret(String msg) {
        AlertDialog.Builder alert = new AlertDialog.Builder(
                ViewTreeActivity.this);

        alert.setMessage(msg);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });


        alert.show();
    }

    @Override
    public void onComplete(String response, int taskcode) {

        if (response != null && taskcode == TaskCode.DELETEMEMBERCODE) {

            try {
                JSONObject responseObj = new JSONObject(response);
                if (responseObj.getInt("StatusCode") == 1) {
                    if (MyApplication.isInternetWorking(ViewTreeActivity.this)) {
                        getTagMemberList();

//                        String url = Urls.treeviewUrl + "?EmailId=" + MyApplication.getEmailID() + "&Password=" + MyApplication.getPassword() + "&NodeId=" + MyApplication.getUserID();
//
//                        webView.setWebViewClient(new MyBrowser());
////                      webView.getSettings().setLoadsImagesAutomatically(true);
////                      webView.getSettings().setLoadsImagesAutomatically(true);
//                        webView.getSettings().setJavaScriptEnabled(true);
//                        webView.setInitialScale(1);
//                        webView.getSettings().setLoadWithOverviewMode(true);
//                        webView.getSettings().setUseWideViewPort(true);
////                      webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
//                        webView.getSettings().setBuiltInZoomControls(true);
//                        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
//                        webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
//                        webView.getSettings().setCacheMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
//                        webView.getSettings().setDomStorageEnabled(true);
//                        if (Build.VERSION.SDK_INT >= 11) {
//                            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
//                        }
//                        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
//                            // Hide the zoom controls for HONEYCOMB+
//                            webView.getSettings().setDisplayZoomControls(false);
//                        }
//                        webView.loadUrl(url);

                        loadTreeView();


                    }
                } else {
                    Toast.makeText(ViewTreeActivity.this, responseObj.getString("Message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (response != null && taskcode == TaskCode.TAGMEMBER) {
            Log.v("tagMemberList ", response);
            parseAllMemberData(response);
        }
    }

    private class MyBrowser extends WebViewClient {
//        ProgressDialog dialog = ProgressDialog.show(ViewTreeActivity.this, "", "Please wait...");

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
            AppGlobalData.reloadwebViewBoolean = false;
//            webView.scrollTo(1200,1000);
//            webView.setInitialScale(100);
//            new Handler().postDelayed(new Runnable() {
//
//                @Override
//                public void run() {
//                    progressBar.setVisibility(View.GONE);
//                    AppGlobalData.reloadwebViewBoolean = false;
//                    webView.scrollTo(10,10);
//                    webView.setInitialScale(1);
//
//                }
//            }, 5000);


        }


        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);
            AlertDialog.Builder builder = new AlertDialog.Builder(ViewTreeActivity.this);
            builder.setMessage("Ssl Error");
            builder.setPositiveButton(getResources().getString(R.string.continue_), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.proceed();
                }
            });
            builder.setNegativeButton(getResources().getString(R.string.Cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.cancel();
                }
            });
            final AlertDialog dialog = builder.create();
            dialog.show();

//            handler.proceed();
        }

        @Override
        public void onReceivedError(WebView view, int errorCod, String description, String failingUrl) {
            Toast.makeText(ViewTreeActivity.this, "Your Internet Connection May not be active Or " + description, Toast.LENGTH_LONG).show();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            view.loadUrl(url);
            try {
                System.out.println("url" + url);

//            http://localhost/AddMember/8
                if (url != null && !url.equalsIgnoreCase("") && url.contains("localhost")) {
                    String urlArr[] = url.split("localhost/");
                    String url2Arr[] = urlArr[1].split("/");
                    if (url2Arr[0].equalsIgnoreCase("AddMember")) {
                        startActivity(new Intent(ViewTreeActivity.this, AddMember.class).putExtra("nodeid", url2Arr[1]));
                    } else if (url2Arr[0].equalsIgnoreCase("EditMember")) {
//                startActivity(new Intent(ViewTreeActivity.this, AddMember.class).);
                        AppGlobalData.editmemeberBoolean = true;
                        startActivity(new Intent(ViewTreeActivity.this, AddMember.class).putExtra("nodeid", url2Arr[1]));

                    } else if (url2Arr[0].equalsIgnoreCase("DeleteMember")) {
                        if (!url2Arr[2].equalsIgnoreCase("-1")) {
                            Log.v("delete_id ", url2Arr[2] + " " + url2Arr[1]);
                            deletePopup(url2Arr[1]);
                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(
                                    ViewTreeActivity.this);

                            alert.setMessage(getResources().getString(R.string.Youarenotallowedtodelete));
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            alert.show();
                        }
                    } else if (url2Arr[0].equalsIgnoreCase("ViewProfile")) {

                        Intent profile_intent = new Intent(ViewTreeActivity.this, ProfileActivity.class).putExtra("id", url2Arr[1]);
                        startActivity(profile_intent);
                    } else if (url2Arr[0].equalsIgnoreCase("SendMail")) {
                        sendInviteDialog(url2Arr[1], "", AppGlobalData.getInstance().getMembernamelistMap().get(url2Arr[1].toString()));

                        /*node  AppGlobalData.getInstance().getMamberListMap().get(selectfamilymamberSpinner.getSelectedItem().toString()) */

                        Log.v("name_of_invitation ", AppGlobalData.getInstance().getMembernamelistMap().get(url2Arr[1].toString() + ""));
//                        sendInviteDialog(url2Arr[1]);
                    } else if (url2Arr[0].equalsIgnoreCase("ChangeTree")) {
                        MyApplication.setViewFamilyTreeId(url2Arr[1]);
                        //url2Arr[1] to be saved
                        /*show loader*/
                        progressBar.setVisibility(View.VISIBLE);
                    } else if (url2Arr[0].equalsIgnoreCase("Stop")) {
                        /*hide loader*/
                        progressBar.setVisibility(View.GONE);
                    }
                } else {
                    loadTreeView();
                    return false;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                loadTreeView();
            }
            return true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (AppGlobalData.reloadwebViewBoolean == true) {
            if (MyApplication.isInternetWorking(ViewTreeActivity.this)) {
                loadTreeView();
            }
        }
    }

    public void sendInviteDialog(final String id) {
        dialog = new Dialog(ViewTreeActivity.this, R.style.Theme_Custom);
        dialog.setContentView(R.layout.send_inivitation_layout);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        TextViewProximaNovaRegular cancelTxt = (TextViewProximaNovaRegular) dialog.findViewById(R.id.cancelTxt);
        TextViewProximaNovaRegular inviteTxt = (TextViewProximaNovaRegular) dialog.findViewById(R.id.inviteTxt);
        final EditTextProximaNovaRegular emailEdt = (EditTextProximaNovaRegular) dialog.findViewById(R.id.emailEdt);
        cancelTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        inviteTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MyApplication.isInternetWorking(ViewTreeActivity.this)) {
                    if (TextUtils.isEmpty(emailEdt.getText().toString())) {
                        emailEdt.setError(getResources().getString(R.string.please_enter_email));
                        emailEdt.requestFocus();
                    } else {
                        List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                        valuePairs.add(new BasicNameValuePair("NodeId", id));
                        valuePairs.add(new BasicNameValuePair("EmailAddress", emailEdt.getText().toString()));
                        valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                        new WebTask(ViewTreeActivity.this, Urls.sendinvitationAPI, valuePairs, new WebTaskComplete() {
                            @Override
                            public void onComplete(String response, int taskcode) {
                                try {

                                    JSONObject responseObj = new JSONObject(response);
                                    if (responseObj.getInt("StatusCode") == 1) {
                                        if (dialog != null) {
                                            dialog.dismiss();
                                            showAlret(responseObj.getString("Message"));
                                        }
                                    } else {
                                        dialog.dismiss();
                                        showAlret(responseObj.getString("Message"));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }, TaskCode.SENDINVITATION).execute();
                    }
                }
            }
        });
    }

    private void loadTreeView() {
        /*for clearing the view */
        MyApplication.setViewFamilyTreeId("");
        webView.loadUrl("about:blank");
        /*===============*/

        String encodeEmail = null;
        String encodePassword = null;
        try {
            encodeEmail = URLEncoder.encode(MyApplication.getEmailID(), "UTF-8");
            encodePassword = URLEncoder.encode(MyApplication.getPassword(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String query = "?EmailId=" + encodeEmail + "&Password=" + encodePassword + "&NodeId=" + MyApplication.getUserID() + "&Language=" + MyApplication.getLanguagecode();
        String url = Urls.treeviewUrl + query;
        Log.v("encode-string ", url);

//        String url = Urls.treeviewUrl + "?EmailId=" + MyApplication.getEmailID() + "&Password=" + MyApplication.getPassword() + "&NodeId=" + MyApplication.getUserID() + "&Language=" + MyApplication.getLanguagecode();

        webView.setWebViewClient(new MyBrowser());
        webView.getSettings().setJavaScriptEnabled(true);
//        webView.scrollTo(0,100);
//        webView.getSettings().setAppCacheMaxSize( 5 * 1024 * 1024 ); // 5MB
//        webView.getSettings().setAppCachePath(getApplicationContext().getCacheDir().getAbsolutePath());
//        webView.getSettings().setAllowFileAccess(true);
//        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
// remove a weird white line on the right size
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);
//        webView.setLayoutParams(new TableRow.LayoutParams(
//        screenWidth / 2, TableRow.LayoutParams.MATCH_PARENT, 1f));
//        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT);
//        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, 1);
//        webLayout.addView(webView, layoutParams);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webView.getSettings().setCacheMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setDefaultTextEncodingName("UTF-8");
//        webView.getSettings().setCacheMode( WebSettings.LOAD_DEFAULT );
        if (Build.VERSION.SDK_INT >= 11) {
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
            webView.getSettings().setDisplayZoomControls(false);
        }
        webView.loadUrl(url);

    }

    @Override
    public File getCacheDir() {
        // NOTE: this method is used in Android 2.1
        return getApplicationContext().getCacheDir();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            languageCode(MyApplication.getLanguagecode());
        } else {
            languageCode(MyApplication.getLanguagecode());
        }
    }

    public void languageCode(String code) {
        Locale locale;
        Configuration lconfig;
        locale = new Locale(code);
        Locale.setDefault(locale);
        lconfig = new Configuration();
        lconfig.locale = locale;
        getBaseContext().getResources().updateConfiguration(lconfig,
                getBaseContext().getResources().getDisplayMetrics());
    }

    private int getScale() {
        Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int width = display.getWidth();
        Double val = new Double(width) / new Double(50000);
        val = val * 100d;
        return val.intValue();
    }

    Dialog dialog1;

    public void sendInviteDialog(final String nodeid, final String EmailAddress, String name) {
        dialog = new Dialog(ViewTreeActivity.this, R.style.Theme_Custom);
        dialog.setContentView(R.layout.send_invitation_popup);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        TextViewProximaNovaRegular cancelTxt = (TextViewProximaNovaRegular) dialog.findViewById(R.id.cancelTxt);
        TextViewProximaNovaRegular inviteTxt = (TextViewProximaNovaRegular) dialog.findViewById(R.id.inviteTxt);
        TextViewProximaNovaRegular nameTxt = (TextViewProximaNovaRegular) dialog.findViewById(R.id.nameTxt);
        TextViewProximaNovaRegular sendinvitationTxt = (TextViewProximaNovaRegular) dialog.findViewById(R.id.inviteTxt);
//        nameTxt.setText(nameTxt.getText().toString());
        nameTxt.setText(name);
        final EditTextProximaNovaRegular emailEdt = (EditTextProximaNovaRegular) dialog.findViewById(R.id.emailEdt);
        ImageView facebookImg = (ImageView) dialog.findViewById(R.id.facebookImg);
        ImageView smsImg = (ImageView) dialog.findViewById(R.id.smsImg);
        ImageView whatsappImg = (ImageView) dialog.findViewById(R.id.whatsappImg);
        ImageView wechatImg = (ImageView) dialog.findViewById(R.id.wechatImg);
        ImageView viberImg = (ImageView) dialog.findViewById(R.id.viberImg);
        viberImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().hideSoftKeyBoard(ViewTreeActivity.this);
                List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                valuePairs.add(new BasicNameValuePair("NodeId", nodeid));
                String url = "";
                if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                    valuePairs.add(new BasicNameValuePair("EmailAddress", EmailAddress));
                    valuePairs.add(new BasicNameValuePair("NewEmailAddress", emailEdt.getText().toString()));
                    url = Urls.mobileinviteUrl;
                } else {
                    url = Urls.mobileinviteUrl;
                }
                valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                new WebTask(ViewTreeActivity.this, url, valuePairs, new WebTaskComplete() {
                    @Override
                    public void onComplete(String response, int taskcode) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("StatusCode") == 1) {
                                String inviteUrl = MyApplication.getUserFirstName() + " " + getResources().getString(R.string.invitation_message) + " " + responseObj.getString("InvitationLink");
//                                Intent i = new Intent(Intent.ACTION_SEND);
//                                i.setPackage("com.viber.voip");
//                                i.setType("text/plain");
//                                i.putExtra(Intent.EXTRA_TEXT, inviteUrl);
                                PackageManager pm = getPackageManager();
                                Intent waIntent = new Intent(Intent.ACTION_SEND);
                                waIntent.setType("text/plain");
                                PackageInfo info = pm.getPackageInfo("com.viber.voip", PackageManager.GET_META_DATA);
                                //Check if package exists or not. If not then code
                                //in catch block will be called
                                waIntent.setPackage("com.viber.voip");
                                waIntent.putExtra(Intent.EXTRA_TEXT, inviteUrl);
                                startActivity(Intent.createChooser(waIntent, "Share with"));
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                            } else {
                                dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, TaskCode.SENDINVITATION).execute();

            }
        });
        wechatImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().hideSoftKeyBoard(ViewTreeActivity.this);
                List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                valuePairs.add(new BasicNameValuePair("NodeId", nodeid));
                String url = "";
                if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                    valuePairs.add(new BasicNameValuePair("EmailAddress", EmailAddress));
                    valuePairs.add(new BasicNameValuePair("NewEmailAddress", emailEdt.getText().toString()));
                    url = Urls.mobileinviteUrl;
                } else {
                    url = Urls.mobileinviteUrl;
                }
                valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                new WebTask(ViewTreeActivity.this, url, valuePairs, new WebTaskComplete() {
                    @Override
                    public void onComplete(String response, int taskcode) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("StatusCode") == 1) {
                                String inviteUrl = MyApplication.getUserFirstName() + " " + getResources().getString(R.string.invitation_message) + " " + responseObj.getString("InvitationLink");
                                shareonWeChat(inviteUrl);
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                            } else {
                                dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, TaskCode.SENDINVITATION).execute();
            }
        });
        whatsappImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().hideSoftKeyBoard(ViewTreeActivity.this);
                List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                valuePairs.add(new BasicNameValuePair("NodeId", nodeid));
                String url = "";
                if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                    valuePairs.add(new BasicNameValuePair("EmailAddress", EmailAddress));
                    valuePairs.add(new BasicNameValuePair("NewEmailAddress", emailEdt.getText().toString()));
                    url = Urls.mobileinviteUrl;
                } else {
                    url = Urls.mobileinviteUrl;
                }
                valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                new WebTask(ViewTreeActivity.this, url, valuePairs, new WebTaskComplete() {
                    @Override
                    public void onComplete(String response, int taskcode) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("StatusCode") == 1) {
                                String inviteUrl = MyApplication.getUserFirstName() + " " + getResources().getString(R.string.invitation_message) + " " + responseObj.getString("InvitationLink");
                                onClickWhatsApp(inviteUrl);
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                            } else {
                                dialog.dismiss();
                                Toast.makeText(ViewTreeActivity.this, responseObj.getString("Message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, TaskCode.SENDINVITATION).execute();
            }
        });
        smsImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().hideSoftKeyBoard(ViewTreeActivity.this);
                List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                valuePairs.add(new BasicNameValuePair("NodeId", nodeid));
                String url = "";
                if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                    valuePairs.add(new BasicNameValuePair("EmailAddress", EmailAddress));
                    valuePairs.add(new BasicNameValuePair("NewEmailAddress", emailEdt.getText().toString()));
                    url = Urls.mobileinviteUrl;
                } else {
                    url = Urls.mobileinviteUrl;
                }
                valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                new WebTask(ViewTreeActivity.this, url, valuePairs, new WebTaskComplete() {
                    @Override
                    public void onComplete(String response, int taskcode) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("StatusCode") == 1) {
                                String inviteUrl = MyApplication.getUserFirstName() + " " + getResources().getString(R.string.invitation_message) + " " + responseObj.getString("InvitationLink");
                                PackageManager pm = getPackageManager();
                                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                                sendIntent.setData(Uri.parse("sms:"));
                                sendIntent.putExtra("sms_body", inviteUrl);
                                startActivity(sendIntent);
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                            } else {
                                dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, TaskCode.SENDINVITATION).execute();
            }
        });
        facebookImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MyApplication.getInstance().hideSoftKeyBoard(ViewTreeActivity.this);
                List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                valuePairs.add(new BasicNameValuePair("NodeId", nodeid));
                String url = "";
                if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                    valuePairs.add(new BasicNameValuePair("EmailAddress", EmailAddress));
                    valuePairs.add(new BasicNameValuePair("NewEmailAddress", emailEdt.getText().toString()));
                    url = Urls.mobileinviteUrl;
                } else {
                    url = Urls.mobileinviteUrl;
                }
                valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                new WebTask(ViewTreeActivity.this, url, valuePairs, new WebTaskComplete() {
                    @Override
                    public void onComplete(String response, int taskcode) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("StatusCode") == 1) {
                                String inviteUrl = MyApplication.getUserFirstName() + " " + getResources().getString(R.string.invitation_message) + " " + responseObj.getString("InvitationLink");
                                shareFb(inviteUrl, emailEdt.getText().toString());
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                            } else {
                                dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, TaskCode.SENDINVITATION).execute();

            }
        });
        sendinvitationTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                    MyApplication.getInstance().hideSoftKeyBoard(ViewTreeActivity.this);
                    List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                    valuePairs.add(new BasicNameValuePair("NodeId", nodeid));
                    valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                    new WebTask(ViewTreeActivity.this, Urls.mobileinviteUrl, valuePairs, new WebTaskComplete() {
                        @Override
                        public void onComplete(String response, int taskcode) {
                            try {
                                Log.v("result_of_send_invi ", response);
                                JSONObject responseObj = new JSONObject(response);
                                if (responseObj.getInt("StatusCode") == 1) {
                                    if (dialog != null) {
                                        dialog.dismiss();
                                        showAlret(responseObj.getString("Message"));
                                    }
                                } else {
                                    dialog.dismiss();
                                    showAlret(responseObj.getString("Message"));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, TaskCode.SENDINVITATION).execute();
                } else {
                    Toast.makeText(ViewTreeActivity.this, getString(R.string.please_enter_email), Toast.LENGTH_SHORT).show();
                }
            }
        });
        cancelTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                MyApplication.getInstance().hideSoftKeyBoard(ViewTreeActivity.this);
            }
        });
    }

    public void shareFb(String url, String email) {

        try {
            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
            sendIntent.setType("plain/text");
            sendIntent.setData(Uri.parse(email));
            sendIntent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
            sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.join_your_family_tree));
            sendIntent.putExtra(Intent.EXTRA_TEXT, url);
            startActivity(sendIntent);
        } catch (Exception e) {
            Intent sendIntent = new Intent(Intent.ACTION_SEND);
            sendIntent.setType("plain/text");
//            sendIntent.setData(Uri.parse("mailto:" + email));
            sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.join_your_family_tree));
            sendIntent.putExtra(Intent.EXTRA_TEXT, url);
            startActivity(sendIntent);
//            startActivity(Intent.createChooser(sendIntent, "Send Email"));
        }
    }

    public void onClickWhatsApp(String url) {
        PackageManager pm = getPackageManager();
        try {
            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
            //Check if package exists or not. If not then code
            //in catch block will be called
            waIntent.setPackage("com.whatsapp");
            waIntent.putExtra(Intent.EXTRA_TEXT, url);
            startActivity(Intent.createChooser(waIntent, "Share with"));
        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(this, "WhatsApp not Installed", Toast.LENGTH_SHORT).show();
        }
    }

    public void shareonWeChat(String url) {
//        WXWebpageObject webpage = new WXWebpageObject();
//        webpage.webpageUrl = "http://www.wechat.com";
//        WXMediaMessage msg = new WXMediaMessage(webpage);
//        msg.title = "Wechat homepage";
//        msg.description=url;
        try {
            PackageManager pm = getPackageManager();

            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            PackageInfo info = pm.getPackageInfo("com.tencent.mm", PackageManager.GET_META_DATA);
            //Check if package exists or not. If not then code
            //in catch block will be called
            waIntent.setPackage("com.tencent.mm");
            waIntent.putExtra(Intent.EXTRA_TEXT, url);
            startActivity(Intent.createChooser(waIntent, "Share with"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void getTagMemberList() {
        List<NameValuePair> valuePairs = new ArrayList<>();
        valuePairs.add(new BasicNameValuePair("NodeId", MyApplication.getUserID()));
        valuePairs.add(new BasicNameValuePair("name", ""));
        if (MyApplication.isInternetWorking(ViewTreeActivity.this)) {
            addmemberLayout.setClickable(false);
            new WebTask(ViewTreeActivity.this, Urls.tagmember, valuePairs, ViewTreeActivity.this, TaskCode.TAGMEMBER, true).execute();
        }
    }

    ArrayList<String> memberList = new ArrayList<>();
    ArrayList<String> memberListNodeId = new ArrayList<>();
    ArrayList<TreeWrapper> tagMemberArraylist = new ArrayList<>();

    private void parseAllMemberData(String response) {

        if (response != null) {
            try {
                JSONObject responseObj = new JSONObject(response);
                if (responseObj.getString("StatusCode").equalsIgnoreCase("1")) {
                    JSONArray MembersArray = responseObj.getJSONArray("Members");
                    if (MembersArray.length() != 0) {
                        for (int j = 0; j < MembersArray.length(); j++) {
                            TreeWrapper treeWrapper = new TreeWrapper();
                            JSONObject nodesObj = MembersArray.getJSONObject(j);
                            treeWrapper.setNodeId(nodesObj.getString("NodeId"));
                            treeWrapper.setName(nodesObj.getString("Name"));
                            treeWrapper.setImageUrl(nodesObj.getString("Image"));
                            treeWrapper.setGender(nodesObj.getString("Gender"));
                            treeWrapper.setDOB(nodesObj.getString("DateOfBirth"));
                            tagMemberArraylist.add(treeWrapper);

                            memberList.add(nodesObj.getString("Name"));
                            memberListNodeId.add(nodesObj.getString("NodeId"));

                        }
                        AppGlobalData.getInstance().getAllMemberList().clear();
                        AppGlobalData.getInstance().getAllMemberNodeIdList().clear();

                        AppGlobalData.getInstance().setAllMemberList(memberList);
                        AppGlobalData.getInstance().setAllMemberNodeIdList(memberListNodeId);

                        AppGlobalData.getInstance().getTagMemberArrayList().clear();
                        AppGlobalData.getInstance().setTagMemberArrayList(tagMemberArraylist);

                        AppGlobalData.getInstance().getAllMemberList().add(0, ViewTreeActivity.this.getResources().getString(R.string.select_member));
                        AppGlobalData.getInstance().getAllMemberNodeIdList().add(0, "");

                        addmemberLayout.setClickable(true);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

}
