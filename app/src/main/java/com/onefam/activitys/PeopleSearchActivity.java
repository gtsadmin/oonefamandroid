package com.onefam.activitys;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.onefam.R;
import com.onefam.applications.MyApplication;
import com.onefam.views.CircularImageView;
import com.onefam.views.EditTextProximaNovaRegular;
import com.onefam.views.TextViewProximaNovaBold;
import com.onefam.views.TextViewProximaNovaRegular;
import com.onefam.webutility.ParseData;
import com.onefam.webutility.TaskCode;
import com.onefam.webutility.Urls;
import com.onefam.webutility.WebTask;
import com.onefam.webutility.WebTaskComplete;
import com.onefam.wrapper.SearchMemberWrapper;
import com.onefam.wrapper.TreeWrapper;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class PeopleSearchActivity extends AppCompatActivity implements WebTaskComplete {

    @Bind(R.id.topbarLayout)
    RelativeLayout topbarLayout;
    @Bind(R.id.searchLayout)
    RelativeLayout searchLayout;
    @Bind(R.id.searchpeopleList)
    ListView searchpeopleList;
    @Bind(R.id.backLayout)
    LinearLayout backLayout;
    @Bind(R.id.searchEdt)
    EditTextProximaNovaRegular searchEdt;
    SearchAdapter familyAdapter;
    ArrayList<TreeWrapper> memberInfoWrappers;
    @Bind(R.id.goTxt)
    TextViewProximaNovaRegular goTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.people_search_layout);
        ButterKnife.bind(this);


//        if (AppGlobalData.getInstance().getTreeWrapperArrayList().size() == 0) {
//            if (MyApplication.isInternetWorking(PeopleSearchActivity.this)) {
//                List<NameValuePair> valuePairs = new ArrayList<>();
//                valuePairs.add(new BasicNameValuePair("NodeId", MyApplication.getUserID()));
//
//                new WebTask(PeopleSearchActivity.this, Urls.searchmemberAPI, valuePairs, this, TaskCode.SEARCHCODE).execute();
//            }
//        } else {
//            familyAdapter = new SearchAdapter(PeopleSearchActivity.this, AppGlobalData.getInstance().getTreeWrapperArrayList());
//            searchpeopleList.setAdapter(familyAdapter);
//        }


        searchEdt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    List<NameValuePair> valuePairs = new ArrayList<>();
                    valuePairs.add(new BasicNameValuePair("NodeId", MyApplication.getUserID()));
                    valuePairs.add(new BasicNameValuePair("name", searchEdt.getText().toString()));
                    new WebTask(PeopleSearchActivity.this, Urls.searchmemberAPI, valuePairs, PeopleSearchActivity.this, TaskCode.SEARCHCODE).execute();
                }
                return false;
            }
        });
//        searchEdt.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                if (familyAdapter != null) {
//                    familyAdapter.filter(searchEdt.getText().toString());
//
//                }
//            }
//        });

    }

    @OnClick(R.id.backLayout)
    public void backLayout() {
        onBackPressed();
    }

    @OnClick(R.id.goTxt)
    public void goTxt() {
        MyApplication.getInstance().hideSoftKeyBoard(PeopleSearchActivity.this);
        List<NameValuePair> valuePairs = new ArrayList<>();
        valuePairs.add(new BasicNameValuePair("NodeId", MyApplication.getUserID()));
        valuePairs.add(new BasicNameValuePair("name", searchEdt.getText().toString()));
        new WebTask(PeopleSearchActivity.this, Urls.searchmemberAPI, valuePairs, PeopleSearchActivity.this, TaskCode.SEARCHCODE).execute();
    }

    @Override
    public void onComplete(String response, int taskcode) {
        if (response != null && taskcode == TaskCode.SEARCHCODE) {
            ArrayList<SearchMemberWrapper> memberInfoWrappers = new ParseData(PeopleSearchActivity.this, response, taskcode).getSearchMember();

            familyAdapter = new SearchAdapter(PeopleSearchActivity.this, memberInfoWrappers);
            searchpeopleList.setAdapter(familyAdapter);


        } else {
//            Toast.makeText(T)
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MyApplication.getInstance().hideSoftKeyBoard(PeopleSearchActivity.this);
//        if (familyAdapter != null) {
//            familyAdapter.filter("");
//
//        }
        finish();
    }

    public class SearchAdapter extends BaseAdapter {
        ArrayList<SearchMemberWrapper> memberInfoWrappers;
        Context context;
        private ArrayList<SearchMemberWrapper> arraylist;

        public SearchAdapter(Context context, ArrayList<SearchMemberWrapper> memberInfoWrappers) {
            this.context = context;
            this.memberInfoWrappers = memberInfoWrappers;
            this.arraylist = new ArrayList<>();
            this.arraylist.addAll(memberInfoWrappers);
        }

        @Override
        public int getCount() {
            return memberInfoWrappers.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            ViewHolder viewHolder = null;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = inflater.inflate(R.layout.people_search_list_layout, null);
                convertView.setTag(viewHolder);
                viewHolder.nameTxt = (TextViewProximaNovaRegular) convertView.findViewById(R.id.nameTxt);
                viewHolder.dobTxt = (TextViewProximaNovaRegular) convertView.findViewById(R.id.dobTxt);
                viewHolder.sendinvitationTxt = (TextViewProximaNovaRegular) convertView.findViewById(R.id.sendinvitationTxt);
                viewHolder.profileImg = (ImageView) convertView.findViewById(R.id.profileImg);
                viewHolder.sideView = (ImageView) convertView.findViewById(R.id.sideView);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MyApplication.getInstance().hideSoftKeyBoard(PeopleSearchActivity.this);
                    System.out.println("position" + memberInfoWrappers.get(position).getName());
                    Intent profile_intent = new Intent(PeopleSearchActivity.this, ProfileActivity.class);
                    profile_intent.putExtra("id", memberInfoWrappers.get(position).getNodeId());
                    startActivity(profile_intent);
                }
            });
            if (memberInfoWrappers.get(position).getGender().equalsIgnoreCase("M")) {
                viewHolder.sideView.setBackgroundColor(getResources().getColor(R.color.top_bar_color));
            } else {
                viewHolder.sideView.setBackgroundColor(getResources().getColor(R.color.bottom_btn_two));
            }
            viewHolder.nameTxt.setText(memberInfoWrappers.get(position).getName());
            if (!TextUtils.isEmpty(memberInfoWrappers.get(position).getDate()) && !memberInfoWrappers.get(position).getDate().equalsIgnoreCase("null") && memberInfoWrappers.get(position).getDate() != null) {
                viewHolder.dobTxt.setVisibility(View.VISIBLE);
                viewHolder.dobTxt.setText("Birth : " + formateDate(memberInfoWrappers.get(position).getDate()));
            } else {
                viewHolder.dobTxt.setVisibility(View.INVISIBLE);
            }
            if (!memberInfoWrappers.get(position).getImage().equalsIgnoreCase("null")) {
                MyApplication.loader.displayImage(Urls.base_Url + "source/upload/profile/" + memberInfoWrappers.get(position).getImage(), viewHolder.profileImg, MyApplication.options);
            } else {
                if (memberInfoWrappers.get(position).getGender().equalsIgnoreCase("M")) {
                    viewHolder.profileImg.setBackgroundResource(R.mipmap.thumb_male);
                } else {
                    viewHolder.profileImg.setBackgroundResource(R.mipmap.thumb_female);
                }
            }

            viewHolder.sendinvitationTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendInviteDialog(memberInfoWrappers.get(position).getNodeId(), "", memberInfoWrappers.get(position).getName());
                }
            });

            return convertView;


        }

        public class ViewHolder {
            ImageView profileImg;
            //            TextViewProximaNovaBold nameTxt;
            TextViewProximaNovaRegular dobTxt, dodTxt, nameTxt, sendinvitationTxt;
            ImageView sideView;
        }

//        public void filter(String charText) {
//            charText = charText.toLowerCase(Locale.getDefault());
//            memberInfoWrappers.clear();
//            if (charText.length() == 0) {
//                memberInfoWrappers.addAll(arraylist);
//            } else {
//                for (TreeWrapper wp : arraylist) {
//                    if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
//                        memberInfoWrappers.add(wp);
//                    }
//                }
//            }
//            notifyDataSetChanged();
//            if (memberInfoWrappers.size() == 0) {
//                Toast.makeText(PeopleSearchActivity.this, "Record not found", Toast.LENGTH_SHORT).show();
//            }
//
//        }
    }

    private String formateDate(String dateString) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat("dd MMMM yyyy");
            Date date = originalFormat.parse(dateString);
            String formattedDate = targetFormat.format(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            languageCode(MyApplication.getLanguagecode());
        } else {
            languageCode(MyApplication.getLanguagecode());
        }
    }

    public void languageCode(String code) {
        Locale locale;
        Configuration lconfig;
        locale = new Locale(code);
        Locale.setDefault(locale);
        lconfig = new Configuration();
        lconfig.locale = locale;
        getBaseContext().getResources().updateConfiguration(lconfig,
                getBaseContext().getResources().getDisplayMetrics());
    }


    Dialog dialog;

    public void sendInviteDialog(final String nodeid, final String EmailAddress, String name) {
        dialog = new Dialog(PeopleSearchActivity.this, R.style.Theme_Custom);
        dialog.setContentView(R.layout.send_invitation_popup);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        TextViewProximaNovaRegular cancelTxt = (TextViewProximaNovaRegular) dialog.findViewById(R.id.cancelTxt);
        TextViewProximaNovaRegular inviteTxt = (TextViewProximaNovaRegular) dialog.findViewById(R.id.inviteTxt);
        TextViewProximaNovaRegular nameTxt = (TextViewProximaNovaRegular) dialog.findViewById(R.id.nameTxt);
        TextViewProximaNovaRegular sendinvitationTxt = (TextViewProximaNovaRegular) dialog.findViewById(R.id.inviteTxt);
//        nameTxt.setText(nameTxt.getText().toString());
        nameTxt.setText(name);
        final EditTextProximaNovaRegular emailEdt = (EditTextProximaNovaRegular) dialog.findViewById(R.id.emailEdt);
        ImageView facebookImg = (ImageView) dialog.findViewById(R.id.facebookImg);
        ImageView smsImg = (ImageView) dialog.findViewById(R.id.smsImg);
        ImageView whatsappImg = (ImageView) dialog.findViewById(R.id.whatsappImg);
        ImageView wechatImg = (ImageView) dialog.findViewById(R.id.wechatImg);
        ImageView viberImg = (ImageView) dialog.findViewById(R.id.viberImg);
        viberImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().hideSoftKeyBoard(PeopleSearchActivity.this);
                List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                valuePairs.add(new BasicNameValuePair("NodeId", nodeid));
                String url = "";
                if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                    valuePairs.add(new BasicNameValuePair("EmailAddress", EmailAddress));
                    valuePairs.add(new BasicNameValuePair("NewEmailAddress", emailEdt.getText().toString()));
                    url = Urls.mobileinviteUrl;
                } else {
                    url = Urls.mobileinviteUrl;
                }
                valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                new WebTask(PeopleSearchActivity.this, url, valuePairs, new WebTaskComplete() {
                    @Override
                    public void onComplete(String response, int taskcode) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("StatusCode") == 1) {
                                String inviteUrl = MyApplication.getUserFirstName() + " " + getResources().getString(R.string.invitation_message) + " " + responseObj.getString("InvitationLink");
                                Intent i = new Intent(Intent.ACTION_SEND);
                                i.setPackage("com.viber.voip");
                                i.setType("text/plain");
                                i.putExtra(Intent.EXTRA_TEXT, inviteUrl);
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                            } else {
                                dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, TaskCode.SENDINVITATION).execute();

            }
        });
        wechatImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().hideSoftKeyBoard(PeopleSearchActivity.this);
                List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                valuePairs.add(new BasicNameValuePair("NodeId", nodeid));
                String url = "";
                if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                    valuePairs.add(new BasicNameValuePair("EmailAddress", EmailAddress));
                    valuePairs.add(new BasicNameValuePair("NewEmailAddress", emailEdt.getText().toString()));
                    url = Urls.mobileinviteUrl;
                } else {
                    url = Urls.mobileinviteUrl;
                }
                valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                new WebTask(PeopleSearchActivity.this, url, valuePairs, new WebTaskComplete() {
                    @Override
                    public void onComplete(String response, int taskcode) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("StatusCode") == 1) {
                                String inviteUrl = MyApplication.getUserFirstName() + " " + getResources().getString(R.string.invitation_message) + " " + responseObj.getString("InvitationLink");
                                shareonWeChat(inviteUrl);
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                            } else {
                                dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, TaskCode.SENDINVITATION).execute();
            }
        });
        whatsappImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().hideSoftKeyBoard(PeopleSearchActivity.this);
                List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                valuePairs.add(new BasicNameValuePair("NodeId", nodeid));
                String url = "";
                if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                    valuePairs.add(new BasicNameValuePair("EmailAddress", EmailAddress));
                    valuePairs.add(new BasicNameValuePair("NewEmailAddress", emailEdt.getText().toString()));
                    url = Urls.mobileinviteUrl;
                } else {
                    url = Urls.mobileinviteUrl;
                }
                valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                new WebTask(PeopleSearchActivity.this, url, valuePairs, new WebTaskComplete() {
                    @Override
                    public void onComplete(String response, int taskcode) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("StatusCode") == 1) {
                                String inviteUrl = MyApplication.getUserFirstName() + " " + getResources().getString(R.string.invitation_message) + " " + responseObj.getString("InvitationLink");
                                onClickWhatsApp(inviteUrl);
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                            } else {
                                dialog.dismiss();
                                Toast.makeText(PeopleSearchActivity.this, responseObj.getString("Message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, TaskCode.SENDINVITATION).execute();
            }
        });
        smsImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().hideSoftKeyBoard(PeopleSearchActivity.this);
                List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                valuePairs.add(new BasicNameValuePair("NodeId", nodeid));
                String url = "";
                if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                    valuePairs.add(new BasicNameValuePair("EmailAddress", EmailAddress));
                    valuePairs.add(new BasicNameValuePair("NewEmailAddress", emailEdt.getText().toString()));
                    url = Urls.mobileinviteUrl;
                } else {
                    url = Urls.mobileinviteUrl;
                }
                valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                new WebTask(PeopleSearchActivity.this, url, valuePairs, new WebTaskComplete() {
                    @Override
                    public void onComplete(String response, int taskcode) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("StatusCode") == 1) {
                                String inviteUrl = MyApplication.getUserFirstName() + " " + getResources().getString(R.string.invitation_message) + " " + responseObj.getString("InvitationLink");
                                PackageManager pm = getPackageManager();
                                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                                sendIntent.setData(Uri.parse("sms:"));
                                sendIntent.putExtra("sms_body", inviteUrl);
                                startActivity(sendIntent);
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                            } else {
                                dialog.dismiss();
                                Toast.makeText(PeopleSearchActivity.this, responseObj.getString("Message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, TaskCode.SENDINVITATION).execute();
            }
        });
        facebookImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().hideSoftKeyBoard(PeopleSearchActivity.this);
                List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                valuePairs.add(new BasicNameValuePair("NodeId", nodeid));
                String url = "";
                if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                    valuePairs.add(new BasicNameValuePair("EmailAddress", EmailAddress));
                    valuePairs.add(new BasicNameValuePair("NewEmailAddress", emailEdt.getText().toString()));
                    url = Urls.mobileinviteUrl;
                } else {
                    url = Urls.mobileinviteUrl;
                }
                valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                new WebTask(PeopleSearchActivity.this, url, valuePairs, new WebTaskComplete() {
                    @Override
                    public void onComplete(String response, int taskcode) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("StatusCode") == 1) {
                                String inviteUrl = MyApplication.getUserFirstName() + " " + getResources().getString(R.string.invitation_message) + " " + responseObj.getString("InvitationLink");
                                shareFb(inviteUrl, emailEdt.getText().toString());
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                            } else {
                                dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, TaskCode.SENDINVITATION).execute();

            }
        });
        sendinvitationTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                    MyApplication.getInstance().hideSoftKeyBoard(PeopleSearchActivity.this);
                    List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                    valuePairs.add(new BasicNameValuePair("NodeId", nodeid));
                    valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                    new WebTask(PeopleSearchActivity.this, Urls.mobileinviteUrl, valuePairs, new WebTaskComplete() {
                        @Override
                        public void onComplete(String response, int taskcode) {
                            try {
                                JSONObject responseObj = new JSONObject(response);
                                if (responseObj.getInt("StatusCode") == 1) {
                                    if (dialog != null) {
                                        dialog.dismiss();
                                    }
                                } else {
                                    dialog.dismiss();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, TaskCode.SENDINVITATION).execute();
                } else {
                    Toast.makeText(PeopleSearchActivity.this, getString(R.string.please_enter_email), Toast.LENGTH_SHORT).show();
                }
            }
        });
        cancelTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                MyApplication.getInstance().hideSoftKeyBoard(PeopleSearchActivity.this);
            }
        });
    }

    public void shareFb(String url, String email) {
        try {
            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
            sendIntent.setType("plain/text");
            sendIntent.setData(Uri.parse(email));
            sendIntent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
            sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.join_your_family_tree));
            sendIntent.putExtra(Intent.EXTRA_TEXT, url);
            startActivity(sendIntent);
        } catch (Exception e) {
            Intent sendIntent = new Intent(Intent.ACTION_SEND);
            sendIntent.setType("plain/text");
//            sendIntent.setData(Uri.parse("mailto:" + email));
            sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.join_your_family_tree));
            sendIntent.putExtra(Intent.EXTRA_TEXT, url);
            startActivity(sendIntent);

        }
    }

    public void onClickWhatsApp(String url) {
        PackageManager pm = getPackageManager();
        try {
            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
            //Check if package exists or not. If not then code
            //in catch block will be called
            waIntent.setPackage("com.whatsapp");
            waIntent.putExtra(Intent.EXTRA_TEXT, url);
            startActivity(Intent.createChooser(waIntent, "Share with"));
        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(this, "WhatsApp not Installed", Toast.LENGTH_SHORT).show();
        }
    }

    public void shareonWeChat(String url) {
//        WXWebpageObject webpage = new WXWebpageObject();
//        webpage.webpageUrl = "http://www.wechat.com";
//        WXMediaMessage msg = new WXMediaMessage(webpage);
//        msg.title = "Wechat homepage";
//        msg.description=url;

        try {
            PackageManager pm = getPackageManager();

            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            PackageInfo info = pm.getPackageInfo("com.tencent.mm", PackageManager.GET_META_DATA);
            //Check if package exists or not. If not then code
            //in catch block will be called
            waIntent.setPackage("com.tencent.mm");
            waIntent.putExtra(Intent.EXTRA_TEXT, url);
            startActivity(Intent.createChooser(waIntent, "Share with"));
        } catch (Exception ex) {
            Toast.makeText(this, "WeChat not Installed", Toast.LENGTH_SHORT).show();
            ex.printStackTrace();
        }
    }
}
