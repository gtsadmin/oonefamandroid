package com.onefam.activitys;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.onefam.R;
import com.onefam.applications.MyApplication;
import com.onefam.datacontroller.AppGlobalData;
import com.onefam.views.CircularImageView;
import com.onefam.views.TextViewProximaNovaRegular;
import com.onefam.webutility.ParseData;
import com.onefam.webutility.TaskCode;
import com.onefam.webutility.Urls;
import com.onefam.webutility.WebTask;
import com.onefam.webutility.WebTaskComplete;
import com.onefam.wrapper.InvitationsFriendsWrapper;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class PaddingInvitationActivity extends AppCompatActivity implements WebTaskComplete {
    @Bind(R.id.backLayout)
    LinearLayout backLayout;
    @Bind(R.id.topbarLayout)
    RelativeLayout topbarLayout;
    @Bind(R.id.familyList)
    ListView familyList;
    boolean checkFrom =false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.padding_invitation_layout);
        ButterKnife.bind(this);
        List<NameValuePair> valuePairs = new ArrayList<>();
        valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
        valuePairs.add(new BasicNameValuePair("Email", MyApplication.getEmailID()));
        new WebTask(PaddingInvitationActivity.this, Urls.getPeddingInvitation, valuePairs, this, TaskCode.GETPEDDINGINVITATIONS).execute();

        checkFrom=getIntent().getBooleanExtra("bln",false);


    }

    @Override
    public void onComplete(String response, int taskcode) {
        if (response != null && taskcode == TaskCode.GETPEDDINGINVITATIONS) {
         ArrayList<InvitationsFriendsWrapper>   invitationsFriendsWrappers = new ParseData(PaddingInvitationActivity.this, response, taskcode).getInvitationFriend();
            PeddingInvitationAdapter  peddingInvitationAdapter = new PeddingInvitationAdapter(PaddingInvitationActivity.this, invitationsFriendsWrappers);
            familyList.setAdapter(peddingInvitationAdapter);
            AppGlobalData.reloadfamilydataBoolean = false;
//            AppGlobalData.appGlobalData.setMemberInfoWrappers(memberInfoWrappers);

        }else if (response != null && taskcode == TaskCode.ACCEPTREQUEST)
        {

        }else if (response != null && taskcode == TaskCode.DECLINEINVITE) {
//            {"StatusCode":1,"Message":"Invitation Declined","NodeId":0}
            try {
                JSONObject responseObj  = new JSONObject(response);
                if (responseObj.getInt("StatusCode")==1)
                {
                    Toast.makeText(PaddingInvitationActivity.this,responseObj.getString("Message"),Toast.LENGTH_SHORT).show();
                    finish();
                }else
                {
                    Toast.makeText(PaddingInvitationActivity.this,responseObj.getString("Message"),Toast.LENGTH_SHORT).show();
//                    finish();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
    @OnClick(R.id.backLayout)
    public void backLayout() {
        onBackPressed();

    }

    @Override
    public void onBackPressed() {
        if (checkFrom)
        {
            startActivity(new Intent(PaddingInvitationActivity.this, FamilyTreeActivity.class));
            finish();
        }else {
            finish();
        }
    }

    public class PeddingInvitationAdapter extends BaseAdapter {
        ArrayList<InvitationsFriendsWrapper> memberInfoWrappers;
        Context context;
        ViewHolder viewHolder = null;

        public PeddingInvitationAdapter(Context context, ArrayList<InvitationsFriendsWrapper> memberInfoWrappers) {
            this.context = context;
            this.memberInfoWrappers = memberInfoWrappers;
        }

        @Override
        public int getCount() {
            return memberInfoWrappers.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = inflater.inflate(R.layout.accept_request_friends_list_layout, null);
                convertView.setTag(viewHolder);
                viewHolder.titleTxt = (TextViewProximaNovaRegular) convertView.findViewById(R.id.titleTxt);
                viewHolder.nameTxt = (TextViewProximaNovaRegular) convertView.findViewById(R.id.nameTxt);
                viewHolder.lavalTxt = (TextViewProximaNovaRegular) convertView.findViewById(R.id.lavalTxt);
                viewHolder.profileImg = (CircularImageView) convertView.findViewById(R.id.profileImg);
                viewHolder.dobTxt = (TextViewProximaNovaRegular) convertView.findViewById(R.id.dobTxt);
                viewHolder.sideView = (ImageView) convertView.findViewById(R.id.sideView);
                viewHolder.acceptTxt=(TextViewProximaNovaRegular)convertView.findViewById(R.id.acceptTxt);
                viewHolder.declineTxt=(TextViewProximaNovaRegular)convertView.findViewById(R.id.declineTxt);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent profile_intent = new Intent(InvitationsFriendsActivity.this, ProfileActivity.class).putExtra("id", memberInfoWrappers.get(position).getNodeId());
//                    startActivity(profile_intent);
                }
            });
            viewHolder.nameTxt.setText(memberInfoWrappers.get(position).getName());
//            viewHolder.lavalTxt.setText(memberInfoWrappers.get(position).getRelationLevel());

            if (memberInfoWrappers.get(position).getGender().equalsIgnoreCase("M")) {
                viewHolder.titleTxt.setTextColor(getResources().getColor(R.color.board_blue_color));
                viewHolder.sideView.setBackgroundColor(getResources().getColor(R.color.board_blue_color));
            } else {
                viewHolder.titleTxt.setTextColor(getResources().getColor(R.color.bottom_btn_two));
                viewHolder.sideView.setBackgroundColor(getResources().getColor(R.color.bottom_btn_two));
            }
            viewHolder.acceptTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    List<NameValuePair> valuePairs = new ArrayList<>();
                    valuePairs.add(new BasicNameValuePair("NodeId",memberInfoWrappers.get(position).getToNodeId()));
                    valuePairs.add(new BasicNameValuePair("Token", ""));
                    valuePairs.add(new BasicNameValuePair("Password", ""));
                    valuePairs.add(new BasicNameValuePair("FromNodeId", memberInfoWrappers.get(position).getNodeId()));
                    new WebTask(PaddingInvitationActivity.this, Urls.acceptInvitation, valuePairs, PaddingInvitationActivity.this, TaskCode.ACCEPTREQUEST).execute();
                }
            });

            viewHolder.declineTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    List<NameValuePair> valuePairs = new ArrayList<>();
                    valuePairs.add(new BasicNameValuePair("NodeId",memberInfoWrappers.get(position).getToNodeId()));
                    valuePairs.add(new BasicNameValuePair("EmailId", MyApplication.getEmailID()));
                    valuePairs.add(new BasicNameValuePair("FromEmailId", memberInfoWrappers.get(position).getEmail()));
                    valuePairs.add(new BasicNameValuePair("FromNodeId", memberInfoWrappers.get(position).getNodeId()));
                    new WebTask(PaddingInvitationActivity.this, Urls.declineInviteUrl, valuePairs, PaddingInvitationActivity.this, TaskCode.DECLINEINVITE).execute();
                }
            });




            MyApplication.loader.displayImage(Urls.base_Url + memberInfoWrappers.get(position).getImage(), viewHolder.profileImg, MyApplication.options);

            return convertView;
        }

        public class ViewHolder {
            CircularImageView profileImg;
            //TextViewProximaNovaBold nameTxt;
            ImageView sideView;
            TextViewProximaNovaRegular dobTxt, dodTxt, lavalTxt, titleTxt, nameTxt,acceptTxt,declineTxt;

        }
    }
}
