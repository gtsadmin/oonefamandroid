package com.onefam.activitys;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.onefam.R;
import com.onefam.applications.MyApplication;
import com.onefam.datacontroller.AppGlobalData;
import com.onefam.views.EditTextProximaNovaRegular;
import com.onefam.views.TextViewProximaNovaRegular;
import com.onefam.webutility.ParseData;
import com.onefam.webutility.TaskCode;
import com.onefam.webutility.Urls;
import com.onefam.webutility.WebTask;
import com.onefam.webutility.WebTaskComplete;
import com.onefam.wrapper.TreeWrapper;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.graphics.drawable.BitmapDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ListView;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreateEventActivity extends AppCompatActivity implements WebTaskComplete {
    @Bind(R.id.backLayout)
    LinearLayout backLayout;
    @Bind(R.id.topbarLayout)
    RelativeLayout topbarLayout;
    @Bind(R.id.eventtitleEdt)
    EditTextProximaNovaRegular eventtitleEdt;
    @Bind(R.id.eventlocationEdt)
    EditTextProximaNovaRegular eventlocationEdt;
    @Bind(R.id.eventdateEdt)
    TextViewProximaNovaRegular eventdateEdt;
    @Bind(R.id.posteventLayout)
    LinearLayout posteventLayout;
    public int year;
    public int month;
    public int day;
    private final int DATE_DIALOG_ID = 1;
    @Bind(R.id.selectfamilymamberTxt)
    TextViewProximaNovaRegular selectfamilymamberTxt;
    private PopupWindow pw;
    private boolean expanded;
    public static boolean[] checkSelected;
    private ArrayList<String> membersIDs;
    private ArrayList<String> namesArrayToShowAlreadySelected = new ArrayList<>();
    private ArrayList<String> namesArray = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_event_layout);
        ButterKnife.bind(this);
        if (AppGlobalData.getInstance().getMemberList().size() == 0) {
            if (MyApplication.isInternetWorking(CreateEventActivity.this)) {
                List<NameValuePair> valuePairs = new ArrayList<>();
                valuePairs.add(new BasicNameValuePair("NodeId", MyApplication.getUserID()));
                new WebTask(CreateEventActivity.this, Urls.getFamalyViewTreeUrl, valuePairs, this, TaskCode.FAMILYVIEWTREECODE).execute();
            }
        } else {
            initialize();
        }

        /*unselcted already selected item from global array*/
        for (int i = 0; i < AppGlobalData.getInstance().getTagMemberArrayList().size(); i++) {
            AppGlobalData.getInstance().getTagMemberArrayList().get(i).setIsSelected(false);
        }
        if (MyApplication.isInternetWorking(CreateEventActivity.this)) {
//            getTagMemberList();
        }
    }

    @OnClick(R.id.backLayout)
    public void backLayout() {
        finish();
    }

    @OnClick(R.id.posteventLayout)
    public void posteventLayout() {
        MyApplication.myApplication.produceAnimation(posteventLayout);
        if (TextUtils.isEmpty(eventtitleEdt.getText().toString())) {
            eventtitleEdt.setError(getResources().getString(R.string.event_title));
            eventtitleEdt.requestFocus();
        } else if (TextUtils.isEmpty(eventdateEdt.getText().toString())) {
            eventdateEdt.setError(getResources().getString(R.string.event_date));
            eventdateEdt.requestFocus();
        } else {
            List<NameValuePair> valuePairs = new ArrayList<>();
            valuePairs.add(new BasicNameValuePair("UserId", MyApplication.getUserID()));
            valuePairs.add(new BasicNameValuePair("Title", eventtitleEdt.getText().toString()));
            valuePairs.add(new BasicNameValuePair("Location", eventlocationEdt.getText().toString()));
            valuePairs.add(new BasicNameValuePair("Date", dateString));
            valuePairs.add(new BasicNameValuePair("Description", ""));
            valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
            if (MyApplication.isInternetWorking(CreateEventActivity.this)) {
                new WebTask(CreateEventActivity.this, Urls.createEventApi, valuePairs, this, TaskCode.CREATEEVENTCODE).execute();
            }
        }
    }

    @OnClick(R.id.eventdateEdt)
    public void eventdateEdt() {
        showDialog(DATE_DIALOG_ID);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        Calendar c = Calendar.getInstance(Locale.ENGLISH);
        switch (id) {
            case DATE_DIALOG_ID:
                // set date picker as current date
                return new DatePickerDialog(this, datePickerListener,
                        c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        }
        return null;
    }

    private String dateString;
    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {
        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            // set selected date into textview
            String dayString = day + "", monthString = (month + 1) + "";
            if ((month + 1) < 10) {
                monthString = "0" + (month + 1);
            }
            if (day < 10) {
                dayString = "0" + day;
            }
            dateString = dayString + "-" + monthString + "-" + year;
            eventdateEdt.setText(formateDate(dateString));
        }
    };

    private String formateDate(String dateString) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat("dd MMMM yyyy");
            Date date = originalFormat.parse(dateString);
            String formattedDate = targetFormat.format(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    private void initialize() {
        //data source for drop-down list


        checkSelected = new boolean[AppGlobalData.appGlobalData.getInstance().getMemberList().size()];
        //initialize all values of list to 'unselected' initially
        for (int i = 0; i < checkSelected.length; i++) {
            checkSelected[i] = false;
        }


        selectfamilymamberTxt.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                membersIDs = new ArrayList<>();
                selectfamilymamberTxt.setHint(getResources().getString(R.string.tag_family_mamber));
                initiatePopUp(AppGlobalData.appGlobalData.getInstance().getMemberList(), selectfamilymamberTxt);
            }
        });
    }

    /*
     * Function to set up the pop-up window which acts as drop-down list
     * */
    private void initiatePopUp(ArrayList<String> items, TextView tv) {
        LayoutInflater inflater = (LayoutInflater) CreateEventActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //get the pop-up window i.e.  drop-down layout
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.pop_up_window, null);

        //get the view to which drop-down layout is to be anchored
        LinearLayout layout1 = (LinearLayout) findViewById(R.id.linearLayout1);
        pw = new PopupWindow(layout, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);

        //Pop-up window background cannot be null if we want the pop-up to listen touch events outside its window
        pw.setBackgroundDrawable(new BitmapDrawable());
        pw.setTouchable(true);

        //let pop-up be informed about touch events outside its window. This  should be done before setting the content of pop-up
        pw.setOutsideTouchable(true);
        pw.setHeight(LayoutParams.WRAP_CONTENT);

        //dismiss the pop-up i.e. drop-down when touched anywhere outside the pop-up
        pw.setTouchInterceptor(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    pw.dismiss();
                    return true;
                }
                return false;
            }
        });
        //provide the source layout for drop-down
        pw.setContentView(layout);
        //anchor the drop-down to bottom-left corner of 'layout1'
        pw.showAsDropDown(layout1);
        //populate the drop-down list
        final ListView list = (ListView) layout.findViewById(R.id.dropDownList);
        for (int i = 0; i < AppGlobalData.getInstance().getTreeWrapperArrayList().size(); i++) {
            if (namesArrayToShowAlreadySelected.size() != 0) {
                for (int j = 0; j < namesArrayToShowAlreadySelected.size(); j++) {
//                    Log.v("Selected_Name ",namesArrayToShowAlreadySelected.get(j));
//                    if (namesArrayToShowAlreadySelected.get(j).contains(AppGlobalData.getInstance().getTreeWrapperArrayList().get(i).getNodeId())) {
                    if (namesArrayToShowAlreadySelected.get(j).contains(AppGlobalData.getInstance().getTagMemberArrayList().get(i).getNodeId())) {
//                        AppGlobalData.getInstance().getTreeWrapperArrayList().get(i).setIsSelected(true);
                        AppGlobalData.getInstance().getTagMemberArrayList().get(i).setIsSelected(true);
                        Log.v("Selected_Name ", namesArrayToShowAlreadySelected.get(j) + " selected ");
                        break;

                    } else {
//                        AppGlobalData.getInstance().getTreeWrapperArrayList().get(i).setIsSelected(false);
                        AppGlobalData.getInstance().getTagMemberArrayList().get(i).setIsSelected(false);
                        Log.v("Selected_Name ", namesArrayToShowAlreadySelected.get(j) + " not selected ");
                    }
                }
            } else {
//                AppGlobalData.getInstance().getTreeWrapperArrayList().get(i).setIsSelected(false);
                AppGlobalData.getInstance().getTagMemberArrayList().get(i).setIsSelected(false);
            }
        }
//        EventDropListAdapter adapter = new EventDropListAdapter(this, AppGlobalData.getInstance().getTreeWrapperArrayList(), tv);
        EventDropListAdapter adapter = new EventDropListAdapter(this, AppGlobalData.getInstance().getTagMemberArrayList(), tv);
        list.setAdapter(adapter);
    }

    @Override
    public void onComplete(String response, int taskcode) {
        if (taskcode == TaskCode.CREATEEVENTCODE && response != null) {
//        {"StatusCode":1,"Message":"Success","NodeId":412}
            try {
                JSONObject responseObj = new JSONObject(response);
                if (responseObj.getInt("StatusCode") == 1) {
                    AppGlobalData.getEventBoolean = true;
                    finish();
//                    AlertDialog.Builder builder = new AlertDialog.Builder(CreateEventActivity.this);
//                    builder.setTitle("" + getResources().getString(R.string.app_name));
//                    builder.setMessage("Event created successfully")
//                            .setCancelable(false)
//                            .setPositiveButton("Ok",
//                                    new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int id) {
//                                            dialog.cancel();
//
//
//                                            finish();
//
//                                        }
//                                    });
//
//                    builder.show();
                } else {
                    Toast.makeText(CreateEventActivity.this, responseObj.getString("Message"), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (response != null && taskcode == TaskCode.FAMILYVIEWTREECODE) {
            ArrayList<TreeWrapper> memberInfoWrappers = new ParseData(CreateEventActivity.this, response, taskcode).getTreeMember();
            initialize();
        } else if (response != null && taskcode == TaskCode.TAGMEMBER) {
            Log.v("tagMemberList ", response);
            parseTagMemberData(response);
        }
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        languageCode(MyApplication.getLanguagecode());
//    }

    public class EventDropListAdapter extends BaseAdapter {

        private ArrayList<TreeWrapper> mListItems;
        private LayoutInflater mInflater;
        private TextView mSelectedItems;
        int selectedCount = 0;
        String firstSelected = "";
        private ViewHolder holder;
        String selected = "";    //shortened selected values representation
//        ArrayList<String> namesArray = new ArrayList<>();

        String getSelected() {
            return selected;
        }

        public void setSelected(String selected) {
            this.selected = selected;
        }

        public EventDropListAdapter(Context context, ArrayList<TreeWrapper> items,
                                    TextView tv) {
            mListItems = new ArrayList<TreeWrapper>();
            mListItems.addAll(items);
            mInflater = LayoutInflater.from(context);
            mSelectedItems = tv;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return mListItems.size();
        }

        @Override
        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.drop_down_list_row, null);
                holder = new ViewHolder();
                holder.tv = (TextView) convertView.findViewById(R.id.SelectOption);
                holder.chkbox = (CheckBox) convertView.findViewById(R.id.checkbox);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.tv.setText(mListItems.get(position).getName());

            final int position1 = position;

            //whenever the checkbox is clicked the selected values textview is updated with new selected values
//            holder.chkbox.setOnClickListener(new View.OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    // TODO Auto-generated method stub
//
//                }
//            });
            holder.chkbox.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    mListItems.get(position).setIsSelected(
                            !mListItems.get(position).isSelected());
                    notifyDataSetChanged();
                    if (mListItems.get(position).isSelected()) {
                        membersIDs.add(mListItems.get(position).getNodeId());
                        namesArray.add(mListItems.get(position).getName());
                        namesArrayToShowAlreadySelected.add(mListItems.get(position).getNodeId());
                    } else {
                        membersIDs.remove(mListItems.get(position).getNodeId());
                        namesArray.remove(mListItems.get(position).getName());
                        namesArrayToShowAlreadySelected.remove(mListItems.get(position).getNodeId());

                    }
                    if (namesArray.size() > 1) {
                        selectfamilymamberTxt.setText(namesArray.get(namesArray.size() - 1) + " & " + (namesArray.size() - 1) + " more");
                    } else {
                        selectfamilymamberTxt.setText(TextUtils.join(",", namesArray));
                    }
//                    setText(position1);
                }
            });
//            holder.chkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    setText(position1);
//                    if (isChecked) {
////                        membersIDs.add(mListItems.get(position).getNodeId());
//                    }
//                }
//            });
            if (mListItems.get(position).isSelected()) {
                holder.chkbox.setChecked(true);

            } else {
                holder.chkbox.setChecked(false);

            }
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mListItems.get(position).setIsSelected(
                            !mListItems.get(position).isSelected());
                    notifyDataSetChanged();
                    if (mListItems.get(position).isSelected()) {
                        membersIDs.add(mListItems.get(position).getNodeId());
                        namesArray.add(mListItems.get(position).getName());
                        namesArrayToShowAlreadySelected.add(mListItems.get(position).getNodeId());
                    } else {
                        membersIDs.remove(mListItems.get(position).getNodeId());
                        namesArray.remove(mListItems.get(position).getName());
                        namesArrayToShowAlreadySelected.remove(mListItems.get(position).getNodeId());

                    }
                    if (namesArray.size() > 1) {
                        selectfamilymamberTxt.setText(namesArray.get(namesArray.size() - 1) + " & " + (namesArray.size() - 1) + " more");
                    } else {
                        selectfamilymamberTxt.setText(TextUtils.join(",", namesArray));
                    }
//                    selectfamilymamberTxt.setText(TextUtils.join(",", namesArray));
                }
            });

            return convertView;
        }


        /*
         * Function which updates the selected values display and information(checkSelected[])
         * */
        private void setText(int position1) {
            if (!CreateEventActivity.checkSelected[position1]) {
                CreateEventActivity.checkSelected[position1] = true;
                selectedCount++;
            } else {
                CreateEventActivity.checkSelected[position1] = false;
                selectedCount--;
            }

            if (selectedCount == 0) {
                mSelectedItems.setText(R.string.tag_family_mamber);
            } else if (selectedCount == 1) {
                for (int i = 0; i < CreateEventActivity.checkSelected.length; i++) {
                    if (CreateEventActivity.checkSelected[i] == true) {
                        firstSelected = mListItems.get(i).getName();
                        break;
                    }
                }
                mSelectedItems.setText(firstSelected);
                setSelected(firstSelected);
            } else if (selectedCount > 1) {
                for (int i = 0; i < CreateEventActivity.checkSelected.length; i++) {
                    if (CreateEventActivity.checkSelected[i] == true) {
                        firstSelected = mListItems.get(i).getName();
                        break;
                    }
                }
                mSelectedItems.setText(firstSelected + " & " + (selectedCount - 1) + " more");
                setSelected(firstSelected + " & " + (selectedCount - 1) + " more");
            }
        }

        private class ViewHolder {
            TextView tv;
            CheckBox chkbox;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            languageCode(MyApplication.getLanguagecode());
            setContentView(R.layout.create_event_layout_land);
            ButterKnife.bind(this);
            if (AppGlobalData.getInstance().getMemberList().size() == 0) {
                if (MyApplication.isInternetWorking(CreateEventActivity.this)) {
                    List<NameValuePair> valuePairs = new ArrayList<>();
                    valuePairs.add(new BasicNameValuePair("NodeId", MyApplication.getUserID()));
                    new WebTask(CreateEventActivity.this, Urls.getFamalyViewTreeUrl, valuePairs, this, TaskCode.FAMILYVIEWTREECODE).execute();
                }
            } else {
                initialize();
            }

        } else {
            languageCode(MyApplication.getLanguagecode());
            setContentView(R.layout.create_event_layout);
            ButterKnife.bind(this);
            if (AppGlobalData.getInstance().getMemberList().size() == 0) {
                if (MyApplication.isInternetWorking(CreateEventActivity.this)) {
                    List<NameValuePair> valuePairs = new ArrayList<>();
                    valuePairs.add(new BasicNameValuePair("NodeId", MyApplication.getUserID()));
                    new WebTask(CreateEventActivity.this, Urls.getFamalyViewTreeUrl, valuePairs, this, TaskCode.FAMILYVIEWTREECODE).execute();
                }
            } else {
                initialize();
            }

        }
    }

    public void languageCode(String code) {
        Locale locale;
        Configuration lconfig;
        locale = new Locale(code);
        Locale.setDefault(locale);
        lconfig = new Configuration();
        lconfig.locale = locale;
        getBaseContext().getResources().updateConfiguration(lconfig,
                getBaseContext().getResources().getDisplayMetrics());
    }


    private void getTagMemberList() {
        List<NameValuePair> valuePairs = new ArrayList<>();
        valuePairs.add(new BasicNameValuePair("NodeId", MyApplication.getUserID()));
        valuePairs.add(new BasicNameValuePair("name", ""));
        if (MyApplication.isInternetWorking(CreateEventActivity.this)) {
            new WebTask(CreateEventActivity.this, Urls.tagmember, valuePairs, CreateEventActivity.this, TaskCode.TAGMEMBER, false).execute();
        }
    }

    ArrayList<TreeWrapper> tagMemberArraylist = new ArrayList<>();

    /*function parsing the data of tag member*/
    private void parseTagMemberData(String response) {

        if (response != null) {
            try {
                JSONObject responseObj = new JSONObject(response);
                if (responseObj.getString("StatusCode").equalsIgnoreCase("1")) {
                    JSONArray MembersArray = responseObj.getJSONArray("Members");
                    if (MembersArray.length() != 0) {
                        for (int j = 0; j < MembersArray.length(); j++) {
                            TreeWrapper treeWrapper = new TreeWrapper();
                            JSONObject nodesObj = MembersArray.getJSONObject(j);

                            treeWrapper.setNodeId(nodesObj.getString("NodeId"));
                            treeWrapper.setName(nodesObj.getString("Name"));
                            treeWrapper.setImageUrl(nodesObj.getString("Image"));
                            treeWrapper.setGender(nodesObj.getString("Gender"));
                            treeWrapper.setDOB(nodesObj.getString("DateOfBirth"));
                            tagMemberArraylist.add(treeWrapper);
                        }
                        AppGlobalData.getInstance().getTagMemberArrayList().clear();
                        AppGlobalData.getInstance().setTagMemberArrayList(tagMemberArraylist);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

}
