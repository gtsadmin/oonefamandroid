package com.onefam.activitys;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public abstract class AbsGenericActivity  extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }
    protected void switchActivity(Activity paramActivity, Class<?> paramClass)
    {
        try
        {
            paramActivity.startActivity(new Intent(paramActivity, paramClass));
            return;
        }
        catch (Exception localException)
        {
            localException.printStackTrace();
        }
    }
}
