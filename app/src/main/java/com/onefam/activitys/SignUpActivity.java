package com.onefam.activitys;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.onefam.R;
import com.onefam.applications.MyApplication;
import com.onefam.datacontroller.AppGlobalData;
import com.onefam.views.EditTextProximaNovaRegular;
import com.onefam.views.RadioButtonProximaNovaRegular;
import com.onefam.views.TextViewProximaNovaRegular;
import com.onefam.webutility.MultipartRequest;
import com.onefam.webutility.TaskCode;
import com.onefam.webutility.Urls;
import com.onefam.webutility.WebTask;
import com.onefam.webutility.WebTaskComplete;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import eu.janmuller.android.simplecropimage.CropImage;

import static com.onefam.R.id.emailEdt;
import static com.onefam.R.id.lastnameEdt;


public class SignUpActivity extends Activity implements WebTaskComplete {
    @Bind(R.id.profileImg)
    ImageView profileImg;

    @Bind(R.id.nameEdt)
    EditTextProximaNovaRegular nameEdt;
    @Bind(R.id.emailId)
    EditTextProximaNovaRegular emailId;
    @Bind(R.id.passwordEdt)
    EditTextProximaNovaRegular passwordEdt;
    @Bind(R.id.confirmEdt)
    EditTextProximaNovaRegular confirmEdt;
    @Bind(R.id.dateEdt)
    TextViewProximaNovaRegular dateEdt;
    @Bind(R.id.mothernameTxt)
    EditTextProximaNovaRegular mothernameTxt;
    @Bind(R.id.fatherEdt)
    EditTextProximaNovaRegular fatherEdt;
    @Bind(R.id.getstaetedBtn)
    Button getstaetedBtn;
    public static final int REQUEST_CODE_GALLERY = 0x51;
    public static final int REQUEST_CODE_CROP_IMAGE = 0x53;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x52;
    @Bind(R.id.maleRadioBtn)
    RadioButtonProximaNovaRegular maleRadioBtn;
    @Bind(R.id.femaleRadioBtn)
    RadioButtonProximaNovaRegular femaleRadioBtn;
    private File mFileTemp;
    private String imagePath = "", images_uptedates = "";

    public int year;
    public int month;
    public int day;
    String gender = "M";
    private final int DATE_DIALOG_ID = 1;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private Calendar calendar;
    String currentdateString;
    String nodeId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_layout);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ButterKnife.bind(this);
        maleRadioBtn.setChecked(true);
        maleRadioBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                gender = "M";
            }
        });
        femaleRadioBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                gender = "F";
            }
        });
        maleRadioBtn.setPadding((int) getResources().getDimension(R.dimen.dim_20),
                maleRadioBtn.getPaddingTop(),
                maleRadioBtn.getPaddingRight(),
                maleRadioBtn.getPaddingBottom());
        femaleRadioBtn.setPadding((int) getResources().getDimension(R.dimen.dim_20),
                femaleRadioBtn.getPaddingTop(),
                femaleRadioBtn.getPaddingRight(),
                femaleRadioBtn.getPaddingBottom());
        fatherEdt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (TextUtils.isEmpty(nameEdt.getText().toString())) {
                        nameEdt.setError(getResources().getString(R.string.enter_full_name));
                        nameEdt.requestFocus();
                    } else if (TextUtils.isEmpty(emailId.getText().toString())) {
                        emailId.setError(getResources().getString(R.string.Pleaseentervalidemailid));
                        emailId.requestFocus();
                    } else if (!MyApplication.isValidEmail(emailId.getText().toString())) {
                        emailId.setError(getResources().getString(R.string.Pleaseentervalidemailid));
                        emailId.requestFocus();
                    } else if (TextUtils.isEmpty(passwordEdt.getText().toString())) {
                        passwordEdt.setError(getResources().getString(R.string.enter_a_password));
                        passwordEdt.requestFocus();
                    } else if (!confirmEdt.getText().toString().equalsIgnoreCase(passwordEdt.getText().toString())) {
                        confirmEdt.setError(getResources().getString(R.string.confirm_password_above));
                        confirmEdt.requestFocus();
                    } else if (TextUtils.isEmpty(dateEdt.getText().toString())) {
//                        dateEdt.setError(getResources().getString(R.string.valid_dob));
                        Toast.makeText(SignUpActivity.this, getResources().getString(R.string.valid_dob), Toast.LENGTH_SHORT).show();
                        dateEdt.requestFocus();
                    } else if (TextUtils.isEmpty(mothernameTxt.getText().toString())) {
                        mothernameTxt.setError(getResources().getString(R.string.enter_mother_full_name));
                        mothernameTxt.requestFocus();
                    } else if (TextUtils.isEmpty(fatherEdt.getText().toString())) {
                        fatherEdt.setError(getResources().getString(R.string.enter_father_full_name));
                        fatherEdt.requestFocus();
                    } else {
                        List<NameValuePair> valuePairs = new ArrayList<>();
                        valuePairs.add(new BasicNameValuePair("FirstName", nameEdt.getText().toString()));
                        valuePairs.add(new BasicNameValuePair("LastName", ""));
                        valuePairs.add(new BasicNameValuePair("Gender", gender));
                        valuePairs.add(new BasicNameValuePair("EmailID", emailId.getText().toString()));
                        valuePairs.add(new BasicNameValuePair("FFirstName", fatherEdt.getText().toString()));
                        valuePairs.add(new BasicNameValuePair("FLastName", ""));
                        valuePairs.add(new BasicNameValuePair("MFirstName", mothernameTxt.getText().toString()));
                        valuePairs.add(new BasicNameValuePair("Password", passwordEdt.getText().toString()));
                        valuePairs.add(new BasicNameValuePair("MLastName", ""));
                        valuePairs.add(new BasicNameValuePair("DOB", dateEdt.getText().toString()));
                        valuePairs.add(new BasicNameValuePair("Token", MyApplication.getTokenID()));
                        valuePairs.add(new BasicNameValuePair("Language", MyApplication.getLanguagecode()));

                        if (MyApplication.isInternetWorking(SignUpActivity.this)) {

                            new WebTask(SignUpActivity.this, Urls.registerUrl, valuePairs, SignUpActivity.this, TaskCode.REGISTERCODE).execute();
                        }

                    }
                    return true;
                }
                return false;
            }
        });
        if (

                checkPlayServices())

        {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, SignUpActivity.class);
            startService(intent);
        }

        calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        String dayString = day + "", monthString = (month + 1) + "";

        if ((month + 1) < 10)

        {
            monthString = "0" + (month + 1);
        }
        if (day < 10)

        {
            dayString = "0" + day;
        }

        currentdateString = monthString + "-" + dayString + "-" + year;

        initiateTextWatcher();
    }

    @OnClick(R.id.getstaetedBtn)
    public void getstaetedBtn() {
        MyApplication.myApplication.produceAnimation(getstaetedBtn);
        if (TextUtils.isEmpty(nameEdt.getText().toString())) {
            nameEdt.setError(getResources().getString(R.string.enter_full_name));
            nameEdt.requestFocus();
        } else if (TextUtils.isEmpty(emailId.getText().toString())) {
            emailId.setError(getResources().getString(R.string.Pleaseentervalidemailid));
            emailId.requestFocus();
        } else if (!MyApplication.isValidEmail(emailId.getText().toString())) {
            emailId.setError(getResources().getString(R.string.Pleaseentervalidemailid));
            emailId.requestFocus();
        } else if (TextUtils.isEmpty(passwordEdt.getText().toString())) {
            passwordEdt.setError(getResources().getString(R.string.enter_a_password));
            passwordEdt.requestFocus();
        } else if (!confirmEdt.getText().toString().equalsIgnoreCase(passwordEdt.getText().toString())) {
            confirmEdt.setError(getResources().getString(R.string.confirm_password_above));
            confirmEdt.requestFocus();
        } else if (TextUtils.isEmpty(dateEdt.getText().toString())) {
//            dateEdt.setError(getResources().getString(R.string.valid_dob));
            Toast.makeText(SignUpActivity.this, getResources().getString(R.string.valid_dob), Toast.LENGTH_SHORT).show();
            dateEdt.requestFocus();
        } else if (TextUtils.isEmpty(mothernameTxt.getText().toString())) {
            mothernameTxt.setError(getResources().getString(R.string.enter_mother_full_name));
            mothernameTxt.requestFocus();
        } else if (TextUtils.isEmpty(fatherEdt.getText().toString())) {
            fatherEdt.setError(getResources().getString(R.string.enter_father_full_name));
            fatherEdt.requestFocus();
        } else {

            List<NameValuePair> valuePairs = new ArrayList<>();
            valuePairs.add(new BasicNameValuePair("FirstName", nameEdt.getText().toString()));
            valuePairs.add(new BasicNameValuePair("LastName", ""));
            valuePairs.add(new BasicNameValuePair("Gender", gender));
            valuePairs.add(new BasicNameValuePair("EmailID", emailId.getText().toString()));
            valuePairs.add(new BasicNameValuePair("FFirstName", fatherEdt.getText().toString()));
            valuePairs.add(new BasicNameValuePair("FLastName", ""));
            valuePairs.add(new BasicNameValuePair("Token", MyApplication.getTokenID()));
            valuePairs.add(new BasicNameValuePair("MFirstName", mothernameTxt.getText().toString()));
            valuePairs.add(new BasicNameValuePair("MLastName", ""));
            valuePairs.add(new BasicNameValuePair("Password", passwordEdt.getText().toString()));
            valuePairs.add(new BasicNameValuePair("DOB", dateString));
            valuePairs.add(new BasicNameValuePair("Language", MyApplication.getLanguagecode()));
            if (MyApplication.isInternetWorking(SignUpActivity.this)) {
                new WebTask(SignUpActivity.this, Urls.registerUrl, valuePairs, this, TaskCode.REGISTERCODE).execute();
            }
        }
    }

    @OnClick(R.id.dateEdt)
    public void dateEdt() {
        showDialog(DATE_DIALOG_ID);
    }

    @OnClick(R.id.profileImg)
    public void profileImg() {
        attachImage();
    }


    private String formateDate(String dateString) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat("dd MMMM yyyy");
            Date date = originalFormat.parse(dateString);
            String formattedDate = targetFormat.format(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        Calendar c = Calendar.getInstance(Locale.ENGLISH);

        switch (id) {
            case DATE_DIALOG_ID:
                // set date picker as current date
                return new DatePickerDialog(this, datePickerListener,
                        c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        }
        return null;
    }


    private String dateString;
    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {


        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;

            // set selected date into textview


            String dayString = day + "", monthString = (month + 1) + "";

            if ((month + 1) < 10) {
                monthString = "0" + (month + 1);
            }
            if (day < 10) {
                dayString = "0" + day;
            }
            dateString = monthString + "-" + dayString + "-" + year;

            SimpleDateFormat originalFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.ENGLISH);
            Date bodDate = null, currentDate = null;
            try {
                bodDate = originalFormat.parse(dateString);
                currentDate = originalFormat.parse(currentdateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (!bodDate.after(currentDate)) {
                dateEdt.setText(formateDate(dateString));
            } else {
//                dateEdt.setError(getResources().getString(R.string.valid_dob));
                Toast.makeText(SignUpActivity.this, getResources().getString(R.string.valid_dob), Toast.LENGTH_SHORT).show();
            }

        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK) {

            try {

                System.out.println("on activity result gallery");
                InputStream inputStream = getContentResolver()
                        .openInputStream(data.getData());
                FileOutputStream fileOutputStream = new FileOutputStream(
                        mFileTemp);
                copyStream(inputStream, fileOutputStream);
                fileOutputStream.close();
                inputStream.close();

                startCropImage();

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == REQUEST_CODE_TAKE_PICTURE && resultCode == RESULT_OK) {

            startCropImage();

        } else if (requestCode == REQUEST_CODE_CROP_IMAGE && resultCode == RESULT_OK) {

            System.out.println("on activity result crop");
            String path = data.getStringExtra(CropImage.IMAGE_PATH);
            if (path == null) {
                return;
            }


            imagePath = mFileTemp.getPath();

            MyApplication.loader.displayImage("file://"
                    + imagePath, profileImg, MyApplication.option2);


        }
    }

    private void openGallery() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    public Uri getOutputMediaFileUri() {

        return Uri.fromFile(mFileTemp);
    }

    private void startCropImage() {
        try {
            System.out.println("on activity result startcrop functions");
            Intent intent = new Intent(SignUpActivity.this, CropImage.class);
            intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
            intent.putExtra(CropImage.SCALE, true);

            intent.putExtra(CropImage.ASPECT_X, 1);
            intent.putExtra(CropImage.ASPECT_Y, 1);

            startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    private void attachImage() {
        setimagepath();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                SignUpActivity.this);

        // set title
        alertDialogBuilder.setTitle(getResources().getString(R.string.chooseoption));

        // set dialog message
        alertDialogBuilder
                .setMessage(getResources().getString(R.string.pleaseselectecimagefrom))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.camera),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, closeMdsDB
                                // current activity
                                dialog.cancel();
                                takePicture();

                            }

                        })
                .setNegativeButton(getResources().getString(R.string.gallery),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just closeMdsDB
                                // the dialog box and do nothing
                                dialog.cancel();
                                openGallery();
                            }

                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.setCancelable(true);
        // show it
        alertDialog.show();
    }

    private void setimagepath() {

        String fileName = "IMG_"
                + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date())
                .toString() + ".jpg";
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {

            File sdIconStorageDir = new File(
                    Environment.getExternalStorageDirectory() + "/"
                            + getResources().getString(R.string.app_name) + "/");
            // create storage directories, if they don't exist
            sdIconStorageDir.mkdirs();

            mFileTemp = new File(Environment.getExternalStorageDirectory()
                    + "/" + getResources().getString(R.string.app_name) + "/",
                    fileName);
        } else {
            mFileTemp = new File(getFilesDir(), fileName);
        }
    }

    private void takePicture() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();

            mImageCaptureUri = Uri.fromFile(mFileTemp);

            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);

        } catch (ActivityNotFoundException e) {

            Log.d("", "cannot take picture", e);
        }

    }

    @Override
    public void onComplete(String response, int taskcode) {
        if (taskcode == TaskCode.REGISTERCODE && response != null) {
//            {"StatusCode":1,"Message":"Registration Successful. We have sent password on your registered mail.","NodeId":0}
            try {
                JSONObject responseObj = new JSONObject(response);
                if (responseObj.getInt("StatusCode") == 1) {

                    MyApplication.saveUserId(responseObj.getString("NodeId"));
                    MyApplication.saveEmailId(emailId.getText().toString());
                    MyApplication.savePassword(passwordEdt.getText().toString());
                    MyApplication.saveUserFirstName(responseObj.getString("FirstName"));
                    if (mFileTemp != null && !responseObj.getString("NodeId").equalsIgnoreCase("0")) {
                        HashMap<String, String> params = new HashMap<>();
                        params.put("NodeId", MyApplication.getUserID());
                        MultipartRequest multipartRequest = new MultipartRequest(Urls.updateprofileUrl, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                System.out.println("response" + response);
//                                 {"StatusCode":1,"Message":"Success","NodeId":0}
                                try {
                                    JSONObject responseObj = new JSONObject(response);
                                    if (responseObj.getInt("StatusCode") == 1) {
                                        startActivity(new Intent(SignUpActivity.this, FamilyTreeActivity.class));
                                        finish();
                                    } else {
                                        Toast.makeText(SignUpActivity.this, responseObj.getString("Message"), Toast.LENGTH_SHORT).show();
//                                        startActivity(new Intent(SignUpActivity.this, FamilyTreeActivity.class));
//                                        finish();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }, mFileTemp, params, "Image");
                        Volley.newRequestQueue(SignUpActivity.this).add(multipartRequest);
                    } else {


                        startActivity(new Intent(SignUpActivity.this, FamilyTreeActivity.class));
                        finish();


                    }

                } else {
                    Toast.makeText(SignUpActivity.this, responseObj.getString("Message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            System.out.println("response" + response);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            languageCode(MyApplication.getLanguagecode());
        } else {
            languageCode(MyApplication.getLanguagecode());
        }
    }

    public void languageCode(String code) {
        Locale locale;
        Configuration lconfig;
        locale = new Locale(code);
        Locale.setDefault(locale);
        lconfig = new Configuration();
        lconfig.locale = locale;
        getBaseContext().getResources().updateConfiguration(lconfig,
                getBaseContext().getResources().getDisplayMetrics());
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
//                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private void initiateTextWatcher() {
        nameEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    nameEdt.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        passwordEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    passwordEdt.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        emailId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    emailId.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        confirmEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    passwordEdt.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mothernameTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    mothernameTxt.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        fatherEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    fatherEdt.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
