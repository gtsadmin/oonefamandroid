package com.onefam.activitys;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.gcm.GCMRegistrar;
import com.onefam.R;
import com.onefam.applications.MyApplication;
import com.onefam.datacontroller.AppGlobalData;
import com.onefam.datacontroller.Constants;
import com.onefam.webutility.ParseData;
import com.onefam.webutility.TaskCode;
import com.onefam.webutility.Urls;
import com.onefam.webutility.WebTask;
import com.onefam.webutility.WebTaskComplete;
import com.onefam.wrapper.TreeWrapper;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.leolin.shortcutbadger.ShortcutBadger;

public class FamilyTreeActivity extends AppCompatActivity implements WebTaskComplete {
    @Bind(R.id.viewtreeLayout)
    LinearLayout viewtreeLayout;
    @Bind(R.id.creatememoryLayout)
    LinearLayout creatememoryLayout;
    @Bind(R.id.notificationImg)
    ImageView notificationImg;
    @Bind(R.id.settingImg)
    ImageView settingImg;
    @Bind(R.id.topbarLayout)
    RelativeLayout topbarLayout;
    @Bind(R.id.welcomelogoImg)
    ImageView welcomelogoImg;
    String deviceId = "";
    public static FamilyTreeActivity FamilyTreeActivityObj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.family_tree_layout);
        ButterKnife.bind(this);
        System.out.println("NodeId" + MyApplication.getUserID());
        ShortcutBadger.applyCount(FamilyTreeActivity.this, 0);
        FamilyTreeActivityObj = this;

        changeNotificationImage();
        getFamilyTreeData();
        getTagMemberList();
    }

    public void changeNotificationImage() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // your UI code here
                if (MyApplication.getShowUnreadNotificationStatus()) {
                    notificationImg.setImageResource(R.drawable.icn_notification_unread);
                } else {
                    notificationImg.setImageResource(R.mipmap.icn_notification);
                }
            }
        });

    }

    private void getTagMemberList() {
        List<NameValuePair> valuePairs = new ArrayList<>();
        valuePairs.add(new BasicNameValuePair("NodeId", MyApplication.getUserID()));
        valuePairs.add(new BasicNameValuePair("name", ""));
        if (MyApplication.isInternetWorking(FamilyTreeActivity.this)) {
            new WebTask(FamilyTreeActivity.this, Urls.tagmember, valuePairs, FamilyTreeActivity.this, TaskCode.TAGMEMBER, true).execute();
        }
    }

    private void getFamilyTreeData() {
        if (AppGlobalData.getInstance().getMemberList().size() == 0) {
            if (MyApplication.isInternetWorking(FamilyTreeActivity.this)) {
                List<NameValuePair> valuePairs = new ArrayList<>();
                valuePairs.add(new BasicNameValuePair("NodeId", MyApplication.getUserID()));
                new WebTask(FamilyTreeActivity.this, Urls.getFamalyViewTreeUrl, valuePairs, this, TaskCode.FAMILYVIEWTREECODE, false).execute();
            }
        }

    }

    @OnClick(R.id.viewtreeLayout)
    public void viewtreeLayout() {
        MyApplication.myApplication.produceAnimation(viewtreeLayout);
        startActivity(new Intent(FamilyTreeActivity.this, ViewTreeActivity.class));
    }

    @OnClick(R.id.settingImg)
    public void settingImg() {
        startActivityForResult(new Intent(FamilyTreeActivity.this, SettingActivity.class), Constants.FINISHCODE);
    }

    @OnClick(R.id.notificationImg)
    public void notificationImg() {
        startActivity(new Intent(FamilyTreeActivity.this, NotificatioActivity.class));
    }

    @OnClick(R.id.creatememoryLayout)
    public void creatememoryLayout() {
        MyApplication.myApplication.produceAnimation(creatememoryLayout);
        MyApplication.getInstance().setCreateMemoryNodID(MyApplication.getUserID());
        startActivity(new Intent(FamilyTreeActivity.this, CreateMemoryActivity.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.FINISHCODE) {
                finish();
            }
        }
    }

    @Override
    public void onComplete(String response, int taskcode) {
        if (taskcode == TaskCode.GETMEMBERLISTCODE && response != null) {
            try {
                JSONObject responseObj = new JSONObject(response);
                if (responseObj.has("Members")) {
                    JSONArray membersArr = responseObj.getJSONArray("Members");
                    ArrayList<String> memberList = new ArrayList<>();
                    HashMap<String, String> memberlistMap = new HashMap<>();
                    HashMap<String, String> membernamelistMap = new HashMap<>();
                    for (int i = 0; i < membersArr.length(); i++) {
                        JSONObject subMemberObj = membersArr.getJSONObject(i);
                        memberList.add(subMemberObj.getString("Name"));
                        memberlistMap.put(subMemberObj.getString("Name"), subMemberObj.getString("NodeId"));
                        membernamelistMap.put(subMemberObj.getString("NodeId"), subMemberObj.getString("Name"));
                    }
                    AppGlobalData.appGlobalData.getInstance().setMamberListMap(memberlistMap);
                    AppGlobalData.appGlobalData.getInstance().setMemberList(memberList);
                    AppGlobalData.appGlobalData.getInstance().setMembernamelistMap(membernamelistMap);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (response != null && taskcode == TaskCode.FAMILYVIEWTREECODE) {
            ArrayList<TreeWrapper> memberInfoWrappers = new ParseData(FamilyTreeActivity.this, response, taskcode).getTreeMember();
        } else if (response != null && taskcode == TaskCode.TAGMEMBER) {
            Log.v("tagMemberList ", response);
            parseAllMemberData(response);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            languageCode(MyApplication.getLanguagecode());
            setContentView(R.layout.family_tree_layout_land);
            ButterKnife.bind(this);
        } else {
            languageCode(MyApplication.getLanguagecode());
            setContentView(R.layout.family_tree_layout);
            ButterKnife.bind(this);
        }
    }

    @Override
    public void onBackPressed() {
        logoutAlert();
    }

    private void logoutAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(FamilyTreeActivity.this);
        builder.setTitle("" + getResources().getString(R.string.app_name));
        builder.setMessage(getResources().getString(R.string.Areyousureyouwanttoexitthisapp))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ShortcutBadger.removeCount(FamilyTreeActivity.this);
                        finish();
                    }
                });
        builder.show();
    }

    public void languageCode(String code) {
        Locale locale;
        Configuration lconfig;
        locale = new Locale(code);
        Locale.setDefault(locale);
        lconfig = new Configuration();
        lconfig.locale = locale;
        getBaseContext().getResources().updateConfiguration(lconfig,
                getBaseContext().getResources().getDisplayMetrics());
    }


    ArrayList<String> memberList = new ArrayList<>();
    ArrayList<String> memberListNodeId = new ArrayList<>();
    ArrayList<TreeWrapper> tagMemberArraylist = new ArrayList<>();

    private void parseAllMemberData(String response) {

        if (response != null) {
            try {
                JSONObject responseObj = new JSONObject(response);
                if (responseObj.getString("StatusCode").equalsIgnoreCase("1")) {
                    JSONArray MembersArray = responseObj.getJSONArray("Members");
                    if (MembersArray.length() != 0) {
                        for (int j = 0; j < MembersArray.length(); j++) {
                            TreeWrapper treeWrapper = new TreeWrapper();
                            JSONObject nodesObj = MembersArray.getJSONObject(j);
                            treeWrapper.setNodeId(nodesObj.getString("NodeId"));
                            treeWrapper.setName(nodesObj.getString("Name"));
                            treeWrapper.setImageUrl(nodesObj.getString("Image"));
                            treeWrapper.setGender(nodesObj.getString("Gender"));
                            treeWrapper.setDOB(nodesObj.getString("DateOfBirth"));
                            tagMemberArraylist.add(treeWrapper);

                            memberList.add(nodesObj.getString("Name"));
                            memberListNodeId.add(nodesObj.getString("NodeId"));

                        }
                        AppGlobalData.getInstance().setAllMemberList(memberList);
                        AppGlobalData.getInstance().setAllMemberNodeIdList(memberListNodeId);

                        AppGlobalData.getInstance().getTagMemberArrayList().clear();
                        AppGlobalData.getInstance().setTagMemberArrayList(tagMemberArraylist);

                        AppGlobalData.getInstance().getAllMemberList().add(0, FamilyTreeActivity.this.getResources().getString(R.string.select_member));
                        AppGlobalData.getInstance().getAllMemberNodeIdList().add(0, "");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
