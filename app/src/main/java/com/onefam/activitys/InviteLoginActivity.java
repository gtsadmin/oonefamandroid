package com.onefam.activitys;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.onefam.R;
import com.onefam.applications.MyApplication;
import com.onefam.views.ButtonProximaNovaRegular;
import com.onefam.views.EditTextProximaNovaRegular;
import com.onefam.views.TextViewProximaNovaRegular;
import com.onefam.webutility.TaskCode;
import com.onefam.webutility.Urls;
import com.onefam.webutility.WebTask;
import com.onefam.webutility.WebTaskComplete;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class InviteLoginActivity extends Activity implements WebTaskComplete {
    @Bind(R.id.loginBtn)
    ButtonProximaNovaRegular loginBtn;
    @Bind(R.id.passwordEdt)
    EditTextProximaNovaRegular passwordEdt;
    @Bind(R.id.confirmpasswordEdt)
    EditTextProximaNovaRegular confirmpasswordEdt;
    @Bind(R.id.signupBtn)
    ButtonProximaNovaRegular signupBtn;
    @Bind(R.id.alreadysignupTxt)
    TextViewProximaNovaRegular alreadysignupTxt;
    String token = "";
    String typeinviteStr = "";
    @Bind(R.id.emailId)
    EditTextProximaNovaRegular emailId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.invite_login_layout);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        String action = intent.getAction();
        Uri data = intent.getData();
//        onefam://onefam.com/636127121903364607
//        onefam://onefam.com/
        // onefam://invite/
        String split[] = data.toString().split("//");
        typeinviteStr = split[1];
        if (typeinviteStr.split("/")[0].equalsIgnoreCase("invite")) {
            String inviteStr[] = typeinviteStr.split("/");
            token = inviteStr[1];
        } else {
            emailId.setVisibility(View.GONE);
            String onefamcomStr[] = split[1].split("/");
            token = onefamcomStr[1];
        }


        String text = "If already signup <u><font color='#4ac1d2'>Click Here</font></u>";
        alreadysignupTxt.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);

        if (!TextUtils.isEmpty(MyApplication.getUserID())) {
            Intent peddingInvitionIntent = new Intent(InviteLoginActivity.this, AcceptInviteActivity.class).putExtra("bln", true).putExtra("token", token).putExtra("type", typeinviteStr);
            startActivity(peddingInvitionIntent);
            finish();
        }


    }

    @OnClick({R.id.alreadysignupTxt, R.id.signupBtn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.signupBtn:
                MyApplication.getInstance().hideSoftKeyBoard(InviteLoginActivity.this);
                if (TextUtils.isEmpty(passwordEdt.getText().toString())) {
                    passwordEdt.setError(getString(R.string.please_enter_password));
                    passwordEdt.requestFocus();

                } else if (!passwordEdt.getText().toString().equalsIgnoreCase(confirmpasswordEdt.getText().toString())) {
                    confirmpasswordEdt.setError(getString(R.string.please_enter_password));
                    confirmpasswordEdt.requestFocus();
                } else {

                    if (typeinviteStr.split("/")[0].equalsIgnoreCase("invite")) {
                        if (!MyApplication.isValidEmail(emailId.getText().toString())) {
                            emailId.setError("Enter valid email");
                            emailId.requestFocus();
                        }else if (TextUtils.isEmpty(emailId.getText().toString())) {
                            emailId.setError(getResources().getString(R.string.email));
                            emailId.requestFocus();
                        }
                        else {
                            List<NameValuePair> valuePairs = new ArrayList<>();
                            valuePairs.add(new BasicNameValuePair("Email",emailId.getText().toString()));
                            valuePairs.add(new BasicNameValuePair("Token", token));
                            valuePairs.add(new BasicNameValuePair("Password", passwordEdt.getText().toString()));
                            new WebTask(InviteLoginActivity.this, Urls.mobileinvationsignupURL, valuePairs, InviteLoginActivity.this, TaskCode.ACCEPTREQUEST).execute();
                        }
                    } else {

                        List<NameValuePair> valuePairs = new ArrayList<>();
                        valuePairs.add(new BasicNameValuePair("Token", token));
                        valuePairs.add(new BasicNameValuePair("Password", passwordEdt.getText().toString()));
                        new WebTask(InviteLoginActivity.this, Urls.acceptInvitation, valuePairs, InviteLoginActivity.this, TaskCode.ACCEPTREQUEST).execute();
                    }
                }

                break;
            case R.id.alreadysignupTxt:
                startActivity(new Intent(InviteLoginActivity.this, LoginActivity.class));
                finish();
                break;
        }

    }

    @Override
    public void onComplete(String response, int taskcode) {
//        {"StatusCode":0,"Message":"Invitation Alredy Accepted","NodeId":0}
        if (response != null && taskcode == TaskCode.ACCEPTREQUEST) {
            try {
                JSONObject resObj = new JSONObject(response);
                if (resObj.getInt("StatusCode") == 1) {
                    MyApplication.saveUserId(resObj.getString("NodeId"));
                    MyApplication.saveEmailId(emailId.getText().toString());
                    MyApplication.savePassword(passwordEdt.getText().toString());
                    startActivity(new Intent(InviteLoginActivity.this, FamilyTreeActivity.class));
                    finish();
                } else {
                    Toast.makeText(InviteLoginActivity.this, resObj.getString("Message"), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
