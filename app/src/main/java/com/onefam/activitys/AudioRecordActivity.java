package com.onefam.activitys;

import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.czt.mp3recorder.MP3Recorder;
import com.github.lassana.recorder.AudioRecorder;
import com.onefam.R;
import com.onefam.views.TextViewProximaNovaRegular;
import com.shuyu.waveview.FileUtils;

import java.io.File;
import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AudioRecordActivity extends AppCompatActivity {
    @Bind(R.id.backLayout)
    LinearLayout backLayout;
    @Bind(R.id.doneTxt)
    TextViewProximaNovaRegular doneTxt;
    @Bind(R.id.topbarLayout)
    RelativeLayout topbarLayout;
    @Bind(R.id.playImg)
    ImageView playImg;
    @Bind(R.id.stopImg)
    ImageView stopImg;
    @Bind(R.id.timeTxt)
    TextViewProximaNovaRegular timeTxt;
    @Bind(R.id.recordingnameTxt)
    TextViewProximaNovaRegular recordingnameTxt;
    Handler handler;
    Boolean isRecording = false;
    AudioRecorder recorder;

    //    MediaRecorder mRecorder;
    MP3Recorder mRecorder;
    String fileName;
    MediaPlayer mPlayer;
    int recordTime, playTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.audio_layout);
        ButterKnife.bind(this);
        playImg.setBackgroundResource(R.mipmap.rec_play);
        recordingAudio();
    }

    @OnClick(R.id.backLayout)
    public void backLayout() {

        onBackPressed();
    }

    @OnClick(R.id.doneTxt)
    public void doneTxt() {
        if (mPlayer != null) {

            mPlayer.stop();


        }
        stopRecording();
        finish();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mPlayer != null) {

            mPlayer.stop();


        }
        stopRecording();

        finish();
    }

    @OnClick(R.id.stopImg)
    public void stopImg() {
        stopRecording();
    }

    boolean playBln = true;

    @OnClick(R.id.playImg)
    public void playImg() {

        if (playBln) {
            if (isRecording) {
                stopRecording();
            }
            playIt();
            playImg.setBackgroundResource(R.mipmap.rec_pause);
            playBln = false;
        } else {
            if (mPlayer != null) {
                playImg.setBackgroundResource(R.mipmap.rec_play);
                mPlayer.stop();
                playBln = true;

            }
        }

    }

    public void recordingAudio() {
        handler = new Handler();
//        fileName = Environment.getExternalStorageDirectory() + "/audio" + System.currentTimeMillis() + ".3gp";

        fileName = FileUtils.getAppPath() + "audio/";

        File file = new File(fileName);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                Toast.makeText(this, "Failed to create file", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        fileName = fileName + "audio_" + System.currentTimeMillis() + ".mp3";

        Log.v("audio_file_prep ", fileName);
        if (!isRecording) {
            //Create MediaRecorder and initialize audio source, output format, and audio encoder
            mRecorder = new MP3Recorder(new File(fileName));

//            mRecorder = new MediaRecorder();
////            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
////            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
////            mRecorder.setOutputFile(fileName);
//            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            // Starting record time
            recordTime = 0;
            // Show TextView that displays record time
            try {

//                mRecorder.prepare();
                mRecorder.start();
            } catch (IOException e) {
                Log.e("LOG_TAG", "prepare failed");
            }
            // Start record job
//            mRecorder.start();
            isRecording = true;
            // Change isRecroding flag to true
//            String nameaudioFile = "audio" + System.currentTimeMillis() + ".3gp";
            recordingnameTxt.setText(fileName.toString());
            // Post the record progress
            handler.post(UpdateRecordTime);
        }


    }

    Runnable UpdateRecordTime = new Runnable() {
        public void run() {
            if (isRecording) {
//                tv.setText(String.valueOf(recordTime));
                if (recordTime > 59) {
                    int minute = recordTime / 60;
                    int second = recordTime % 60;
                    if (minute > 59) {
                        int hour = minute / 60;
                        int minute1 = minute % 60;
                        timeTxt.setText(String.valueOf(hour) + ":" + String.valueOf(minute) + ":" + String.valueOf(second));
                    } else if (59 > minute) {
                        timeTxt.setText(String.valueOf(minute) + ":" + String.valueOf(second));
                    }

                } else if (59 > recordTime) {

                    timeTxt.setText("00:" + String.valueOf(recordTime));
                }


                recordTime += 1;
                // Delay 1s before next call
                handler.postDelayed(this, 1000);
            }
        }
    };

    public void stopRecording() {
        if (isRecording) {
            // Stop recording and release resource
            mRecorder.stop();
//            mRecorder.release();
            mRecorder = null;
            // Change isRecording flag to false
            isRecording = false;
            // Hide TextView that shows record time

            playIt(); // Play the audio
        }
    }

    public void playIt() {
        // Create MediaPlayer object
        mPlayer = new MediaPlayer();
        // set start time
        playTime = 0;
        // Reset max and progress of the SeekBar

        try {
            // Initialize the player and start playing the audio
            mPlayer.setDataSource(fileName);
            mPlayer.prepare();
            mPlayer.start();

        } catch (IOException e) {
            Log.e("LOG_TAG", "prepare failed");
        }
    }

}
