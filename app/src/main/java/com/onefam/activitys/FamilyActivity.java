package com.onefam.activitys;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.onefam.R;
import com.onefam.applications.MyApplication;
import com.onefam.datacontroller.AppGlobalData;
import com.onefam.views.CircularImageView;
import com.onefam.views.EditTextProximaNovaRegular;
import com.onefam.views.TextViewProximaNovaRegular;
import com.onefam.webutility.ParseData;
import com.onefam.webutility.TaskCode;
import com.onefam.webutility.Urls;
import com.onefam.webutility.WebTask;
import com.onefam.webutility.WebTaskComplete;
import com.onefam.wrapper.TreeWrapper;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.onefam.R.drawable.index;

public class FamilyActivity extends AppCompatActivity implements WebTaskComplete {
    @Bind(R.id.backLayout)
    LinearLayout backLayout;
    @Bind(R.id.topbarLayout)
    RelativeLayout topbarLayout;
    @Bind(R.id.familyList)
    ListView familyList;
    @Bind(R.id.addmemberLayout)
    LinearLayout addmemberLayout;
    FamilyAdapter familyAdapter;
    @Bind(R.id.listView)
    SwipeMenuListView listView;
    ArrayList<TreeWrapper> memberInfoWrappers;
    int removePos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.family_layout);
        ButterKnife.bind(this);
        languageCode(MyApplication.getLanguagecode());
        MyApplication.getInstance().setLoad_profile(false);
        createDeletebtn();
        listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        AppGlobalData.editmemeberBoolean = true;
                        startActivity(new Intent(FamilyActivity.this, AddMember.class).putExtra("nodeid", memberInfoWrappers.get(position).getNodeId()));
                        break;
                    case 1:
                        AlertDialog.Builder builder = new AlertDialog.Builder(FamilyActivity.this);
                        builder.setMessage(getResources().getString(R.string.Areyousureyouwanttodelete))
                                .setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                removePos = position;
                                List<NameValuePair> valuePairs = new ArrayList<>();
                                valuePairs.add(new BasicNameValuePair("NodeId", memberInfoWrappers.get(position).getNodeId()));
                                valuePairs.add(new BasicNameValuePair("CanDelete", "1"));
                                valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                                if (MyApplication.isInternetWorking(FamilyActivity.this)) {
                                    new WebTask(FamilyActivity.this, Urls.deleteMember, valuePairs, FamilyActivity.this, TaskCode.DELETEMEMBERCODE).execute();
                                }
                            }
                        }).setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                        AlertDialog alert = builder.create();
                        alert.show();
                        break;

                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });
    }

    @OnClick(R.id.backLayout)
    public void backLayout() {
        onBackPressed();

    }

    @OnClick(R.id.addmemberLayout)
    public void addmemberLayout() {
        MyApplication.getInstance().produceAnimation(addmemberLayout);
        startActivity(new Intent(FamilyActivity.this, AddMember.class));
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onComplete(String response, int taskcode) {
        if (response != null && taskcode == TaskCode.FAMILYVIEWTREECODE) {
            Log.v("family_string ", response);
            memberInfoWrappers = new ParseData(FamilyActivity.this, response, taskcode).getTreeMember();
            familyAdapter = new FamilyAdapter(FamilyActivity.this, memberInfoWrappers);
            familyList.setAdapter(familyAdapter);
            AppGlobalData.reloadfamilydataBoolean = false;
//            AppGlobalData.appGlobalData.setMemberInfoWrappers(memberInfoWrappers);

        } else if (response != null && taskcode == TaskCode.DELETEMEMBERCODE) {
            try {
                JSONObject responseObj = new JSONObject(response);
                if (responseObj.getInt("StatusCode") == 1) {
                    AppGlobalData.reloadwebViewBoolean = true;
                    AppGlobalData.reloadfamilydataBoolean = true;
                    getTagMemberList();
//                    memberInfoWrappers.remove(removePos);
//                    familyAdapter.notifyDataSetChanged();
                } else {

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (response != null && taskcode == TaskCode.TAGMEMBER) {
            Log.v("tagMemberList ", response);
            parseAllMemberData(response);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if ((AppGlobalData.getInstance().getTreeWrapperArrayList().size() == 0 || AppGlobalData.reloadfamilydataBoolean == true) && !MyApplication.getViewFamilyTreeId().equalsIgnoreCase(MyApplication.getUserID())) {
        if (!MyApplication.getViewFamilyTreeId().equalsIgnoreCase(MyApplication.getUserID())) {

            if (MyApplication.isInternetWorking(FamilyActivity.this)) {
                List<NameValuePair> valuePairs = new ArrayList<>();
                if (!TextUtils.isEmpty(MyApplication.getViewFamilyTreeId())) {
                    valuePairs.add(new BasicNameValuePair("NodeId", MyApplication.getViewFamilyTreeId()));
                } else {
                    valuePairs.add(new BasicNameValuePair("NodeId", MyApplication.getUserID()));
                }
                Log.v("value_node_id ", valuePairs.get(0).toString());
                new WebTask(FamilyActivity.this, Urls.getFamalyViewTreeUrl, valuePairs, this, TaskCode.FAMILYVIEWTREECODE).execute();
            }
        } else {
            memberInfoWrappers = new ArrayList<>();
            memberInfoWrappers.addAll(AppGlobalData.getInstance().getTreeWrapperArrayList());
            familyAdapter = new FamilyAdapter(FamilyActivity.this, memberInfoWrappers);
            familyList.setAdapter(familyAdapter);
            AppGlobalData.reloadwebViewBoolean = true;
        }
    }

    public class FamilyAdapter extends BaseAdapter {
        ArrayList<TreeWrapper> memberInfoWrappers;
        Context context;


        public FamilyAdapter(Context context, ArrayList<TreeWrapper> memberInfoWrappers) {
            this.context = context;
            this.memberInfoWrappers = memberInfoWrappers;
        }

        @Override
        public int getCount() {
            return memberInfoWrappers.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder = null;

            if (convertView == null) {
                viewHolder = new ViewHolder();
                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.people_search_list_layout, null);

                viewHolder.titleTxt = (TextViewProximaNovaRegular) convertView.findViewById(R.id.titleTxt);
                viewHolder.nameTxt = (TextViewProximaNovaRegular) convertView.findViewById(R.id.nameTxt);
                viewHolder.lavalTxt = (TextViewProximaNovaRegular) convertView.findViewById(R.id.lavalTxt);
                viewHolder.profileImg = (ImageView) convertView.findViewById(R.id.profileImg);
                viewHolder.dobTxt = (TextViewProximaNovaRegular) convertView.findViewById(R.id.dobTxt);
                viewHolder.dodTxt = (TextViewProximaNovaRegular) convertView.findViewById(R.id.dodTxt);
                viewHolder.sendinvitationTxt = (TextViewProximaNovaRegular) convertView.findViewById(R.id.sendinvitationTxt);
                viewHolder.sideView = (ImageView) convertView.findViewById(R.id.sideView);
                viewHolder.deleteLayout = (LinearLayout) convertView.findViewById(R.id.deleteLayout);
                viewHolder.editLayout = (LinearLayout) convertView.findViewById(R.id.editLayout);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.deleteLayout.setVisibility(View.VISIBLE);
            viewHolder.editLayout.setVisibility(View.VISIBLE);
            viewHolder.deleteLayout.setTag(position);
            viewHolder.editLayout.setTag(position);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent profile_intent = new Intent(FamilyActivity.this, ProfileActivity.class).putExtra("id", memberInfoWrappers.get(position).getNodeId());
                    startActivity(profile_intent);
                }
            });
            viewHolder.nameTxt.setText(memberInfoWrappers.get(position).getName());
            viewHolder.lavalTxt.setText(memberInfoWrappers.get(position).getRelationLevel());

            if (memberInfoWrappers.get(position).getGender().equalsIgnoreCase("M")) {
                viewHolder.titleTxt.setTextColor(getResources().getColor(R.color.board_blue_color));
                viewHolder.sideView.setBackgroundColor(getResources().getColor(R.color.board_blue_color));
            } else {
                viewHolder.titleTxt.setTextColor(getResources().getColor(R.color.bottom_btn_two));
                viewHolder.sideView.setBackgroundColor(getResources().getColor(R.color.bottom_btn_two));
            }

            viewHolder.titleTxt.setText(memberInfoWrappers.get(position).getRelation());
            if (memberInfoWrappers.get(position).getCanDelete().equalsIgnoreCase("-1")) {
                viewHolder.deleteLayout.setVisibility(View.GONE);
            } else {
                viewHolder.deleteLayout.setVisibility(View.VISIBLE);
            }
            viewHolder.dodTxt.setVisibility(View.GONE);

            viewHolder.deleteLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final int pos = (int) view.getTag();
                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            FamilyActivity.this);

                    alert.setMessage(getResources().getString(R.string.Areyousureyouwanttodelete));
                    alert.setPositiveButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                        }
                    });
                    alert.setNegativeButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                            List<NameValuePair> valuePairs = new ArrayList<>();
                            valuePairs.add(new BasicNameValuePair("NodeId", memberInfoWrappers.get(pos).getNodeId()));
                            valuePairs.add(new BasicNameValuePair("CanDelete", "1"));
                            valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                            if (MyApplication.isInternetWorking(FamilyActivity.this)) {
                                memberInfoWrappers.remove(pos);
                                familyAdapter.notifyDataSetChanged();
//                                Log.v("remove_data_name ", AppGlobalData.getInstance().getAllMemberList().get(pos));
//                                Log.v("remove_data_id ", AppGlobalData.getInstance().getAllMemberNodeIdList().get(pos));
//                                AppGlobalData.getInstance().getAllMemberList().remove(AppGlobalData.getInstance().getAllMemberList().indexOf(memberInfoWrappers.get(pos).getName()));
//                                AppGlobalData.getInstance().getAllMemberNodeIdList().remove(AppGlobalData.getInstance().getAllMemberNodeIdList().indexOf(memberInfoWrappers.get(pos).getNodeId()));
//
//
//
//                                for (int i = 0; i < AppGlobalData.getInstance().getTagMemberArrayList().size(); i++) {
//                                    if ((memberInfoWrappers.get(pos).getNodeId().equalsIgnoreCase(AppGlobalData.getInstance().getTagMemberArrayList().get(i).getNodeId()))){
//                                        Log.v("remove_data_object ", AppGlobalData.getInstance().getTagMemberArrayList().get(i).getNodeId());
//
//                                        AppGlobalData.getInstance().getTagMemberArrayList().remove(i);
//                                    }
//                                }
                                new WebTask(FamilyActivity.this, Urls.deleteMember, valuePairs, FamilyActivity.this, TaskCode.DELETEMEMBERCODE, false).execute();
                            }

                        }
                    });

                    alert.show();
                }
            });
            viewHolder.editLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AppGlobalData.editmemeberBoolean = true;
                    startActivity(new Intent(FamilyActivity.this, AddMember.class).putExtra("nodeid", memberInfoWrappers.get(position).getNodeId()));
                }
            });

            MyApplication.loader.displayImage("https://onefam.com" + memberInfoWrappers.get(position).getImageUrl(), viewHolder.profileImg, MyApplication.options);
//            }
            if (!TextUtils.isEmpty(memberInfoWrappers.get(position).getDOB())) {
                viewHolder.dobTxt.setVisibility(View.VISIBLE);
                viewHolder.dobTxt.setText(getResources().getString(R.string.birth) + ": " + formateDate(memberInfoWrappers.get(position).getDOB()));
            } else {
                viewHolder.dobTxt.setVisibility(View.INVISIBLE);
                viewHolder.dobTxt.setText("");

            }
            if (memberInfoWrappers.get(position).getCanInvite().equalsIgnoreCase("Y")) {
                viewHolder.sendinvitationTxt.setVisibility(View.VISIBLE);
            } else if (memberInfoWrappers.get(position).getCanInvite().equalsIgnoreCase("N")) {
                viewHolder.sendinvitationTxt.setVisibility(View.INVISIBLE);

            }
            viewHolder.sendinvitationTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendInviteDialog(memberInfoWrappers.get(position).getNodeId(), "", memberInfoWrappers.get(position).getName());
                }
            });
//            viewHolder.
            return convertView;
        }

        public class ViewHolder {
            ImageView profileImg;
            //            TextViewProximaNovaBold nameTxt;
            ImageView sideView;
            TextViewProximaNovaRegular dobTxt, dodTxt, lavalTxt, titleTxt, nameTxt, sendinvitationTxt;
            LinearLayout editLayout, deleteLayout;
        }
    }

    public void createDeletebtn() {
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background

                openItem.setBackground(new ColorDrawable(Color.parseColor("#4ac1d2")));
                // set item width
                openItem.setWidth((int) getResources().getDimension(R.dimen.dim_50));
                // set item title
                openItem.setIcon(R.mipmap.member_edit);
                openItem.setTitle("Edit");

                // set item title fontsize
                openItem.setTitleSize(15);
                // set item title font color
                openItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(openItem);
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.parseColor("#df705c")));
                // set item width
                deleteItem.setWidth((int) getResources().getDimension(R.dimen.dim_50));
                openItem.setIcon(R.mipmap.member_delete);
                deleteItem.setTitle("Delete");
                // set item title fontsize
                deleteItem.setTitleSize(15);
                // set item title font color
                deleteItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(deleteItem);

            }
        };

// set creator

        listView.setMenuCreator(creator);
    }

    private String formateDate(String dateString) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat("dd MMMM yyyy");
            Date date = originalFormat.parse(dateString);
            String formattedDate = targetFormat.format(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            languageCode(MyApplication.getLanguagecode());
        } else {
            languageCode(MyApplication.getLanguagecode());
        }
    }

    public void languageCode(String code) {
        Locale locale;
        Configuration lconfig;
        locale = new Locale(code);
        Locale.setDefault(locale);
        lconfig = new Configuration();
        lconfig.locale = locale;
        getBaseContext().getResources().updateConfiguration(lconfig,
                getBaseContext().getResources().getDisplayMetrics());
    }

    Dialog dialog;

    public void sendInviteDialog(final String nodeid, final String EmailAddress, String name) {
        dialog = new Dialog(FamilyActivity.this, R.style.Theme_Custom);
        dialog.setContentView(R.layout.send_invitation_popup);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        TextViewProximaNovaRegular cancelTxt = (TextViewProximaNovaRegular) dialog.findViewById(R.id.cancelTxt);
        TextViewProximaNovaRegular inviteTxt = (TextViewProximaNovaRegular) dialog.findViewById(R.id.inviteTxt);
        TextViewProximaNovaRegular nameTxt = (TextViewProximaNovaRegular) dialog.findViewById(R.id.nameTxt);
        TextViewProximaNovaRegular sendinvitationTxt = (TextViewProximaNovaRegular) dialog.findViewById(R.id.inviteTxt);
//        nameTxt.setText(nameTxt.getText().toString());
        nameTxt.setText(name);
        final EditTextProximaNovaRegular emailEdt = (EditTextProximaNovaRegular) dialog.findViewById(R.id.emailEdt);
        ImageView facebookImg = (ImageView) dialog.findViewById(R.id.facebookImg);
        ImageView smsImg = (ImageView) dialog.findViewById(R.id.smsImg);
        ImageView whatsappImg = (ImageView) dialog.findViewById(R.id.whatsappImg);
        ImageView wechatImg = (ImageView) dialog.findViewById(R.id.wechatImg);
        ImageView viberImg = (ImageView) dialog.findViewById(R.id.viberImg);
        viberImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().hideSoftKeyBoard(FamilyActivity.this);
                List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                valuePairs.add(new BasicNameValuePair("NodeId", nodeid));
                String url = "";
                if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                    valuePairs.add(new BasicNameValuePair("EmailAddress", EmailAddress));
                    valuePairs.add(new BasicNameValuePair("NewEmailAddress", emailEdt.getText().toString()));
                    url = Urls.mobileinviteUrl;
                } else {
                    url = Urls.mobileinviteUrl;
                }
                valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                new WebTask(FamilyActivity.this, url, valuePairs, new WebTaskComplete() {
                    @Override
                    public void onComplete(String response, int taskcode) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("StatusCode") == 1) {
                                String inviteUrl = MyApplication.getUserFirstName() + " " + getResources().getString(R.string.invitation_message) + " " + responseObj.getString("InvitationLink");
                                Intent i = new Intent(Intent.ACTION_SEND);
                                i.setPackage("com.viber.voip");
                                i.setType("text/plain");
                                i.putExtra(Intent.EXTRA_TEXT, inviteUrl);
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                            } else {
                                dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, TaskCode.SENDINVITATION).execute();

            }
        });
        wechatImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().hideSoftKeyBoard(FamilyActivity.this);
                List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                valuePairs.add(new BasicNameValuePair("NodeId", nodeid));
                String url = "";
                if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                    valuePairs.add(new BasicNameValuePair("EmailAddress", EmailAddress));
                    valuePairs.add(new BasicNameValuePair("NewEmailAddress", emailEdt.getText().toString()));
                    url = Urls.mobileinviteUrl;
                } else {
                    url = Urls.mobileinviteUrl;
                }
                valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                new WebTask(FamilyActivity.this, url, valuePairs, new WebTaskComplete() {
                    @Override
                    public void onComplete(String response, int taskcode) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("StatusCode") == 1) {
                                String inviteUrl = MyApplication.getUserFirstName() + " " + getResources().getString(R.string.invitation_message) + " " + responseObj.getString("InvitationLink");
                                shareonWeChat(inviteUrl);
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                            } else {
                                dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, TaskCode.SENDINVITATION).execute();
            }
        });
        whatsappImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().hideSoftKeyBoard(FamilyActivity.this);
                List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                valuePairs.add(new BasicNameValuePair("NodeId", nodeid));
                String url = "";
                if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                    valuePairs.add(new BasicNameValuePair("EmailAddress", EmailAddress));
                    valuePairs.add(new BasicNameValuePair("NewEmailAddress", emailEdt.getText().toString()));
                    url = Urls.mobileinviteUrl;
                } else {
                    url = Urls.mobileinviteUrl;
                }
                valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                new WebTask(FamilyActivity.this, url, valuePairs, new WebTaskComplete() {
                    @Override
                    public void onComplete(String response, int taskcode) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("StatusCode") == 1) {
                                String inviteUrl = MyApplication.getUserFirstName() + " " + getResources().getString(R.string.invitation_message) + " " + responseObj.getString("InvitationLink");
                                onClickWhatsApp(inviteUrl);
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                            } else {
                                dialog.dismiss();
                                Toast.makeText(FamilyActivity.this, responseObj.getString("Message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, TaskCode.SENDINVITATION).execute();
            }
        });
        smsImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().hideSoftKeyBoard(FamilyActivity.this);
                List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                valuePairs.add(new BasicNameValuePair("NodeId", nodeid));
                String url = "";
                if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                    valuePairs.add(new BasicNameValuePair("EmailAddress", EmailAddress));
                    valuePairs.add(new BasicNameValuePair("NewEmailAddress", emailEdt.getText().toString()));
                    url = Urls.mobileinviteUrl;
                } else {
                    url = Urls.mobileinviteUrl;
                }
                valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                new WebTask(FamilyActivity.this, url, valuePairs, new WebTaskComplete() {
                    @Override
                    public void onComplete(String response, int taskcode) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("StatusCode") == 1) {
                                String inviteUrl = MyApplication.getUserFirstName() + " " + getResources().getString(R.string.invitation_message) + " " + responseObj.getString("InvitationLink");
                                PackageManager pm = getPackageManager();
                                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                                sendIntent.setData(Uri.parse("sms:"));
                                sendIntent.putExtra("sms_body", inviteUrl);
                                startActivity(sendIntent);
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                            } else {
                                dialog.dismiss();
                                Toast.makeText(FamilyActivity.this, responseObj.getString("Message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, TaskCode.SENDINVITATION).execute();
            }
        });
        facebookImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().hideSoftKeyBoard(FamilyActivity.this);
                List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                valuePairs.add(new BasicNameValuePair("NodeId", nodeid));
                String url = "";
                if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                    valuePairs.add(new BasicNameValuePair("EmailAddress", EmailAddress));
                    valuePairs.add(new BasicNameValuePair("NewEmailAddress", emailEdt.getText().toString()));
                    url = Urls.mobileinviteUrl;
                } else {
                    url = Urls.mobileinviteUrl;
                }
                valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                new WebTask(FamilyActivity.this, url, valuePairs, new WebTaskComplete() {
                    @Override
                    public void onComplete(String response, int taskcode) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            if (responseObj.getInt("StatusCode") == 1) {
                                String inviteUrl = MyApplication.getUserFirstName() + " " + getResources().getString(R.string.invitation_message) + " " + responseObj.getString("InvitationLink");
                                shareFb(inviteUrl, emailEdt.getText().toString());
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                            } else {
                                dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, TaskCode.SENDINVITATION).execute();

            }
        });
        sendinvitationTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                    MyApplication.getInstance().hideSoftKeyBoard(FamilyActivity.this);
                    List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();
                    valuePairs.add(new BasicNameValuePair("NodeId", nodeid));
                    valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
                    new WebTask(FamilyActivity.this, Urls.mobileinviteUrl, valuePairs, new WebTaskComplete() {
                        @Override
                        public void onComplete(String response, int taskcode) {
                            try {
                                JSONObject responseObj = new JSONObject(response);
                                if (responseObj.getInt("StatusCode") == 1) {
                                    if (dialog != null) {
                                        dialog.dismiss();
                                    }
                                } else {
                                    dialog.dismiss();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, TaskCode.SENDINVITATION).execute();
                } else {
                    Toast.makeText(FamilyActivity.this, getString(R.string.please_enter_email), Toast.LENGTH_SHORT).show();
                }
            }
        });
        cancelTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                MyApplication.getInstance().hideSoftKeyBoard(FamilyActivity.this);
            }
        });
    }

    public void shareFb(String url, String email) {
        try {
            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
            sendIntent.setType("plain/text");
            sendIntent.setData(Uri.parse(email));
            sendIntent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
            sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.join_your_family_tree));
            sendIntent.putExtra(Intent.EXTRA_TEXT, url);
            startActivity(sendIntent);
        } catch (Exception e) {
            Intent sendIntent = new Intent(Intent.ACTION_SEND);
            sendIntent.setType("plain/text");
//            sendIntent.setData(Uri.parse("mailto:" + email));
            sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.join_your_family_tree));
            sendIntent.putExtra(Intent.EXTRA_TEXT, url);
            startActivity(sendIntent);

        }
    }

    public void onClickWhatsApp(String url) {
        PackageManager pm = getPackageManager();
        try {
            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
            //Check if package exists or not. If not then code
            //in catch block will be called
            waIntent.setPackage("com.whatsapp");
            waIntent.putExtra(Intent.EXTRA_TEXT, url);
            startActivity(Intent.createChooser(waIntent, "Share with"));
        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(this, "WhatsApp not Installed", Toast.LENGTH_SHORT).show();
        }
    }

    public void shareonWeChat(String url) {
//        WXWebpageObject webpage = new WXWebpageObject();
//        webpage.webpageUrl = "http://www.wechat.com";
//        WXMediaMessage msg = new WXMediaMessage(webpage);
//        msg.title = "Wechat homepage";
//        msg.description=url;

        try {
            PackageManager pm = getPackageManager();

            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            PackageInfo info = pm.getPackageInfo("com.tencent.mm", PackageManager.GET_META_DATA);
            //Check if package exists or not. If not then code
            //in catch block will be called
            waIntent.setPackage("com.tencent.mm");
            waIntent.putExtra(Intent.EXTRA_TEXT, url);
            startActivity(Intent.createChooser(waIntent, "Share with"));
        } catch (Exception ex) {
            Toast.makeText(this, "WeChat not Installed", Toast.LENGTH_SHORT).show();
            ex.printStackTrace();
        }
    }


    private void getTagMemberList() {
        List<NameValuePair> valuePairs = new ArrayList<>();
        valuePairs.add(new BasicNameValuePair("NodeId", MyApplication.getUserID()));
        valuePairs.add(new BasicNameValuePair("name", ""));
        if (MyApplication.isInternetWorking(FamilyActivity.this)) {
            addmemberLayout.setClickable(false);
            new WebTask(FamilyActivity.this, Urls.tagmember, valuePairs, FamilyActivity.this, TaskCode.TAGMEMBER, true).execute();
        }
    }

    ArrayList<String> memberList = new ArrayList<>();
    ArrayList<String> memberListNodeId = new ArrayList<>();
    ArrayList<TreeWrapper> tagMemberArraylist = new ArrayList<>();

    private void parseAllMemberData(String response) {

        if (response != null) {
            try {
                JSONObject responseObj = new JSONObject(response);
                if (responseObj.getString("StatusCode").equalsIgnoreCase("1")) {
                    JSONArray MembersArray = responseObj.getJSONArray("Members");
                    if (MembersArray.length() != 0) {
                        for (int j = 0; j < MembersArray.length(); j++) {
                            TreeWrapper treeWrapper = new TreeWrapper();
                            JSONObject nodesObj = MembersArray.getJSONObject(j);
                            treeWrapper.setNodeId(nodesObj.getString("NodeId"));
                            treeWrapper.setName(nodesObj.getString("Name"));
                            treeWrapper.setImageUrl(nodesObj.getString("Image"));
                            treeWrapper.setGender(nodesObj.getString("Gender"));
                            treeWrapper.setDOB(nodesObj.getString("DateOfBirth"));
                            tagMemberArraylist.add(treeWrapper);

                            memberList.add(nodesObj.getString("Name"));
                            memberListNodeId.add(nodesObj.getString("NodeId"));

                        }
                        AppGlobalData.getInstance().getAllMemberList().clear();
                        AppGlobalData.getInstance().getAllMemberNodeIdList().clear();

                        AppGlobalData.getInstance().setAllMemberList(memberList);
                        AppGlobalData.getInstance().setAllMemberNodeIdList(memberListNodeId);

                        AppGlobalData.getInstance().getTagMemberArrayList().clear();
                        AppGlobalData.getInstance().setTagMemberArrayList(tagMemberArraylist);

                        AppGlobalData.getInstance().getAllMemberList().add(0, FamilyActivity.this.getResources().getString(R.string.select_member));
                        AppGlobalData.getInstance().getAllMemberNodeIdList().add(0, "");

                        addmemberLayout.setClickable(true);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


}
