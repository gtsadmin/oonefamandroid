package com.onefam.activitys;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.onefam.R;
import com.onefam.applications.MyApplication;
import com.onefam.datacontroller.AppGlobalData;
import com.onefam.webutility.Urls;

import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class PrivacyActivity extends AppCompatActivity {

    @Bind(R.id.topbarLayout)
    RelativeLayout topbarLayout;
    @Bind(R.id.backLayout)
    LinearLayout backLayout;
    @Bind(R.id.webView)
    WebView webView;
    @Bind(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.privacy_layout);
        ButterKnife.bind(this);
        if (MyApplication.isInternetWorking(PrivacyActivity.this)) {

            String url = Urls.privacyURL;
            webView.setWebViewClient(new MyBrowser());
//                webView.getSettings().setLoadsImagesAutomatically(true);
//                webView.getSettings().setLoadsImagesAutomatically(true);
            webView.getSettings().setJavaScriptEnabled(true);

            webView.setInitialScale(1);
//        webView.getSettings().setAppCacheMaxSize( 5 * 1024 * 1024 ); // 5MB
//        webView.getSettings().setAppCachePath(getApplicationContext().getCacheDir().getAbsolutePath());
//        webView.getSettings().setAllowFileAccess(true);
//        webView.getSettings().setAppCacheEnabled(true);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setUseWideViewPort(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setPluginState(WebSettings.PluginState.ON);
            webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
            webView.getSettings().setCacheMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            webView.getSettings().setLoadsImagesAutomatically(true);
            webView.getSettings().setDomStorageEnabled(true);
//        webView.getSettings().setCacheMode( WebSettings.LOAD_DEFAULT );

            if (Build.VERSION.SDK_INT >= 11) {
                webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            }
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
                webView.getSettings().setDisplayZoomControls(false);
            }
            webView.loadUrl(url);

        }
    }

    @OnClick(R.id.backLayout)
    public void backLayout() {
        finish();
    }

    private class MyBrowser extends WebViewClient {
//        ProgressDialog dialog = ProgressDialog.show(ViewTreeActivity.this, "", "Please wait...");

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            progressBar.setVisibility(View.GONE);
            AppGlobalData.reloadwebViewBoolean = false;


        }

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
            AlertDialog.Builder builder = new AlertDialog.Builder(PrivacyActivity.this);
            builder.setMessage("Ssl Error");
            builder.setPositiveButton("Contiue", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.proceed();
                }
            });
            builder.setNegativeButton(getResources().getString(R.string.Cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.cancel();
                }
            });
            final AlertDialog dialog = builder.create();
            dialog.show();
        }

        @Override
        public void onReceivedError(WebView view, int errorCod, String description, String failingUrl) {
            Toast.makeText(PrivacyActivity.this, "Your Internet Connection May not be active Or " + description, Toast.LENGTH_LONG).show();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            view.loadUrl(url);
            return true;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            languageCode(MyApplication.getLanguagecode());
        } else {
            languageCode(MyApplication.getLanguagecode());
        }
    }

    public void languageCode(String code) {
        Locale locale;
        Configuration lconfig;
        locale = new Locale(code);
        Locale.setDefault(locale);
        lconfig = new Configuration();
        lconfig.locale = locale;
        getBaseContext().getResources().updateConfiguration(lconfig,
                getBaseContext().getResources().getDisplayMetrics());
    }
}
