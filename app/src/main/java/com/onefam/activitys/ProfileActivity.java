package com.onefam.activitys;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.onefam.R;
import com.onefam.applications.MyApplication;
import com.onefam.datacontroller.AppGlobalData;
import com.onefam.datacontroller.Constants;
import com.onefam.views.TextViewProximaNovaRegular;
import com.onefam.webutility.MultipartRequest;
import com.onefam.webutility.ParseData;
import com.onefam.webutility.TaskCode;
import com.onefam.webutility.Urls;
import com.onefam.webutility.WebTask;
import com.onefam.webutility.WebTaskComplete;
import com.onefam.wrapper.GetProfileMediaWrapper;
import com.onefam.wrapper.TreeWrapper;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import eu.janmuller.android.simplecropimage.CropImage;
import me.leolin.shortcutbadger.ShortcutBadger;

import static com.onefam.R.id.profileImg;

public class ProfileActivity extends AppCompatActivity implements WebTaskComplete {
    @Bind(R.id.backLayout)
    LinearLayout backLayout;
    @Bind(R.id.topbarLayout)
    RelativeLayout topbarLayout;
    @Bind(profileImg)
    ImageView profileImgCamera;
    @Bind(R.id.nameTxt)
    TextViewProximaNovaRegular nameTxt;
    @Bind(R.id.dobTxt)
    TextViewProximaNovaRegular dobTxt;
    @Bind(R.id.dodTxt)
    TextViewProximaNovaRegular dodTxt;
    TreeWrapper treeWrapper;
    public static final int REQUEST_CODE_GALLERY = 0x51;
    public static final int REQUEST_CODE_CROP_IMAGE = 0x53;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x52;
    @Bind(R.id.mediaLayout)
    LinearLayout mediaLayout;

    @Bind(R.id.photosmediaLayout)
    LinearLayout photosmediaLayout;

    @Bind(R.id.audiomediaLayout)
    LinearLayout audiomediaLayout;

    @Bind(R.id.profile_image)
    CircleImageView profileImage;
    @Bind(R.id.closeLayout)
    LinearLayout closeLayout;
    @Bind(R.id.doneTxt)
    ImageView doneTxt;
    @Bind(R.id.tiltle)
    RelativeLayout tiltle;
    @Bind(R.id.timeTxt)
    TextViewProximaNovaRegular timeTxt;
    @Bind(R.id.playImg)
    ImageView playImg;
    @Bind(R.id.stopImg)
    ImageView stopImg;
    @Bind(R.id.progressBar1)
    ProgressBar progressBar1;
    @Bind(R.id.recordingnameTxt)
    TextViewProximaNovaRegular recordingnameTxt;
    @Bind(R.id.audioLayout)
    RelativeLayout audioLayout;
    @Bind(R.id.attachImg)
    ImageView attachImg;


    private File mFileTemp;
    private String imagePath = "", images_uptedates = "";
    String nodeId = "";
    String user_name = "", user_image = "";
    MediaPlayer mediaPlayer;
    Handler myHandler;
    boolean not_recordData = true;
    Binder bind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_layout);
        ButterKnife.bind(this);
        MyApplication.getInstance().setLoad_profile(false);
        try {
//            treeWrapper = (TreeWrapper) getIntent().getExtras().getSerializable("model");
            nodeId = getIntent().getExtras().getString("id");
            MyApplication.getInstance().setCreateMemoryNodID(nodeId);
//            if (!treeWrapper.getDOB().equalsIgnoreCase("")) {
//                dobTxt.setText("Birth : " + formateDate(treeWrapper.getDOB()));
//                if (!treeWrapper.getBirthAddress().equalsIgnoreCase("")) {
//                    dobTxt.setText("Birth : " + formateDate(treeWrapper.getDOB()) + " , " + treeWrapper.getBirthAddress());
//                }
//            }

            if (MyApplication.isInternetWorking(ProfileActivity.this)) {

                getProfileMedia();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        MyApplication.sp = getSharedPreferences("OneFam", 0);
        SharedPreferences.Editor edit = MyApplication.sp.edit();
        edit.putInt("Badger", 0);
        edit.commit();
        ShortcutBadger.removeCount(ProfileActivity.this); //for 1.1.4+
//        dodTxt.setText("Death : "+treeWrapper.get);
    }

    @OnClick(R.id.attachImg)
    public void attachImg() {
        startActivity(new Intent(ProfileActivity.this, CreateMemoryActivity.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (MyApplication.getInstance().isLoad_profile()) {
            if (MyApplication.isInternetWorking(ProfileActivity.this)) {
                mediaLayout.removeAllViews();
                photosmediaLayout.removeAllViews();
                audiomediaLayout.removeAllViews();

                getProfileMedia();
                MyApplication.getInstance().setLoad_profile(false);
            }
        }
    }
//    @OnClick(R.id.photosviewsallTxt)
//    public void photosviewsallTxt() {
//        Intent photoIntent = new Intent(ProfileActivity.this, PhotosVideosActivity.class);
//        photoIntent.putExtra("title", Constants.PHOTOS);
//        startActivity(photoIntent);
//    }
//
//    @OnClick(R.id.videosviewsallTxt)
//    public void videosviewsallTxt() {
//        Intent videoIntent = new Intent(ProfileActivity.this, PhotosVideosActivity.class);
//        videoIntent.putExtra("title", Constants.VIDEOS);
//        startActivity(videoIntent);
//    }

    @OnClick(R.id.backLayout)
    public void backLayout() {
        onBackPressed();
    }

    @OnClick(profileImg)
    public void profileImg() {
        attachImage();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    boolean startPuase = true;

    @OnClick(R.id.playImg)
    public void playImg() {
        if (startPuase) {
            if (mediaPlayer != null) {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                    playImg.setBackgroundResource(R.mipmap.rec_play);
                    startPuase = false;
                }
            }

        } else {
            if (mediaPlayer != null) {

                mediaPlayer.start();
                playImg.setBackgroundResource(R.mipmap.rec_pause);
                startPuase = true;

            }

        }
    }

    @OnClick(R.id.closeLayout)
    public void closeLayout() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }
        uptoButtom(audioLayout);
    }

    @OnClick(R.id.stopImg)
    public void stopImg() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }
        uptoButtom(audioLayout);
    }

    public void getProfileMedia() {
        List<NameValuePair> valuePairs = new ArrayList<>();
        valuePairs.add(new BasicNameValuePair("UserId", nodeId));
        new WebTask(ProfileActivity.this, Urls.getProfiledataUrl, valuePairs, this, TaskCode.GETPROFILEDATACODE).execute();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK) {

            try {

                System.out.println("on activity result gallery");
                InputStream inputStream = getContentResolver()
                        .openInputStream(data.getData());
                FileOutputStream fileOutputStream = new FileOutputStream(
                        mFileTemp);
                copyStream(inputStream, fileOutputStream);
                fileOutputStream.close();
                inputStream.close();

                startCropImage();

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == REQUEST_CODE_TAKE_PICTURE && resultCode == RESULT_OK) {

            startCropImage();

        } else if (requestCode == REQUEST_CODE_CROP_IMAGE && resultCode == RESULT_OK) {

            try {
                System.out.println("on activity result crop");
                String path = data.getStringExtra(CropImage.IMAGE_PATH);
                if (path == null) {
                    return;
                }

                imagePath = mFileTemp.getPath();

                MyApplication.loader.displayImage("file://"
                        + imagePath, profileImage);
                setProfileImgAlert();
            } catch (Exception e) {
                Log.e("photoCapture ", e.toString());
                Toast.makeText(ProfileActivity.this, "Issue in getting image", Toast.LENGTH_SHORT).show();
            }


        } else if (requestCode == 100 && resultCode == RESULT_OK) {
            finish();
        }
    }

    private void openGallery() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    public Uri getOutputMediaFileUri() {
        return Uri.fromFile(mFileTemp);
    }

    private void startCropImage() {
        try {
            System.out.println("on activity result startcrop functions");
            Intent intent = new Intent(ProfileActivity.this, CropImage.class);
            intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
            intent.putExtra(CropImage.SCALE, true);

            intent.putExtra(CropImage.ASPECT_X, 1);
            intent.putExtra(CropImage.ASPECT_Y, 1);

            startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    private void setProfileImgAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setTitle("" + getResources().getString(R.string.app_name));
        builder.setMessage(getResources().getString(R.string.Areyousureyouwanttochangeprofileimage))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.dismiss();
                            }
                        })
                .setNegativeButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.cancel();
                        uploadFile();

                    }
                });
        builder.show();
    }

    private void attachImage() {
        setimagepath();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                ProfileActivity.this);

        // set title
        alertDialogBuilder.setTitle(getResources().getString(R.string.chooseoption));

        // set dialog message
        alertDialogBuilder
                .setMessage(getResources().getString(R.string.pleaseselectecimagefrom))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.camera),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, closeMdsDB
                                // current activity
                                dialog.cancel();
                                takePicture();

                            }

                        })
                .setNegativeButton(getResources().getString(R.string.gallery),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just closeMdsDB
                                // the dialog box and do nothing
                                dialog.cancel();
                                openGallery();
                            }

                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.setCancelable(true);
        // show it
        alertDialog.show();
    }

    private void takePicture() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();

            mImageCaptureUri = Uri.fromFile(mFileTemp);

            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);

        } catch (ActivityNotFoundException e) {

            Log.d("", "cannot take picture", e);
        }

    }

    private void setimagepath() {

        String fileName = "IMG_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date())
                .toString() + ".jpg";
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {

            File sdIconStorageDir = new File(
                    Environment.getExternalStorageDirectory() + "/"
                            + getResources().getString(R.string.app_name) + "/");
            // create storage directories, if they don't exist
            sdIconStorageDir.mkdirs();

            mFileTemp = new File(Environment.getExternalStorageDirectory()
                    + "/" + getResources().getString(R.string.app_name) + "/",
                    fileName);
        } else {
            mFileTemp = new File(getFilesDir(), fileName);
        }
    }

    @Override
    public void onComplete(String response, int taskcode) {
        if (taskcode == TaskCode.GETPROFILEDATACODE && response != null)

        {
            try {
                JSONObject responeseObj = new JSONObject(response);
                if (responeseObj.getInt("StatusCode") == 1) {
                    JSONObject profileObj = responeseObj.getJSONObject("UserProfileData");
                    user_name = profileObj.getString("Name");
                    nameTxt.setText(user_name);

                    if (!profileObj.getString("DateOfBirth").equalsIgnoreCase("") && !profileObj.getString("DateOfBirth").equalsIgnoreCase("null")) {
                        dobTxt.setText("Birth: " + formateDateDeath(profileObj.getString("DateOfBirth")));
                        if (!TextUtils.isEmpty(profileObj.getString("BirthPlace")) && profileObj.getString("BirthPlace") != null && !profileObj.getString("BirthPlace").equalsIgnoreCase("null")) {
                            dobTxt.setText("Birth: " + formateDateDeath(profileObj.getString("DateOfBirth")) + ", " + profileObj.getString("BirthPlace"));
                        }
                    }


                    if (!TextUtils.isEmpty(profileObj.getString("DateOfDeath")) && profileObj.getString("DateOfDeath") != null && !profileObj.getString("DateOfDeath").equalsIgnoreCase("null")) {
                        dodTxt.setText("Death: " + formateDateDeath(profileObj.getString("DateOfDeath")));
                        if (!TextUtils.isEmpty(profileObj.getString("DeathPlace")) && profileObj.getString("DeathPlace") != null && !profileObj.getString("DeathPlace").equalsIgnoreCase("null")) {
                            dodTxt.setText("Death: " + formateDateDeath(profileObj.getString("DateOfDeath")) + ", " + profileObj.getString("DeathPlace"));
                        }
                    }
////                        dodTxt.setText("Age : ");
//                    }
                    if (profileObj.getString("ProfileImage").equalsIgnoreCase("")) {
//                        user_image = Urls.base_Url + treeWrapper.getImageUrl();
                        profileImgCamera.bringToFront();
                    } else {
                        user_image = Urls.profileimagebasicUrl + profileObj.getString("ProfileImage");
                    }

                    if (user_image.contains("thumb")) {
                        profileImgCamera.bringToFront();
                    } else {
                        profileImage.bringToFront();
                    }

                    MyApplication.loader.displayImage(user_image, profileImage, MyApplication.options);
                    List<NameValuePair> valuePairs = new ArrayList<>();
                    valuePairs.add(new BasicNameValuePair("UserId", MyApplication.getUserID()));
                    valuePairs.add(new BasicNameValuePair("NodeId", nodeId));


                    new WebTask(ProfileActivity.this, Urls.getprofilemediaUrl, valuePairs, this, TaskCode.GETPROFILEMEDIA).execute();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else if (taskcode == TaskCode.GETPROFILEMEDIA && response != null) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);


            HashMap<String, ArrayList<GetProfileMediaWrapper>> getprofilemediaHashMap = new ParseData(ProfileActivity.this, response, taskcode).getProfileMediaWrappers();
            if (getprofilemediaHashMap.size() == 0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
                builder.setTitle(getResources().getString(R.string.nomediafiles));
                builder.setMessage(getResources().getString(R.string.areyouwanttocreatememory))
                        .setCancelable(false)
                        .setPositiveButton(getResources().getString(R.string.no),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        dialog.dismiss();
                                    }
                                })
                        .setNegativeButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                startActivityForResult(new Intent(ProfileActivity.this, CreateMemoryActivity.class), 100);

//                                finish();
                            }
                        });
                builder.show();

            }
            ArrayList<String> keyValues = new ArrayList<>();
            for (Map.Entry<String, ArrayList<GetProfileMediaWrapper>> entry : getprofilemediaHashMap.entrySet()) {
                String key = entry.getKey();
                keyValues.add(key);
                // do stuff
            }
            for (int i = 0; i < keyValues.size(); i++) {
                int media_num = Integer.parseInt(keyValues.get(i));
                final ArrayList<GetProfileMediaWrapper> getProfileMediaWrappers = getprofilemediaHashMap.get(String.valueOf(media_num));
                if (Constants.PHOTO == media_num) {
                    View mediainflateView = inflater.inflate(R.layout.profile_media_inflate_layout, null);
//                    mediaLayout.addView(mediainflateView);
                    photosmediaLayout.addView(mediainflateView);

                    TextViewProximaNovaRegular mediaTitleTxt = (TextViewProximaNovaRegular) mediainflateView.findViewById(R.id.mediaTitleTxt);
                    ImageView mediaImgOne = (ImageView) mediainflateView.findViewById(R.id.mediaImgOne);
                    ImageView mediaImgTwo = (ImageView) mediainflateView.findViewById(R.id.mediaImgTwo);
                    ImageView mediaImgThree = (ImageView) mediainflateView.findViewById(R.id.mediaImgThree);
                    final ProgressBar progressBar1 = (ProgressBar) mediainflateView.findViewById(R.id.progressBar1);
                    final ProgressBar progressBar2 = (ProgressBar) mediainflateView.findViewById(R.id.progressBar2);
                    final ProgressBar progressBar3 = (ProgressBar) mediainflateView.findViewById(R.id.progressBar3);
                    TextViewProximaNovaRegular viewsallTxt = (TextViewProximaNovaRegular) mediainflateView.findViewById(R.id.viewsallTxt);
                    viewsallTxt.setTag(i);
                    mediaTitleTxt.setText(getResources().getString(R.string.photos));

                    for (int j = 0; j < getProfileMediaWrappers.size(); j++) {
                        if (j == 0) {
                            not_recordData = false;
                            System.out.println("");
                            MyApplication.loader.displayImage(Urls.profilemedia_basephotoUrl + getProfileMediaWrappers.get(0).getFileName(), mediaImgOne, MyApplication.options, new ImageLoadingListener() {
                                @Override
                                public void onLoadingStarted(String imageUri, View view) {
                                    progressBar1.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                    progressBar1.setVisibility(View.GONE);
                                }

                                @Override
                                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                    progressBar1.setVisibility(View.GONE);
                                }

                                @Override
                                public void onLoadingCancelled(String imageUri, View view) {
                                    progressBar1.setVisibility(View.GONE);
                                }
                            });
                            mediaImgOne.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(new Intent(ProfileActivity.this, PreviewActivity.class).putExtra("title", Constants.PHOTO).putExtra("pos", 0).putExtra("url", getProfileMediaWrappers.get(0).getFileName()).putExtra("activitycode", Constants.PROFILEACTIVITYCODE).putExtra("id", nodeId).putExtra("model", getProfileMediaWrappers));

                                }
                            });

                        } else if (j == 1) {
                            MyApplication.loader.displayImage(Urls.profilemedia_basephotoUrl + getProfileMediaWrappers.get(1).getFileName(), mediaImgTwo, MyApplication.options, new ImageLoadingListener() {
                                @Override
                                public void onLoadingStarted(String imageUri, View view) {
                                    progressBar2.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                    progressBar2.setVisibility(View.GONE);
                                }

                                @Override
                                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                    progressBar2.setVisibility(View.GONE);
                                }

                                @Override
                                public void onLoadingCancelled(String imageUri, View view) {
                                    progressBar2.setVisibility(View.GONE);
                                }
                            });
                            mediaImgTwo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(new Intent(ProfileActivity.this, PreviewActivity.class).putExtra("title", Constants.PHOTO).putExtra("pos", 1).putExtra("url", getProfileMediaWrappers.get(1).getFileName()).putExtra("activitycode", Constants.PROFILEACTIVITYCODE).putExtra("id", nodeId).putExtra("model", getProfileMediaWrappers));

                                }
                            });
                        } else if (j == 2) {
                            MyApplication.loader.displayImage(Urls.profilemedia_basephotoUrl + getProfileMediaWrappers.get(2).getFileName(), mediaImgThree, MyApplication.options, new ImageLoadingListener() {
                                @Override
                                public void onLoadingStarted(String imageUri, View view) {
                                    progressBar3.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                    progressBar3.setVisibility(View.GONE);
                                }

                                @Override
                                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                    progressBar3.setVisibility(View.GONE);
                                }

                                @Override
                                public void onLoadingCancelled(String imageUri, View view) {
                                    progressBar3.setVisibility(View.GONE);
                                }
                            });
                            mediaImgThree.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(new Intent(ProfileActivity.this, PreviewActivity.class).putExtra("title", Constants.PHOTO).putExtra("pos", 2).putExtra("url", getProfileMediaWrappers.get(2).getFileName()).putExtra("activitycode", Constants.PROFILEACTIVITYCODE).putExtra("id", nodeId).putExtra("model", getProfileMediaWrappers));

                                }
                            });
                        }
                    }
                    viewsallTxt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int pos = (int) v.getTag();
                            Intent photoIntent = new Intent(ProfileActivity.this, PhotosVideosActivity.class);
                            photoIntent.putExtra("title", Constants.PHOTO);
                            photoIntent.putExtra("name", user_name);
                            photoIntent.putExtra("iamge", user_image);
                            photoIntent.putExtra("id", nodeId);

                            startActivity(photoIntent);
                        }
                    });

                } else if (Constants.VIDEO == media_num) {
                    View mediainflateView = inflater.inflate(R.layout.profile_media_inflate_layout, null);
                    mediaLayout.addView(mediainflateView);

                    TextViewProximaNovaRegular mediaTitleTxt = (TextViewProximaNovaRegular) mediainflateView.findViewById(R.id.mediaTitleTxt);
                    ImageView mediaImgOne = (ImageView) mediainflateView.findViewById(R.id.mediaImgOne);
                    ImageView mediaImgTwo = (ImageView) mediainflateView.findViewById(R.id.mediaImgTwo);
                    ImageView mediaImgThree = (ImageView) mediainflateView.findViewById(R.id.mediaImgThree);
                    final ProgressBar progressBar1 = (ProgressBar) mediainflateView.findViewById(R.id.progressBar1);
                    final ProgressBar progressBar2 = (ProgressBar) mediainflateView.findViewById(R.id.progressBar2);
                    final ProgressBar progressBar3 = (ProgressBar) mediainflateView.findViewById(R.id.progressBar3);
                    TextViewProximaNovaRegular viewsallTxt = (TextViewProximaNovaRegular) mediainflateView.findViewById(R.id.viewsallTxt);
                    viewsallTxt.setTag(i);
                    mediaTitleTxt.setText(getResources().getString(R.string.videos));
                    for (int j = 0; j < getProfileMediaWrappers.size(); j++) {
                        if (j == 0) {
                            not_recordData = false;
                            MyApplication.loader.displayImage(Urls.profilemedia_basevideoThumbsUrl + getProfileMediaWrappers.get(0).getThumbPath(), mediaImgOne, MyApplication.options, new ImageLoadingListener() {
                                @Override
                                public void onLoadingStarted(String imageUri, View view) {
                                    progressBar1.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                    progressBar1.setVisibility(View.GONE);
                                }

                                @Override
                                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                    progressBar1.setVisibility(View.GONE);
                                }

                                @Override
                                public void onLoadingCancelled(String imageUri, View view) {
                                    progressBar1.setVisibility(View.GONE);
                                }
                            });
                            mediaImgOne.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    startActivity(new Intent(ProfileActivity.this, PreviewActivity.class).putExtra("title", Constants.VIDEO).putExtra("pos", 0).putExtra("url", getProfileMediaWrappers.get(0).getFileName()).putExtra("activitycode", Constants.PROFILEACTIVITYCODE).putExtra("id", nodeId).putExtra("model", getProfileMediaWrappers));

                                }
                            });
                        } else if (j == 1) {
                            MyApplication.loader.displayImage(Urls.profilemedia_basevideoThumbsUrl + getProfileMediaWrappers.get(1).getThumbPath(), mediaImgTwo, MyApplication.options, new ImageLoadingListener() {
                                @Override
                                public void onLoadingStarted(String imageUri, View view) {
                                    progressBar2.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                    progressBar2.setVisibility(View.GONE);
                                }

                                @Override
                                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                    progressBar2.setVisibility(View.GONE);
                                }

                                @Override
                                public void onLoadingCancelled(String imageUri, View view) {
                                    progressBar2.setVisibility(View.GONE);
                                }
                            });
                            mediaImgTwo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(new Intent(ProfileActivity.this, PreviewActivity.class).putExtra("title", Constants.VIDEO).putExtra("pos", 1).putExtra("url", getProfileMediaWrappers.get(1).getFileName()).putExtra("activitycode", Constants.PROFILEACTIVITYCODE).putExtra("id", nodeId).putExtra("model", getProfileMediaWrappers));

                                }
                            });
                        } else if (j == 2) {
                            MyApplication.loader.displayImage(Urls.profilemedia_basevideoThumbsUrl + getProfileMediaWrappers.get(2).getThumbPath(), mediaImgThree, MyApplication.options, new ImageLoadingListener() {
                                @Override
                                public void onLoadingStarted(String imageUri, View view) {
                                    progressBar3.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                    progressBar3.setVisibility(View.GONE);
                                }

                                @Override
                                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                    progressBar3.setVisibility(View.GONE);
                                }

                                @Override
                                public void onLoadingCancelled(String imageUri, View view) {
                                    progressBar3.setVisibility(View.GONE);
                                }
                            });
                            mediaImgThree.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(new Intent(ProfileActivity.this, PreviewActivity.class).putExtra("title", Constants.VIDEO).putExtra("pos", 2).putExtra("url", getProfileMediaWrappers.get(2).getFileName()).putExtra("activitycode", Constants.PROFILEACTIVITYCODE).putExtra("id", nodeId).putExtra("model", getProfileMediaWrappers));

                                }
                            });
                        }
                    }
                    viewsallTxt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int pos = (int) v.getTag();
                            Intent photoIntent = new Intent(ProfileActivity.this, PhotosVideosActivity.class);
                            photoIntent.putExtra("title", Constants.VIDEO);
                            photoIntent.putExtra("name", user_name);
                            photoIntent.putExtra("iamge", user_image);
                            photoIntent.putExtra("id", nodeId);

                            startActivity(photoIntent);
                        }
                    });

                } else if (Constants.AUDIO == media_num) {
                    View mediainflateView = inflater.inflate(R.layout.profile_media_inflate_layout, null);
//                    mediaLayout.addView(mediainflateView);
                    audiomediaLayout.addView(mediainflateView);

                    TextViewProximaNovaRegular mediaTitleTxt = (TextViewProximaNovaRegular) mediainflateView.findViewById(R.id.mediaTitleTxt);
                    ImageView mediaImgOne = (ImageView) mediainflateView.findViewById(R.id.mediaImgOne);
                    mediaImgOne.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    ImageView mediaImgTwo = (ImageView) mediainflateView.findViewById(R.id.mediaImgTwo);
                    mediaImgTwo.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    ImageView mediaImgThree = (ImageView) mediainflateView.findViewById(R.id.mediaImgThree);
                    mediaImgThree.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    TextViewProximaNovaRegular viewsallTxt = (TextViewProximaNovaRegular) mediainflateView.findViewById(R.id.viewsallTxt);
                    viewsallTxt.setTag(i);
                    mediaTitleTxt.setText("Audio");
                    for (int j = 0; j < getProfileMediaWrappers.size(); j++) {
                        if (j == 0) {
                            not_recordData = false;
//                            mediaImgOne.setBackgroundResource(R.mipmap.thumb_audio);
                            int image = R.mipmap.thumb_audio;
                            MyApplication.loader.displayImage("drawable://" + image, mediaImgOne, MyApplication.options);
                            mediaImgOne.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    buttomtoUp(audioLayout);
                                    mediaPlayer(getProfileMediaWrappers.get(0).getFileName());

                                }
                            });

                        } else if (j == 1) {
//                            mediaImgTwo.setBackgroundResource(R.mipmap.thumb_audio);
                            int image = R.mipmap.thumb_audio;
                            MyApplication.loader.displayImage("drawable://" + image, mediaImgTwo, MyApplication.options);
                            mediaImgTwo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    buttomtoUp(audioLayout);
                                    mediaPlayer(getProfileMediaWrappers.get(1).getFileName());

                                }
                            });
                        } else if (j == 2) {
//                            mediaImgThree.setBackgroundResource(R.mipmap.thumb_audio);
                            int image = R.mipmap.thumb_audio;
                            MyApplication.loader.displayImage("drawable://" + image, mediaImgThree, MyApplication.options);
                            mediaImgThree.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    buttomtoUp(audioLayout);
                                    mediaPlayer(getProfileMediaWrappers.get(2).getFileName());

                                }
                            });
                        }
                    }
                    viewsallTxt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int pos = (int) v.getTag();
                            Intent photoIntent = new Intent(ProfileActivity.this, PhotosVideosActivity.class);
                            photoIntent.putExtra("title", Constants.AUDIO);
                            photoIntent.putExtra("name", user_name);
                            photoIntent.putExtra("iamge", user_image);
                            photoIntent.putExtra("id", nodeId);

                            startActivity(photoIntent);
                        }
                    });

                }
//                if (not_recordData)
//                {
//                    Toast.makeText(ProfileActivity.this,"Record not found",Toast.LENGTH_SHORT).show();
//                }
//                else if (Constants.DOC == media_num) {
//                    mediaTitleTxt.setText("Docs");
////                    MyApplication.loader.displayImage(Urls.profilemedia_basedocThumbsUrl + getProfileMediaWrappers.get(0).getThumbPath(), mediaImgOne, MyApplication.option3);
//                    mediaImgOne.setBackgroundResource(R.mipmap.thumb_doc);
//                    mediaImgTwo.setBackgroundResource(R.mipmap.thumb_doc);
//                    mediaImgThree.setBackgroundResource(R.mipmap.thumb_doc);
//                }


            }


        }
    }

    private String formateDate(String dateString) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat("dd MMMM yyyy");
            Date date = originalFormat.parse(dateString);
            String formattedDate = targetFormat.format(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String formateDateDeath(String dateString) {

        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat("dd-MMMM-yyyy");
            Date date = originalFormat.parse(dateString);
            String formattedDate = targetFormat.format(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void mediaPlayer(String str) {


        myHandler = new Handler();

        String fileUrl = Urls.profilemedia_baseaudioUrl + str;
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        try {
            mediaPlayer.setDataSource(fileUrl);
            mediaPlayer.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mediaPlayer.start();
        playImg.setBackgroundResource(R.mipmap.rec_pause);

        progressBar1.setMax(mediaPlayer.getDuration());
        seekUpdation();
//        mediaPlayer.set


    }

    Runnable run = new Runnable() {

        @Override
        public void run() {
            seekUpdation();
        }
    };

    public void seekUpdation() {

        progressBar1.setProgress(mediaPlayer.getCurrentPosition());
        myHandler.postDelayed(run, 1000);
    }


    public void buttomtoUp(View view) {
        Animation bottomUp = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.bottom_up);

        view.startAnimation(bottomUp);
        view.setVisibility(View.VISIBLE);
    }

    public void uptoButtom(View view) {
        Animation bottomUp = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.bottom_down);

        view.startAnimation(bottomUp);
        view.setVisibility(View.GONE);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            languageCode(MyApplication.getLanguagecode());
        } else {
            languageCode(MyApplication.getLanguagecode());
        }
    }

    public void languageCode(String code) {
        Locale locale;
        Configuration lconfig;
        locale = new Locale(code);
        Locale.setDefault(locale);
        lconfig = new Configuration();
        lconfig.locale = locale;
        getBaseContext().getResources().updateConfiguration(lconfig,
                getBaseContext().getResources().getDisplayMetrics());
    }

    private void uploadFile() {
        String memberids = "";

        HashMap<String, String> params = new HashMap<>();
//            params.put("AlbumId", albumidsSpinner.getSelectedItem().toString());

        params.put("NodeId", nodeId);
        params.put("LoginId", MyApplication.getUserID());

        final ProgressDialog dialog = ProgressDialog.show(ProfileActivity.this, "", "Please wait...");
        MultipartRequest multipartRequest = new MultipartRequest(Urls.uploadProfileImage, new Response.Listener<String>() {


            @Override
            public void onResponse(String response) {
                dialog.dismiss();
                if (response != null) {
                    try {
                        JSONObject responseObj = new JSONObject(response);
                        if (responseObj.getInt("StatusCode") == 1) {
                            AppGlobalData.reloadwebViewBoolean = true;
                            AppGlobalData.reloadfamilydataBoolean = true;
                            AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
                            builder.setTitle("" + getResources().getString(R.string.app_name));
                            builder.setMessage(getResources().getString(R.string.Successfullyuploaded))
                                    .setCancelable(false)
                                    .setPositiveButton("Ok",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                    finish();
                                                }
                                            });

                            builder.show();
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
                            builder.setTitle("" + getResources().getString(R.string.app_name));
                            builder.setMessage(responseObj.getString("Message"))
                                    .setCancelable(false)
                                    .setPositiveButton("Ok",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                    finish();
                                                }
                                            });

                            builder.show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                System.out.println("response" + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();

                AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
                builder.setTitle("" + getResources().getString(R.string.app_name));
                builder.setMessage(getString(R.string.Successfullyuploaded))
                        .setCancelable(false)
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        finish();


                                    }
                                });

                builder.show();
                System.out.println("response" + error);
            }
        }, mFileTemp, params, "Image");
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(60 * 1000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(ProfileActivity.this).add(multipartRequest);
    }


}
