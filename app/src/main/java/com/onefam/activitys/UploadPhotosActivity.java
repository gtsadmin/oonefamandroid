package com.onefam.activitys;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.czt.mp3recorder.MP3Recorder;
import com.github.lassana.recorder.AudioRecorder;
import com.onefam.R;
import com.onefam.applications.MyApplication;
import com.onefam.datacontroller.AppGlobalData;
import com.onefam.datacontroller.Constants;
import com.onefam.views.EditTextProximaNovaRegular;
import com.onefam.views.MultiSelectSpinner;
import com.onefam.views.RadioButtonProximaNovaRegular;
import com.onefam.views.TextViewProximaNovaRegular;
import com.onefam.webutility.AndroidMultiPartEntity;
import com.onefam.webutility.MultipartRequest;
import com.onefam.webutility.ParseData;
import com.onefam.webutility.TaskCode;
import com.onefam.webutility.Urls;
import com.onefam.webutility.WebTask;
import com.onefam.webutility.WebTaskComplete;
import com.onefam.wrapper.GetEventWrapper;
import com.onefam.wrapper.TreeWrapper;
import com.shuyu.waveview.FileUtils;

import org.apache.http.NameValuePair;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import eu.janmuller.android.simplecropimage.CropImage;

import static com.onefam.R.id.emailId;
import static com.onefam.R.id.passwordEdt;
import static java.lang.Boolean.FALSE;


public class UploadPhotosActivity extends AppCompatActivity implements WebTaskComplete {
    @Bind(R.id.backLayout)
    LinearLayout backLayout;
    @Bind(R.id.topbarLayout)
    RelativeLayout topbarLayout;
    @Bind(R.id.opencameraLayout)
    LinearLayout opencameraLayout;
    @Bind(R.id.uploadfromgalleryLayout)
    LinearLayout uploadfromgalleryLayout;

    @Bind(R.id.tagfamilyeventSpinner)
    Spinner tagfamilyeventSpinner;

    @Bind(R.id.uploadLayout)
    LinearLayout uploadLayout;

    @Bind(R.id.opencameraTxt)
    TextViewProximaNovaRegular opencameraTxt;
    @Bind(R.id.cameraImg)
    ImageView cameraImg;
    @Bind(R.id.takeuploadaduioLayout)
    LinearLayout takeuploadaduioLayout;
    @Bind(R.id.uploadTxt)
    TextViewProximaNovaRegular uploadTxt;
    @Bind(R.id.selectfamilymamberTxt)
    TextViewProximaNovaRegular selectfamilymamberTxt;
    @Bind(R.id.nameEdt)
    EditTextProximaNovaRegular nameEdt;
    @Bind(R.id.albumidsSpinner)
    Spinner albumidsSpinner;
    @Bind(R.id.linearLayout1)
    LinearLayout linearLayout1;
    @Bind(R.id.publicRadioBtn)
    RadioButtonProximaNovaRegular publicRadioBtn;
    @Bind(R.id.privateBtn)
    RadioButtonProximaNovaRegular privateBtn;
    @Bind(R.id.galleryImg)
    ImageView galleryImg;
    @Bind(R.id.cameragalleryLayout)
    LinearLayout cameragalleryLayout;
    @Bind(R.id.closeLayout)
    LinearLayout closeLayout;

    @Bind(R.id.tiltle)
    RelativeLayout tiltle;
    @Bind(R.id.playImg)
    ImageView playImg;
    @Bind(R.id.stopImg)
    ImageView stopImg;
    @Bind(R.id.timeTxt)
    TextViewProximaNovaRegular timeTxt;
    @Bind(R.id.recordingnameTxt)
    TextViewProximaNovaRegular recordingnameTxt;
    @Bind(R.id.audioLayout)
    RelativeLayout audioLayout;
    @Bind(R.id.selectfamilymamberSpinner)
    MultiSelectSpinner selectfamilymamberSpinner;
    @Bind(R.id.doneTxt)
    ImageView doneTxt;
    @Bind(R.id.recordstopImg)
    ImageView recordstopImg;
    @Bind(R.id.topTitleTxt)
    TextViewProximaNovaRegular topTitleTxt;
    private File mFileTemp, mFileTemp1;
    private String imagePath = "", images_uptedates = "";
    Uri fileUri;
    int selectImgcode;
    Handler handler;
    Boolean isRecording = false;
    AudioRecorder recorder;
    boolean playBln = true;
    //    MediaRecorder mRecorder;
    MP3Recorder mRecorder;
    String fileName, apiUrl = "";
    MediaPlayer mPlayer = null;
    int recordTime, playTime;
    private PopupWindow pw;
    private boolean expanded;
    public static boolean[] checkSelected;
    String eventID = "";
    int pulic_private = 1;
    private ArrayList<String> membersIDs;
    private ArrayList<String> namesArrayToShowAlreadySelected = new ArrayList<>();
    private ArrayList<String> namesArray = new ArrayList<>();

    HashMap<String, File> fileHashMap = new HashMap<>();
    int selectImg = 0;
    boolean audioBln = true;
    boolean recordingBln = true;
    long totalSize = 0;
    int CAMERACODE_GALLERYCODE;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upload_photo_layout);
        ButterKnife.bind(this);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setIndeterminate(true);
        pDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getString(R.string.uploading), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                fileHashMap.clear();
                dialog.dismiss();
                finish();
            }
        });
        publicRadioBtn.setPadding((int) getResources().getDimension(R.dimen.dim_15),
                publicRadioBtn.getPaddingTop(),
                publicRadioBtn.getPaddingRight(),
                publicRadioBtn.getPaddingBottom());
        privateBtn.setPadding((int) getResources().getDimension(R.dimen.dim_20),
                privateBtn.getPaddingTop(),
                privateBtn.getPaddingRight(),
                privateBtn.getPaddingBottom());
        publicRadioBtn.setChecked(true);
        selectImgcode = getIntent().getIntExtra("num", 0);
        if (selectImgcode == Constants.UPLOADPHOTO) {
            apiUrl = Urls.uploadImageAPI;
            cameraImg.setBackgroundResource(R.mipmap.icn_upload_photo);
            uploadTxt.setText(getResources().getString(R.string.upload_photo));
        } else if (selectImgcode == Constants.UPLOADVIDEOS) {
            cameraImg.setBackgroundResource(R.mipmap.icn_upload_video);
            uploadTxt.setText(getResources().getString(R.string.upload_video));
            apiUrl = Urls.uploadVideoAPI;
        } else if (selectImgcode == Constants.UPLOADAUDIO) {
            cameraImg.setBackgroundResource(R.mipmap.icn_upload_audio);
            uploadTxt.setText(getResources().getString(R.string.upload_audio));
            apiUrl = Urls.uploadAudioAPI;
        }
        if (selectImgcode == Constants.UPLOADAUDIO) {
            opencameraTxt.setText(getResources().getString(R.string.tap_to_record));
        } else {
            opencameraTxt.setText(getResources().getString(R.string.open_camera));
        }
        if (AppGlobalData.getInstance().getMemberList().size() == 0 || AppGlobalData.reloadfamilydataBoolean == true) {
            if (MyApplication.isInternetWorking(UploadPhotosActivity.this)) {
                List<NameValuePair> valuePairs = new ArrayList<>();
                valuePairs.add(new BasicNameValuePair("NodeId", MyApplication.getUserID()));
                new WebTask(UploadPhotosActivity.this, Urls.getFamalyViewTreeUrl, valuePairs, this, TaskCode.FAMILYVIEWTREECODE).execute();
            }
        } else {
            initialize();
            if (MyApplication.isInternetWorking(UploadPhotosActivity.this)) {
                if (AppGlobalData.getInstance().getEventList().size() == 0 || AppGlobalData.getEventBoolean == true) {
                    List<NameValuePair> valuePairs = new ArrayList<>();
                    valuePairs.add(new BasicNameValuePair("UserId", MyApplication.getUserID()));
                    new WebTask(UploadPhotosActivity.this, Urls.getEventListAPI, valuePairs, this, TaskCode.GETEVENTCODE).execute();
                } else {
                    setEventSpinner();
                }
            }
        }
//        if (MyApplication.isInternetWorking(UploadPhotosActivity.this)) {
//            if (AppGlobalData.getInstance().getAlbumList().size()==0) {
//
//                List<NameValuePair> valuePairs = new ArrayList<>();
//                valuePairs.add(new BasicNameValuePair("UserId", MyApplication.getUserID()));
//                new WebTask(UploadPhotosActivity.this, Urls.getAlbumIDApi+MyApplication.getUserID(), null, this, TaskCode.GETALBUMAPI).execute();
//            }else
//            {
//                setalbumSpinner();
//            }
//        }

        if (MyApplication.isInternetWorking(UploadPhotosActivity.this)) {
//            getTagMemberList();
        }

        /*unselcted already selected item from global array*/
        for (int i = 0; i < AppGlobalData.getInstance().getTagMemberArrayList().size(); i++) {
            AppGlobalData.getInstance().getTagMemberArrayList().get(i).setIsSelected(false);
        }

        publicRadioBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    pulic_private = 1;
                }
            }
        });
        privateBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    pulic_private = 0;
                }
            }
        });
    }

    @OnClick(R.id.backLayout)
    public void backLayout() {
        finish();
    }

    @OnClick(R.id.audioLayout)
    public void audioLayout() {
    }

    @OnClick(R.id.uploadfromgalleryLayout)
    public void uploadfromgalleryLayout() {
        MyApplication.getInstance().produceAnimation(uploadfromgalleryLayout);
        if (selectImgcode == Constants.UPLOADPHOTO) {
            setimagepath();
            openGallery();
            selectImg = 1;
        } else if (selectImgcode == Constants.UPLOADVIDEOS) {
            openGalleryVideo();
        } else if (selectImgcode == Constants.UPLOADAUDIO) {
            openaudioGallery();
//            stopRecording();
        }
    }

    @OnClick(R.id.opencameraLayout)
    public void opencameraLayout() {
//        buttomtoUp(uploadLayout);
        MyApplication.getInstance().produceAnimation(opencameraLayout);
        if (selectImgcode == Constants.UPLOADPHOTO) {
            setimagepath();
            takePicture();
            selectImg = 2;
        } else if (selectImgcode == Constants.UPLOADVIDEOS) {
            recordVideo();
        } else if (selectImgcode == Constants.UPLOADAUDIO) {
            audioRecordingView();
        }
    }

    @OnClick(R.id.uploadLayout)
    public void uploadLayout() {
        MyApplication.myApplication.produceAnimation(uploadLayout);
        if (mFileTemp == null) {
            Toast.makeText(UploadPhotosActivity.this, getResources().getString(R.string.Mediaisrequired), Toast.LENGTH_SHORT).show();
        } else {
//             uploadFile();
            new UploadFileToServer().execute();
        }
    }

    private void uploadFile() {
        String memberids = "";
        if (membersIDs != null) {
            if (membersIDs.size() != 0) {
                memberids = TextUtils.join(",", membersIDs);
                System.out.println("memberids" + memberids);
            }
        }
        HashMap<String, String> params = new HashMap<>();
//            params.put("AlbumId", albumidsSpinner.getSelectedItem().toString());
        params.put("AlbumName", nameEdt.getText().toString());
        params.put("IsPublic", "" + pulic_private);
        params.put("EventIDs", eventID);
        params.put("UserId", MyApplication.getUserID());
        params.put("LoginId", MyApplication.getUserID());
        params.put("TagFamilyMembers", memberids);
        final ProgressDialog dialog = ProgressDialog.show(UploadPhotosActivity.this, "", "Please wait...");
        MultipartRequest multipartRequest = new MultipartRequest(apiUrl, new Response.Listener<String>() {


            @Override
            public void onResponse(String response) {
                dialog.dismiss();
                if (response != null) {
                    try {
                        JSONObject responseObj = new JSONObject(response);
                        if (responseObj.getInt("StatusCode") == 1) {
                            fileHashMap.clear();
                            AlertDialog.Builder builder = new AlertDialog.Builder(UploadPhotosActivity.this);
                            builder.setTitle("" + getResources().getString(R.string.app_name));
                            builder.setMessage(getString(R.string.Successfullyuploaded))
                                    .setCancelable(false)
                                    .setPositiveButton("Ok",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                    finish();
                                                }
                                            });

                            builder.show();
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(UploadPhotosActivity.this);
                            builder.setTitle("" + getResources().getString(R.string.app_name));
                            builder.setMessage(responseObj.getString("Message"))
                                    .setCancelable(false)
                                    .setPositiveButton("Ok",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                    finish();
                                                }
                                            });

                            builder.show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                System.out.println("response" + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                fileHashMap.clear();
                AlertDialog.Builder builder = new AlertDialog.Builder(UploadPhotosActivity.this);
                builder.setTitle("" + getResources().getString(R.string.app_name));
                builder.setMessage(getString(R.string.Successfullyuploaded))
                        .setCancelable(false)
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        finish();


                                    }
                                });

                builder.show();
                System.out.println("response" + error);
            }
        }, fileHashMap, params, "");
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(60 * 1000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(UploadPhotosActivity.this).add(multipartRequest);
    }

    private void takePicture() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();

            mImageCaptureUri = Uri.fromFile(mFileTemp);

            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, Constants.REQUEST_CODE_TAKE_PICTURE);

        } catch (ActivityNotFoundException e) {
            Log.d("", "cannot take picture", e);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_CODE_GALLERY && resultCode == RESULT_OK) {

            try {

                System.out.println("on activity result gallery");
                InputStream inputStream = getContentResolver()
                        .openInputStream(data.getData());
                FileOutputStream fileOutputStream = new FileOutputStream(
                        mFileTemp);
                copyStream(inputStream, fileOutputStream);
                fileOutputStream.close();
                inputStream.close();

//                startCropImage();
                CAMERACODE_GALLERYCODE = 1;
                imagePath = mFileTemp.getPath();

                Toast.makeText(UploadPhotosActivity.this, "Image Selected", Toast.LENGTH_SHORT).show();
                if (selectImg == 1) {
                    MyApplication.loader.displayImage("file://"
                            + imagePath, galleryImg, MyApplication.option2);

                    fileHashMap.put("file", mFileTemp);
                } else {
                    MyApplication.loader.displayImage("file://"
                            + imagePath, cameraImg, MyApplication.option2);
                    fileHashMap.put("file1", mFileTemp);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == Constants.REQUEST_CODE_TAKE_PICTURE && resultCode == RESULT_OK) {

            try {
                imagePath = mFileTemp.getPath();
                CAMERACODE_GALLERYCODE = 2;
                Toast.makeText(UploadPhotosActivity.this, "Image Selected", Toast.LENGTH_SHORT).show();
                if (selectImg == 1) {
                    MyApplication.loader.displayImage("file://"
                            + imagePath, galleryImg, MyApplication.option2);
                    fileHashMap.put("file", mFileTemp);
                } else {
                    MyApplication.loader.displayImage("file://"
                            + imagePath, cameraImg, MyApplication.option2);
                    fileHashMap.put("file1", mFileTemp);

                }
            } catch (Exception e) {
                Log.e("photoCapture ", e.toString());
                Toast.makeText(UploadPhotosActivity.this, "Issue in getting image", Toast.LENGTH_SHORT).show();
            }

        } else if (requestCode == Constants.REQUEST_CODE_CROP_IMAGE && resultCode == RESULT_OK) {

            System.out.println("on activity result crop");
            String path = data.getStringExtra(CropImage.IMAGE_PATH);
            if (path == null) {

                return;
            }


            imagePath = mFileTemp.getPath();

            Toast.makeText(UploadPhotosActivity.this, "Image Selected", Toast.LENGTH_SHORT).show();
            if (selectImg == 1) {
                MyApplication.loader.displayImage("file://"
                        + imagePath, galleryImg, MyApplication.option2);
                fileHashMap.put("file", mFileTemp);
            } else {
                MyApplication.loader.displayImage("file://"
                        + imagePath, cameraImg, MyApplication.option2);
                fileHashMap.put("file1", mFileTemp);

            }


        } else if (requestCode == Constants.REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK) {
            try {
                String selectedImageOrFileId = fileUri.getPath();

                CAMERACODE_GALLERYCODE = 1;
                if (selectedImageOrFileId != null) {
                    mFileTemp = new File(selectedImageOrFileId);
                    fileHashMap.put("file2", mFileTemp);
                    Toast.makeText(UploadPhotosActivity.this, "Video Captured", Toast.LENGTH_SHORT).show();
                    Bitmap bmThumbnail;

                    // MICRO_KIND: 96 x 96 thumbnail

//                    bmThumbnail = ThumbnailUtils.createVideoThumbnail(selectedImageOrFileId,
//                            MediaStore.Video.Thumbnails.MICRO_KIND);

                    //MINI_KING : 512 x 384.thumbnail

                    bmThumbnail = ThumbnailUtils.createVideoThumbnail(selectedImageOrFileId,
                            MediaStore.Video.Thumbnails.MICRO_KIND);
                    cameraImg.setImageBitmap(bmThumbnail);
                }


            } catch (Exception e) {
                e.printStackTrace();
            }


        } else if (requestCode == Constants.REQUEST_VIDEO_GALLERY && resultCode == RESULT_OK) {
            Uri selectedVieo = data.getData();
            String[] filePathColumn = {MediaStore.Video.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedVieo,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String filePath = cursor.getString(columnIndex);
            CAMERACODE_GALLERYCODE = 2;
            cursor.close();
            if (filePath != null) {

                mFileTemp = new File(filePath);
                fileHashMap.put("file1", mFileTemp);
                Toast.makeText(UploadPhotosActivity.this, "Video Selected", Toast.LENGTH_SHORT).show();
                Bitmap bmThumbnail;

                // MICRO_KIND: 96 x 96 thumbnail
                bmThumbnail = ThumbnailUtils.createVideoThumbnail(filePath,
                        MediaStore.Video.Thumbnails.MICRO_KIND);
                galleryImg.setImageBitmap(bmThumbnail);
            }

        } else if (requestCode == Constants.REQUEST_AUDIO_GALLERY && resultCode == RESULT_OK) {
            Uri selectedAudio = data.getData();
            String[] filePathColumn = {MediaStore.Audio.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedAudio,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String filePath = cursor.getString(columnIndex);

            cursor.close();
            if (filePath != null) {
                mFileTemp = new File(filePath);
                Toast.makeText(UploadPhotosActivity.this, "Audio" +
                        " Selected", Toast.LENGTH_SHORT).show();
                fileHashMap.put("file", mFileTemp);
            }
        }
    }

    private void openGallery() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, Constants.REQUEST_CODE_GALLERY);
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    public Uri getOutputMediaFileUri() {

        return Uri.fromFile(mFileTemp);
    }

    private void setimagepath() {

        String fileName = "IMG_"
                + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date())
                .toString() + ".jpg";
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {

            File sdIconStorageDir = new File(
                    Environment.getExternalStorageDirectory() + "/"
                            + getResources().getString(R.string.app_name) + "/");
            // create storage directories, if they don't exist
            sdIconStorageDir.mkdirs();

            mFileTemp = new File(Environment.getExternalStorageDirectory()
                    + "/" + getResources().getString(R.string.app_name) + "/",
                    fileName);
        } else {
            mFileTemp = new File(getFilesDir(), fileName);
        }
    }

    private void startCropImage() {
        try {
            System.out.println("on activity result startcrop functions");
            Intent intent = new Intent(UploadPhotosActivity.this, CropImage.class);
            intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
            intent.putExtra(CropImage.SCALE, true);
            intent.putExtra(CropImage.ASPECT_X, 1);
            intent.putExtra(CropImage.ASPECT_Y, 1);
            startActivityForResult(intent, Constants.REQUEST_CODE_CROP_IMAGE);
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    private void openGalleryVideo() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("video/*");
        startActivityForResult(photoPickerIntent,
                Constants.REQUEST_VIDEO_GALLERY);
    }

    private void recordVideo() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        fileUri = getOutputMediaFileUri(Constants.MEDIA_TYPE_VIDEO);
        // set video quality
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
//        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 30);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, Constants.REQUEST_VIDEO_CAPTURE);

    }

    private File getOutputMediaFile(int type) {
        // Create a media file name

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == Constants.MEDIA_TYPE_VIDEO) {
            mediaFile = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "VID_" + timeStamp + ".mp4");
            // media path:
            // /storage/emulated/0/Pictures/Gossip/VID_20141002_100957.mp4
        } else {
            return null;
        }
        long filelength1 = mediaFile.length();

        System.out.println("filelength mediaFile :" + filelength1 + " path: " + mediaFile.getAbsolutePath());

        return mediaFile;
    }

    private Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    public void openaudioGallery() {
        Intent intent = new Intent();
        intent.setType("audio/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Constants.REQUEST_AUDIO_GALLERY);
    }


    @Override
    public void onComplete(String response, int taskcode) {
        if (response != null && taskcode == TaskCode.FAMILYVIEWTREECODE) {
            ArrayList<TreeWrapper> memberInfoWrappers = new ParseData(UploadPhotosActivity.this, response, taskcode).getTreeMember();
            initialize();
            if (MyApplication.isInternetWorking(UploadPhotosActivity.this)) {
                if (AppGlobalData.getInstance().getEventList().size() == 0 || AppGlobalData.getEventBoolean == true) {
                    List<NameValuePair> valuePairs = new ArrayList<>();
                    valuePairs.add(new BasicNameValuePair("UserId", MyApplication.getUserID()));
                    new WebTask(UploadPhotosActivity.this, Urls.getEventListAPI, valuePairs, this, TaskCode.GETEVENTCODE).execute();
                }
            }
        } else if (response != null && taskcode == TaskCode.GETEVENTCODE) {
            ArrayList<GetEventWrapper> getEventWrapperArrayList = new ParseData(UploadPhotosActivity.this, response, TaskCode.GETEVENTCODE).getEventList();
            setEventSpinner();
        } else if (response != null && taskcode == TaskCode.GETALBUMAPI) {
            try {
                JSONObject responseObj = new JSONObject(response);
                if (responseObj.getInt("StatusCode") == 1) {
                    ArrayList<String> albumList = new ArrayList<>();
                    HashMap<String, String> albumlistMap = new HashMap<>();
                    if (responseObj.has("Albums")) {
                        JSONArray subObj = responseObj.getJSONArray("Albums");
                        for (int i = 0; i < subObj.length(); i++) {
                            JSONObject object = subObj.getJSONObject(i);
                            albumList.add(object.getString("AlbumName"));
                            albumlistMap.put(object.getString("AlbumName"), object.getString("AlbumId"));
                        }
                        AppGlobalData.getInstance().setAlbumList(albumList);
                        AppGlobalData.getInstance().setAlbumlistMap(albumlistMap);

                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (response != null && taskcode == TaskCode.TAGMEMBER) {
            Log.v("tagMemberList ", response);
            parseTagMemberData(response);
        }


    }

    private void setEventSpinner() {
        ArrayAdapter<String> member_ship_arr = new ArrayAdapter<String>(this, R.layout.spinner_layout, AppGlobalData.getInstance().getEventList()) {
            public View getView(int position, View convertView,
                                ViewGroup parent) {
                View v = super.getView(position, convertView, parent);


                if (position == 0) {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.gray_color));
                    ((TextView) v).setTextSize(getResources().getDimension(R.dimen.dim_6));
                } else {

                    ((TextView) v).setTextColor(getResources()
                            .getColorStateList(R.color.white));
                }
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView,
                        parent);


                ((TextView) v).setTextColor(Color.BLACK);


                return v;
            }
        };

        tagfamilyeventSpinner.setAdapter(member_ship_arr);
        tagfamilyeventSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!tagfamilyeventSpinner.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.TagFamilyEvent))) {
                    eventID = AppGlobalData.getInstance().getEventlistMap().get(tagfamilyeventSpinner.getSelectedItem().toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private void initialize() {
        //data source for drop-down list


        checkSelected = new boolean[AppGlobalData.appGlobalData.getInstance().getMemberList().size()];
        //initialize all values of list to 'unselected' initially
        for (int i = 0; i < checkSelected.length; i++) {
            checkSelected[i] = false;
        }


        selectfamilymamberTxt.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                membersIDs = new ArrayList<>();
                selectfamilymamberTxt.setHint(getResources().getString(R.string.tag_family_mamber));
                initiatePopUp(selectfamilymamberTxt);
            }
        });
    }

    /*
     * Function to set up the pop-up window which acts as drop-down list
     * */
    private void initiatePopUp(TextView tv) {
        LayoutInflater inflater = (LayoutInflater) UploadPhotosActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //get the pop-up window i.e.  drop-down layout
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.pop_up_window, null);

        //get the view to which drop-down layout is to be anchored
        LinearLayout layout1 = (LinearLayout) findViewById(R.id.linearLayout1);
        pw = new PopupWindow(layout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, true);

        //Pop-up window background cannot be null if we want the pop-up to listen touch events outside its window
        pw.setBackgroundDrawable(new BitmapDrawable());
        pw.setTouchable(true);

        //let pop-up be informed about touch events outside its window. This  should be done before setting the content of pop-up
        pw.setOutsideTouchable(true);
        pw.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);

        //dismiss the pop-up i.e. drop-down when touched anywhere outside the pop-up
        pw.setTouchInterceptor(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    pw.dismiss();
                    return true;
                }
                return false;
            }
        });

        //provide the source layout for drop-down
        pw.setContentView(layout);

        //anchor the drop-down to bottom-left corner of 'layout1'
        pw.showAsDropDown(layout1);

        //populate the drop-down list
        final ListView list = (ListView) layout.findViewById(R.id.dropDownList);

//        for (int i = 0; i < AppGlobalData.getInstance().getTreeWrapperArrayList().size(); i++) {
//            AppGlobalData.getInstance().getTreeWrapperArrayList().get(i).setIsSelected(false);
//        }
//        EventDropListAdapter adapter = new EventDropListAdapter(this, AppGlobalData.getInstance().getTreeWrapperArrayList(), tv);

        for (int i = 0; i < AppGlobalData.getInstance().getTagMemberArrayList().size(); i++) {
            if (namesArrayToShowAlreadySelected.size() != 0) {
                for (int j = 0; j < namesArrayToShowAlreadySelected.size(); j++) {
//                    Log.v("Selected_Name ",namesArrayToShowAlreadySelected.get(j));
                    if (namesArrayToShowAlreadySelected.get(j).contains(AppGlobalData.getInstance().getTagMemberArrayList().get(i).getNodeId())) {
                        AppGlobalData.getInstance().getTagMemberArrayList().get(i).setIsSelected(true);
                        Log.v("Selected_Name ", namesArrayToShowAlreadySelected.get(j) + " selected ");
                        break;

                    } else {
                        AppGlobalData.getInstance().getTagMemberArrayList().get(i).setIsSelected(false);
                        Log.v("Selected_Name ", namesArrayToShowAlreadySelected.get(j) + " not selected ");
                    }
                }
            } else {
                AppGlobalData.getInstance().getTagMemberArrayList().get(i).setIsSelected(false);
            }


//            AppGlobalData.getInstance().getTagMemberArrayList().get(i).setIsSelected(false);
        }
        EventDropListAdapter adapter = new EventDropListAdapter(this, AppGlobalData.getInstance().getTagMemberArrayList(), tv);

        list.setAdapter(adapter);


    }

    public class EventDropListAdapter extends BaseAdapter {

        private ArrayList<TreeWrapper> mListItems;
        private LayoutInflater mInflater;
        private TextView mSelectedItems;
        int selectedCount = 0;
        String firstSelected = "";
        private ViewHolder holder;
        String selected = "";    //shortened selected values representation
//        ArrayList<String> namesArray = new ArrayList<>();

        String getSelected() {
            return selected;
        }

        public void setSelected(String selected) {
            this.selected = selected;
        }

        public EventDropListAdapter(Context context, ArrayList<TreeWrapper> items,
                                    TextView tv) {
            mListItems = new ArrayList<TreeWrapper>();
            mListItems.addAll(items);
            mInflater = LayoutInflater.from(context);
            mSelectedItems = tv;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return mListItems.size();
        }

        @Override
        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.drop_down_list_row, null);
                holder = new ViewHolder();
                holder.tv = (TextView) convertView.findViewById(R.id.SelectOption);
                holder.chkbox = (CheckBox) convertView.findViewById(R.id.checkbox);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.tv.setText(mListItems.get(position).getName());

            final int position1 = position;

            //whenever the checkbox is clicked the selected values textview is updated with new selected values
            holder.chkbox.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    mListItems.get(position).setIsSelected(
                            !mListItems.get(position).isSelected());
                    notifyDataSetChanged();
                    if (mListItems.get(position).isSelected()) {
                        membersIDs.add(mListItems.get(position).getNodeId());
                        namesArray.add(mListItems.get(position).getName());
                        namesArrayToShowAlreadySelected.add(mListItems.get(position).getNodeId());
                    } else {
                        membersIDs.remove(mListItems.get(position).getNodeId());
                        namesArray.remove(mListItems.get(position).getName());
                        namesArrayToShowAlreadySelected.remove(mListItems.get(position).getNodeId());

                    }
                    selectfamilymamberTxt.setText(TextUtils.join(",", namesArray));
//                    setText(position1);
                }
            });
//            holder.chkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    setText(position1);
//                    if (isChecked) {
////                        membersIDs.add(mListItems.get(position).getNodeId());
//                    }
//                }
//            });
            if (mListItems.get(position).isSelected()) {
                holder.chkbox.setChecked(true);

            } else {
                holder.chkbox.setChecked(false);

            }
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mListItems.get(position).setIsSelected(
                            !mListItems.get(position).isSelected());
                    notifyDataSetChanged();
                    if (mListItems.get(position).isSelected()) {
                        membersIDs.add(mListItems.get(position).getNodeId());
                        namesArray.add(mListItems.get(position).getName());
                        namesArrayToShowAlreadySelected.add(mListItems.get(position).getNodeId());
                    } else {
                        membersIDs.remove(mListItems.get(position).getNodeId());
                        namesArray.remove(mListItems.get(position).getName());
                        namesArrayToShowAlreadySelected.remove(mListItems.get(position).getNodeId());
                    }
                    selectfamilymamberTxt.setText(TextUtils.join(",", namesArray));
                }
            });

            return convertView;
        }


        /*
         * Function which updates the selected values display and information(checkSelected[])
         * */
        private void setText(int position1) {
            if (!UploadPhotosActivity.checkSelected[position1]) {
                UploadPhotosActivity.checkSelected[position1] = true;
                selectedCount++;
            } else {
                UploadPhotosActivity.checkSelected[position1] = false;
                selectedCount--;
            }

            if (selectedCount == 0) {
                mSelectedItems.setText(R.string.tag_family_mamber);
            } else if (selectedCount == 1) {
                for (int i = 0; i < UploadPhotosActivity.checkSelected.length; i++) {
                    if (UploadPhotosActivity.checkSelected[i] == true) {
                        firstSelected = mListItems.get(i).getName();
                        break;
                    }
                }
                mSelectedItems.setText(firstSelected);
                setSelected(firstSelected);
            } else if (selectedCount > 1) {
                for (int i = 0; i < UploadPhotosActivity.checkSelected.length; i++) {
                    if (UploadPhotosActivity.checkSelected[i] == true) {
                        firstSelected = mListItems.get(i).getName();
                        break;
                    }
                }
                mSelectedItems.setText(firstSelected + " & " + (selectedCount - 1) + " more");
                setSelected(firstSelected + " & " + (selectedCount - 1) + " more");
            }
        }

        private class ViewHolder {
            TextView tv;
            CheckBox chkbox;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            cameragalleryLayout.setOrientation(LinearLayout.HORIZONTAL);
//            selectfamilymamberTxt.setBackgroundResource(R.mipmap.drop_down_landscape);
//            tagfamilyeventSpinner.setBackgroundResource(R.mipmap.drop_down_landscape);
            languageCode(MyApplication.getLanguagecode());
        } else {
            cameragalleryLayout.setOrientation(LinearLayout.VERTICAL);
//            selectfamilymamberTxt.setBackgroundResource(R.mipmap.drop_down);
//            tagfamilyeventSpinner.setBackgroundResource(R.mipmap.drop_down);
            languageCode(MyApplication.getLanguagecode());
        }
    }

    public void languageCode(String code) {
        Locale locale;
        Configuration lconfig;
        locale = new Locale(code);
        Locale.setDefault(locale);
        lconfig = new Configuration();
        lconfig.locale = locale;
        getBaseContext().getResources().updateConfiguration(lconfig,
                getBaseContext().getResources().getDisplayMetrics());
    }

    private void audioRecordingView() {
        if (audioBln) {
            buttomtoUp(audioLayout);
            audioBln = false;
        } else {
            buttomtoUp(audioLayout);
            audioBln = true;
        }


        playImg.setBackgroundResource(R.mipmap.rec_play);
        stopImg.setBackgroundResource(R.mipmap.rec_record);

        closeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPlayer != null) {

                    mPlayer.stop();


                }
                stopRecording();

                uptoButtom(audioLayout);
            }
        });
        doneTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (mPlayer != null) {
//
//                    mPlayer.stop();
//
//
//                }
                stopRecording();
                mFileTemp = new File(fileName);
                fileHashMap.put("file1", mFileTemp);
                uptoButtom(audioLayout);
            }
        });
        playImg.setVisibility(View.GONE);
        playImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playBln) {
                    if (isRecording) {

                        stopRecording();
                    }
                    timeTxt.setVisibility(View.GONE);
                    stopImg.setVisibility(View.GONE);
                    playIt();
                    playImg.setBackgroundResource(R.mipmap.rec_pause);
                    playBln = false;
                } else {
                    if (mPlayer != null) {
                        recordingBln = true;
                        topTitleTxt.setText(getResources().getString(R.string.Recording));
                        stopImg.setBackgroundResource(R.mipmap.rec_record);
                        stopImg.setVisibility(View.VISIBLE);
                        playImg.setBackgroundResource(R.mipmap.rec_play);
                        mPlayer.stop();
                        playBln = true;

                    }
                }
            }
        });

        stopImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playImg.setVisibility(View.VISIBLE);
                if (recordingBln) {
                    timeTxt.setVisibility(View.VISIBLE);
                    recordingAudio();
                    topTitleTxt.setText(getResources().getString(R.string.Recording));
                    recordingBln = false;
                    stopImg.setBackgroundResource(R.mipmap.rec_stop);
                } else {
                    stopRecording();
                    recordingBln = true;
                    stopImg.setBackgroundResource(R.mipmap.rec_record);
                }

//                uptoButtom(audioLayout);
            }
        });
        recordstopImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopRecording();
            }
        });
    }

    public void recordingAudio() {
        handler = new Handler();
//        fileName = Environment.getExternalStorageDirectory() + "/audio" + System.currentTimeMillis() + ".mp3";


        fileName = FileUtils.getAppPath() + "Audio/";

        File file = new File(fileName);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                Toast.makeText(this, "Failed to create file", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        fileName = fileName + "audio_" + System.currentTimeMillis() + ".mp3";

        Log.v("audio_file_prep ", fileName);


        if (!isRecording) {
            //Create MediaRecorder and initialize audio source, output format, and audio encoder
            mRecorder = new MP3Recorder(new File(fileName));

//            mRecorder = new MediaRecorder();
//            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
//            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
//            mRecorder.setOutputFile(fileName);
//            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            // Starting record time
            recordTime = 0;
            // Show TextView that displays record time

            try {
                mRecorder.start();
            } catch (IOException e) {
                Log.e("LOG_TAG", "prepare failed");
            }
            // Start record job
//            mRecorder.start();
            isRecording = true;
            // Change isRecroding flag to true
//            String nameaudioFile = "audio" + System.currentTimeMillis() + ".mp3";
            recordingnameTxt.setText(fileName.toString());
            recordingnameTxt.setVisibility(View.GONE);
            // Post the record progress
            handler.post(UpdateRecordTime);
        }


    }

    Runnable UpdateRecordTime = new Runnable() {
        public void run() {
            if (isRecording) {
//                tv.setText(String.valueOf(recordTime));
                if (recordTime > 59) {
                    int minute = recordTime / 60;
                    int second = recordTime % 60;
                    if (minute > 59) {
                        int hour = minute / 60;
                        int minute1 = minute % 60;
                        timeTxt.setText(String.valueOf(hour) + ":" + String.valueOf(minute) + ":" + String.valueOf(second));
                    } else if (59 > minute) {
                        timeTxt.setText(String.valueOf(minute) + ":" + String.valueOf(second));
                    }

                } else if (59 > recordTime) {
                    if (recordTime < 10) {
                        timeTxt.setText("00:" + "0" + String.valueOf(recordTime));
                    } else {
                        timeTxt.setText("00:" + String.valueOf(recordTime));
                    }
                }


                recordTime += 1;
                // Delay 1s before next call
                handler.postDelayed(this, 1000);
            }
        }
    };

    public void stopRecording() {
        if (isRecording) {
            // Stop recording and release resource
            mRecorder.stop();
//            mRecorder.release();
            mRecorder = null;
            // Change isRecording flag to false
            isRecording = false;
            topTitleTxt.setText(getResources().getString(R.string.RecordingStop));
            // Hide TextView that shows record time

            // Play the audio
        }
    }

    public void playIt() {
        // Create MediaPlayer object
        mPlayer = new MediaPlayer();
        // set start time
        playTime = 0;
        // Reset max and progress of the SeekBar

        try {
            // Initialize the player and start playing the audio

            if (fileName != null) {

                mPlayer.setDataSource(fileName);
                mPlayer.prepare();
                mPlayer.start();
                topTitleTxt.setText(getResources().getString(R.string.Playing));
            }

        } catch (IOException e) {
            Log.e("LOG_TAG", "prepare failed");
        }
    }

    public void buttomtoUp(View view) {
        Animation bottomUp = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.bottom_up);

        view.startAnimation(bottomUp);
        view.setVisibility(View.VISIBLE);
    }

    public void uptoButtom(View view) {
        Animation bottomUp = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.bottom_down);

        view.startAnimation(bottomUp);
        view.setVisibility(View.GONE);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public class UploadFileToServer extends AsyncTask<Void, Integer, String> {


        @Override
        protected void onPreExecute() {

            super.onPreExecute();
//            dialog = ProgressDialog.show(UploadPhotosActivity.this, "", "Please wait....");
            pDialog.setMessage(getResources().getString(R.string.Uploading));
            showDialog();
//            pDialog.setProgress(0);
        }

//        @Override
//        protected void onProgressUpdate(Integer... progress) {
////            pDialog.setProgress(progress[0]);
//        }

        @Override
        protected String doInBackground(Void... params) {
            return multipost(apiUrl);
        }

        public String multipost(String urlString) {
            String response = "";
            int count;
            String responseStr = "";
            try {
                URL url = new URL(urlString);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                conn.setReadTimeout(60000);
                conn.setConnectTimeout(60000);
                conn.setRequestMethod("POST");
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);
                MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
//                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(HttpMultipartMode.BROWSER_COMPATIBLE,
//                        new AndroidMultiPartEntity.ProgressListener() {
//
//                            @Override
//                            public void transferred(long num) {
//                                publishProgress((int) ((num / (float) totalSize) * 100));
//                            }
//                        });
                String memberids = "";
                if (membersIDs != null) {
                    if (membersIDs.size() != 0) {
                        memberids = TextUtils.join(",", membersIDs);
                        System.out.println("memberids" + memberids);
                    }
                }

                if (mFileTemp != null) {
                    for (String key : fileHashMap.keySet()) {

                        entity.addPart(key, new FileBody(fileHashMap.get(key)));
                    }

                }


                // Extra parameters if you want to pass to server
                entity.addPart("IsPublic",
                        new StringBody("" + pulic_private));
                entity.addPart("EventIDs", new StringBody(eventID));
                String nodeId = MyApplication.getInstance().getCreateMemoryNodID();
                entity.addPart("UserId", new StringBody(nodeId));//usuario
                entity.addPart("LoginId", new StringBody(MyApplication.getUserID()));
                entity.addPart("TagFamilyMembers", new StringBody(memberids));
                totalSize = entity.getContentLength();


                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.addRequestProperty("Content-length", entity.getContentLength() + "");
                conn.addRequestProperty(entity.getContentType().getName(), entity.getContentType().getValue());

                OutputStream os = conn.getOutputStream();
                entity.writeTo(os);


                os.close();

                conn.connect();

                int responseCode = conn.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    responseStr = readStream(conn.getInputStream());
                    return responseStr;
                }

            } catch (Exception e) {
                Log.e("MainActivity", "multipart post error " + e + "(" + urlString + ")");
            }
            if (TextUtils.isEmpty(responseStr))
                return null;
            else return responseStr;
        }

        private String readStream(InputStream in) {
            BufferedReader reader = null;
            StringBuilder builder = new StringBuilder();
            try {
                reader = new BufferedReader(new InputStreamReader(in));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return builder.toString();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

//            HttpClient httpclient = new DefaultHttpClient();
//            HttpPost httppost = new HttpPost(apiUrl);

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });
                String memberids = "";
                if (membersIDs != null) {
                    if (membersIDs.size() != 0) {
                        memberids = TextUtils.join(",", membersIDs);
                        System.out.println("memberids" + memberids);
                    }
                }
//                HashMap<String, String> params = new HashMap<>();
////            params.put("AlbumId", albumidsSpinner.getSelectedItem().toString());
//                params.put("AlbumName", nameEdt.getText().toString());
//                params.put("IsPublic", "" + pulic_private);
//                params.put("EventIDs", eventID);
//                params.put("UserId", MyApplication.getUserID());
//                params.put("LoginId", MyApplication.getUserID());
//                params.put("TagFamilyMembers", memberids);
                if (mFileTemp != null) {
                    for (String key : fileHashMap.keySet()) {

                        entity.addPart(key, new FileBody(fileHashMap.get(key)));
                    }

                }


                // Extra parameters if you want to pass to server
                entity.addPart("IsPublic",
                        new StringBody("" + pulic_private));
                entity.addPart("EventIDs", new StringBody(eventID));
                entity.addPart("UserId", new StringBody(MyApplication.getUserID()));//usuario
                entity.addPart("LoginId", new StringBody(MyApplication.getUserID()));
                entity.addPart("TagFamilyMembers", new StringBody(memberids));
                totalSize = entity.getContentLength();
//                responseString=  multipost(apiUrl,entity);
//                httppost.setEntity(entity);
//
//                // Making server call
//                HttpResponse response = httpclient.execute(httppost);
//                HttpEntity r_entity = response.getEntity();
//
//                int statusCode = response.getStatusLine().getStatusCode();
//                if (statusCode == 200) {
//                    // Server response
//                    responseString = EntityUtils.toString(r_entity);
//                } else {
//                    responseString = "Error occurred! Http Status Code: "
//                            + statusCode;
//                }

            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {


            super.onPostExecute(result);
            try {
                hideDialog();
                if (result != null) {
                    try {
                        JSONObject responseObj = new JSONObject(result);
                        if (responseObj.getInt("StatusCode") == 1) {
                            MyApplication.getInstance().setLoad_profile(true);
                            fileHashMap.clear();
                            AlertDialog.Builder builder = new AlertDialog.Builder(UploadPhotosActivity.this);
                            builder.setTitle("" + getResources().getString(R.string.app_name));
                            builder.setMessage(getResources().getString(R.string.Successfullyuploaded))
                                    .setCancelable(false)
                                    .setPositiveButton("Ok",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                    finish();


                                                }
                                            });

                            builder.show();
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(UploadPhotosActivity.this);
                            builder.setTitle("" + getResources().getString(R.string.app_name));
                            builder.setMessage(responseObj.getString("Message"))
                                    .setCancelable(false)
                                    .setPositiveButton("Ok",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                    finish();


                                                }
                                            });

                            builder.show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                System.out.println("response" + result);
            } catch (Exception ex) {

            }
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    private void getTagMemberList() {
        List<NameValuePair> valuePairs = new ArrayList<>();
        valuePairs.add(new BasicNameValuePair("NodeId", MyApplication.getUserID()));
        valuePairs.add(new BasicNameValuePair("name", ""));
        if (MyApplication.isInternetWorking(UploadPhotosActivity.this)) {
            new WebTask(UploadPhotosActivity.this, Urls.tagmember, valuePairs, UploadPhotosActivity.this, TaskCode.TAGMEMBER, false).execute();
        }
    }

    ArrayList<TreeWrapper> tagMemberArraylist = new ArrayList<>();

    /*function parsing the data of tag member*/
    private void parseTagMemberData(String response) {

        if (response != null) {
            try {
                JSONObject responseObj = new JSONObject(response);
                if (responseObj.getString("StatusCode").equalsIgnoreCase("1")) {
                    JSONArray MembersArray = responseObj.getJSONArray("Members");
                    if (MembersArray.length() != 0) {
                        for (int j = 0; j < MembersArray.length(); j++) {
                            TreeWrapper treeWrapper = new TreeWrapper();
                            JSONObject nodesObj = MembersArray.getJSONObject(j);

                            treeWrapper.setNodeId(nodesObj.getString("NodeId"));
                            treeWrapper.setName(nodesObj.getString("Name"));
                            treeWrapper.setImageUrl(nodesObj.getString("Image"));
                            treeWrapper.setGender(nodesObj.getString("Gender"));
                            treeWrapper.setDOB(nodesObj.getString("DateOfBirth"));
                            tagMemberArraylist.add(treeWrapper);
                        }
                        AppGlobalData.getInstance().getTagMemberArrayList().clear();
                        AppGlobalData.getInstance().setTagMemberArrayList(tagMemberArraylist);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

}
