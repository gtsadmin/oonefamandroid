package com.onefam.activitys;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.onefam.R;
import com.onefam.applications.MyApplication;
import com.onefam.datacontroller.Constants;
import java.util.Locale;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
public class CreateMemoryActivity extends AppCompatActivity {
    @Bind(R.id.backLayout)
    LinearLayout backLayout;
    @Bind(R.id.topbarLayout)
    RelativeLayout topbarLayout;
    @Bind(R.id.takeuploadphotoLayout)
    LinearLayout takeuploadphotoLayout;
    @Bind(R.id.takeuploadvideosLayout)
    LinearLayout takeuploadvideosLayout;
    @Bind(R.id.takeuploadaduioLayout)
    LinearLayout takeuploadaduioLayout;
    @Bind(R.id.addeventLayout)
    LinearLayout addeventLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_memory);
        ButterKnife.bind(this);
    }
    @OnClick(R.id.backLayout)
    public void backLayout() {
        onBackPressed();
    }
    @OnClick(R.id.takeuploadphotoLayout)
    public void takeuploadphotoLayout() {
        startActivity(new Intent(CreateMemoryActivity.this, UploadPhotosActivity.class).putExtra("num", Constants.UPLOADPHOTO));
    }
    @OnClick(R.id.takeuploadvideosLayout)
    public void takeuploadvideosLayout() {
        startActivity(new Intent(CreateMemoryActivity.this, UploadPhotosActivity.class).putExtra("num", Constants.UPLOADVIDEOS));
    }

    @OnClick(R.id.takeuploadaduioLayout)
    public void takeuploadaduioLayout() {
        startActivity(new Intent(CreateMemoryActivity.this, UploadPhotosActivity.class).putExtra("num", Constants.UPLOADAUDIO));
    }

    @OnClick(R.id.addeventLayout)
    public void addeventLayout() {
        MyApplication.myApplication.produceAnimation(addeventLayout);
        startActivity(new Intent(CreateMemoryActivity.this, CreateEventActivity.class));
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            languageCode(MyApplication.getLanguagecode());
            setContentView(R.layout.create_memory_land);
            ButterKnife.bind(this);

        } else {
            languageCode(MyApplication.getLanguagecode());
            setContentView(R.layout.create_memory);
            ButterKnife.bind(this);

        }
    }

    public void languageCode(String code) {
        Locale locale;
        Configuration lconfig;
        locale = new Locale(code);
        Locale.setDefault(locale);
        lconfig = new Configuration();
        lconfig.locale = locale;
        getBaseContext().getResources().updateConfiguration(lconfig,
                getBaseContext().getResources().getDisplayMetrics());
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        finish();
    }
}
