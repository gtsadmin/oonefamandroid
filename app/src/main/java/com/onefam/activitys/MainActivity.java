package com.onefam.activitys;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.gcm.GCMRegistrar;
import com.onefam.R;
import com.onefam.applications.MyApplication;
import com.onefam.views.TextViewProximaNovaRegular;

import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.leolin.shortcutbadger.ShortcutBadger;

public class MainActivity extends Activity {

    @Bind(R.id.welcomelogoImg)
    ImageView welcomelogoImg;
    @Bind(R.id.forgetpasswordTxt)
    TextViewProximaNovaRegular forgetpasswordTxt;
    @Bind(R.id.signupTxt)
    TextViewProximaNovaRegular signupTxt;
    @Bind(R.id.termsTxt)
    TextViewProximaNovaRegular termsTxt;

    @Bind(R.id.bottomLayout)
    RelativeLayout bottomLayout;
    @Bind(R.id.loginBtn)
    Button loginBtn;
    String deviceId = "";
    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
//        int badgeCount = 1;
        sp = getSharedPreferences("OneFam", 0);

        SharedPreferences.Editor edit = sp.edit();
        edit.putInt("Badger", 0);
        edit.commit();
        ShortcutBadger.removeCount(MainActivity.this); //for 1.1.4+
//        ShortcutBadger.with(getApplicationContext()).remove();
        if (!MyApplication.getUserID().equalsIgnoreCase("")) {
            startActivity(new Intent(MainActivity.this, FamilyTreeActivity.class));
            finish();
        }
//        registerapp();
    }

    @OnClick(R.id.loginBtn)
    public void loginBtn() {
        MyApplication.myApplication.produceAnimation(loginBtn);
        startActivity(new Intent(MainActivity.this, LoginActivity.class));

    }

    @OnClick(R.id.forgetpasswordTxt)
    public void forgetpasswordTxt() {
        startActivity(new Intent(MainActivity.this, ResetPasswordActivity.class));
    }

    @OnClick(R.id.signupTxt)
    public void signupTxt() {
        startActivity(new Intent(MainActivity.this, SignUpActivity.class));
    }

    @OnClick(R.id.termsTxt)
    public void termsTxt() {
        startActivity(new Intent(MainActivity.this, TermsConditionsActivity.class));
    }


    public void registerapp() {
        try {
            GCMRegistrar.checkDevice(this);
            GCMRegistrar.checkManifest(this);

            deviceId = GCMRegistrar.getRegistrationId(this);

            Log.d("app GCM ID", "REG ID == " + deviceId);
            // device_id = Splash.deviceId;
            if (GCMRegistrar.getRegistrationId(this).length() == 0) {

                Log.d("app reg id", "REG ID sss== " + deviceId);

                GCMRegistrar.register(this, MyApplication.GOOGLE_SENDER_ID);

                // registerapp();
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long arg0) {
                    }

                    @Override
                    public void onFinish() {
                        if (deviceId.equalsIgnoreCase("")) {
                            registerapp();
                            Log.d("response", "m here");
                        } else {
                            if (MyApplication.isInternetWorking(MainActivity.this)) {

                            }

                        }
                    }
                }.start();

            } else {
                Log.d("app", "Already registered");

            }

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            languageCode(MyApplication.getLanguagecode());
        } else {
            languageCode(MyApplication.getLanguagecode());
        }
    }

    public void languageCode(String code) {
        Locale locale;
        Configuration lconfig;
        locale = new Locale(code);
        Locale.setDefault(locale);
        lconfig = new Configuration();
        lconfig.locale = locale;
        getBaseContext().getResources().updateConfiguration(lconfig,
                getBaseContext().getResources().getDisplayMetrics());
    }
}
