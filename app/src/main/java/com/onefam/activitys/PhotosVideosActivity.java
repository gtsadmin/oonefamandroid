package com.onefam.activitys;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.onefam.R;
import com.onefam.applications.MyApplication;
import com.onefam.datacontroller.Constants;
import com.onefam.views.TextViewProximaNovaRegular;
import com.onefam.webutility.ParseData;
import com.onefam.webutility.TaskCode;
import com.onefam.webutility.Urls;
import com.onefam.webutility.WebTask;
import com.onefam.webutility.WebTaskComplete;
import com.onefam.wrapper.GetProfileMediaWrapper;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.onefam.R.id.deleteImg;
import static com.onefam.R.id.emailId;
import static com.onefam.R.id.passwordEdt;


public class PhotosVideosActivity extends AppCompatActivity implements WebTaskComplete {
    @Bind(R.id.titleTxt)
    TextViewProximaNovaRegular titleTxt;
    @Bind(R.id.backLayout)
    LinearLayout backLayout;
    @Bind(R.id.topbarLayout)
    RelativeLayout topbarLayout;
    @Bind(R.id.profileImg)
    ImageView profileImg;
    @Bind(R.id.nameTxt)
    TextViewProximaNovaRegular nameTxt;
    @Bind(R.id.titlegridTxt)
    TextViewProximaNovaRegular titlegridTxt;
    @Bind(R.id.galleryGrid)
    GridView galleryGrid;
    @Bind(R.id.iconImg)
    ImageView iconImg;
    int title_num;

    public static ArrayList<GetProfileMediaWrapper> getProfileMediaWrappers;
    @Bind(R.id.profile_image)
    CircleImageView profileImage;
    @Bind(R.id.closeLayout)
    LinearLayout closeLayout;
    @Bind(R.id.doneTxt)
    ImageView doneTxt;
    @Bind(R.id.tiltle)
    RelativeLayout tiltle;
    @Bind(R.id.timeTxt)
    TextViewProximaNovaRegular timeTxt;
    @Bind(R.id.playImg)
    ImageView playImg;
    @Bind(R.id.stopImg)
    ImageView stopImg;
    @Bind(R.id.progressBar1)
    ProgressBar progressBar1;
    @Bind(R.id.recordingnameTxt)
    TextViewProximaNovaRegular recordingnameTxt;
    @Bind(R.id.audioLayout)
    RelativeLayout audioLayout;
    MediaPlayer mediaPlayer;
    Handler myHandler;

    PhotoVideoCollection photoVideoCollection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photos_layout);
        ButterKnife.bind(this);
        nameTxt.setText(getIntent().getExtras().getString("name"));
        title_num = getIntent().getExtras().getInt("title");
        List<NameValuePair> valuePairs = new ArrayList<>();
        valuePairs.add(new BasicNameValuePair("NodeId", getIntent().getExtras().getString("id")));
        valuePairs.add(new BasicNameValuePair("AlbumId", ""));
        valuePairs.add(new BasicNameValuePair("StartIndex", "1"));
        valuePairs.add(new BasicNameValuePair("PageSize", "25"));


        if (title_num == Constants.PHOTO) {
            titleTxt.setText(getResources().getString(R.string.photos));
            titlegridTxt.setText(getResources().getString(R.string.photos));
            Drawable drawable = getResources().getDrawable(R.mipmap.icn_photos);
            iconImg.setImageDrawable(drawable);
            if (MyApplication.isInternetWorking(PhotosVideosActivity.this)) {
                new WebTask(PhotosVideosActivity.this, Urls.ViewAllImages, valuePairs, this, TaskCode.ALLIMAGECODE).execute();
            }
        } else if (title_num == Constants.VIDEO) {
            Drawable drawable = getResources().getDrawable(R.mipmap.icn_video);
            iconImg.setImageDrawable(drawable);
            titleTxt.setText(getResources().getString(R.string.videos));
            titlegridTxt.setText(getResources().getString(R.string.videos));
            if (MyApplication.isInternetWorking(PhotosVideosActivity.this)) {

                new WebTask(PhotosVideosActivity.this, Urls.ViewAllVideos, valuePairs, this, TaskCode.ALLVIDEOSCODE).execute();
            }


        } else if (title_num == Constants.DOC) {
            titleTxt.setText("Doc");
            titlegridTxt.setText("Doc");
        } else if (title_num == Constants.AUDIO) {
            titleTxt.setText("Audio");
            titlegridTxt.setText("Audio");
            Drawable drawable = getResources().getDrawable(R.mipmap.icn_audio);
            iconImg.setImageDrawable(drawable);
            if (MyApplication.isInternetWorking(PhotosVideosActivity.this)) {

                new WebTask(PhotosVideosActivity.this, Urls.viewAllAudios, valuePairs, this, TaskCode.ALLVIDEOSCODE).execute();
            }
        }


        MyApplication.loader.displayImage(getIntent().getExtras().getString("iamge"), profileImage, MyApplication.options);

    }

    @OnClick(R.id.backLayout)
    public void backLayout() {
        onBackPressed();
    }

    @OnClick(R.id.iconImg)
    public void iconImg() {
        if (title_num == Constants.PHOTO) {
            startActivity(new Intent(PhotosVideosActivity.this, UploadPhotosActivity.class).putExtra("num", Constants.UPLOADPHOTO));

        } else if (title_num == Constants.VIDEO) {
            startActivity(new Intent(PhotosVideosActivity.this, UploadPhotosActivity.class).putExtra("num", Constants.UPLOADVIDEOS));

        } else if (title_num == Constants.AUDIO) {
            startActivity(new Intent(PhotosVideosActivity.this, UploadPhotosActivity.class).putExtra("num", Constants.UPLOADAUDIO));

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (MyApplication.getInstance().isLoad_profile()) {
//            MyApplication.getInstance().setLoad_profile(false);
            List<NameValuePair> valuePairs = new ArrayList<>();
            valuePairs.add(new BasicNameValuePair("NodeId", getIntent().getExtras().getString("id")));
            valuePairs.add(new BasicNameValuePair("AlbumId", ""));
            valuePairs.add(new BasicNameValuePair("StartIndex", "1"));
            valuePairs.add(new BasicNameValuePair("PageSize", "25"));


            if (title_num == Constants.PHOTO) {
                titleTxt.setText(getResources().getString(R.string.photos));
                titlegridTxt.setText(getResources().getString(R.string.photos));
                Drawable drawable = getResources().getDrawable(R.mipmap.icn_photos);
                iconImg.setImageDrawable(drawable);
                if (MyApplication.isInternetWorking(PhotosVideosActivity.this)) {
                    new WebTask(PhotosVideosActivity.this, Urls.ViewAllImages, valuePairs, this, TaskCode.ALLIMAGECODE).execute();
                }
            } else if (title_num == Constants.VIDEO) {
                Drawable drawable = getResources().getDrawable(R.mipmap.icn_video);
                iconImg.setImageDrawable(drawable);
                titleTxt.setText(getResources().getString(R.string.videos));
                titlegridTxt.setText(getResources().getString(R.string.videos));
                if (MyApplication.isInternetWorking(PhotosVideosActivity.this)) {

                    new WebTask(PhotosVideosActivity.this, Urls.ViewAllVideos, valuePairs, this, TaskCode.ALLVIDEOSCODE).execute();
                }


            } else if (title_num == Constants.DOC) {
                titleTxt.setText("Doc");
                titlegridTxt.setText("Doc");
            } else if (title_num == Constants.AUDIO) {
                titleTxt.setText("Audio");
                titlegridTxt.setText("Audio");
                Drawable drawable = getResources().getDrawable(R.mipmap.icn_audio);
                iconImg.setImageDrawable(drawable);
                if (MyApplication.isInternetWorking(PhotosVideosActivity.this)) {

                    new WebTask(PhotosVideosActivity.this, Urls.viewAllAudios, valuePairs, this, TaskCode.ALLVIDEOSCODE).execute();
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    boolean startPuase = true;

    @OnClick(R.id.playImg)
    public void playImg() {
        if (startPuase) {
            if (mediaPlayer != null) {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                    playImg.setBackgroundResource(R.mipmap.rec_play);
                    startPuase = false;
                }
            }

        } else {
            if (mediaPlayer != null) {

                mediaPlayer.start();
                playImg.setBackgroundResource(R.mipmap.rec_pause);
                startPuase = true;

            }

        }
    }

    @OnClick(R.id.closeLayout)
    public void closeLayout() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }
        uptoButtom(audioLayout);
    }

    @OnClick(R.id.stopImg)
    public void stopImg() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }
        uptoButtom(audioLayout);
    }

    @Override
    public void onComplete(String response, int taskcode) {
        if (taskcode == TaskCode.ALLIMAGECODE) {
            getProfileMediaWrappers = new ParseData(PhotosVideosActivity.this, response, TaskCode.ALLIMAGECODE).getAllImage();
            photoVideoCollection = new PhotoVideoCollection(PhotosVideosActivity.this, getProfileMediaWrappers);
            galleryGrid.setAdapter(photoVideoCollection);

        } else if (taskcode == TaskCode.ALLVIDEOSCODE) {
            getProfileMediaWrappers = new ParseData(PhotosVideosActivity.this, response, TaskCode.ALLVIDEOSCODE).getAllVideos();
            photoVideoCollection = new PhotoVideoCollection(PhotosVideosActivity.this, getProfileMediaWrappers);
            galleryGrid.setAdapter(photoVideoCollection);
        } else if (taskcode == TaskCode.GETALLAUDIO) {
            getProfileMediaWrappers = new ParseData(PhotosVideosActivity.this, response, TaskCode.GETALLAUDIO).getAllAudio();
            photoVideoCollection = new PhotoVideoCollection(PhotosVideosActivity.this, getProfileMediaWrappers);
            galleryGrid.setAdapter(photoVideoCollection);
        } else if (taskcode == TaskCode.DELETEMEDIA) {
            Log.v("delete_response ", response);
            MyApplication.getInstance().setLoad_profile(true);

        }
    }

    public class PhotoVideoCollection extends BaseAdapter {
        ArrayList<GetProfileMediaWrapper> getProfileMediaWrappers;
        Context context;
//        ViewHolder holder = null;

        public PhotoVideoCollection(Context context, ArrayList<GetProfileMediaWrapper> getProfileMediaWrappers) {
            this.getProfileMediaWrappers = getProfileMediaWrappers;
            this.context = context;
        }

        @Override
        public int getCount() {
            return getProfileMediaWrappers.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;

            if (convertView == null) {
                holder = new ViewHolder();
                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.photo_videos_list_layout, parent,false);

                holder.thumbImg = (ImageView) convertView.findViewById(R.id.thumbImg);
                holder.loadBar = (ProgressBar) convertView.findViewById(R.id.loadBar);
                holder.deleteImg = (ImageView) convertView.findViewById(deleteImg);

                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();

            }
            holder.thumbImg.setTag(position);
            holder.deleteImg.setTag(position);

            if (title_num == Constants.PHOTO) {
                holder.thumbImg.setScaleType(ImageView.ScaleType.CENTER_CROP);
                MyApplication.loader.displayImage(Urls.profilemedia_basephotoUrl + getProfileMediaWrappers.get(position).getFileName(), holder.thumbImg, MyApplication.options);
                holder.thumbImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Integer in = (Integer) v.getTag();
                        startActivity(new Intent(PhotosVideosActivity.this, PreviewActivity.class).putExtra("title", Constants.PHOTO).putExtra("pos", in.intValue()).putExtra("activitycode", Constants.ALLPREVIEWACTIVITYCODE));
                    }
                });
            } else if (title_num == Constants.VIDEO) {
                holder.thumbImg.setScaleType(ImageView.ScaleType.CENTER_CROP);
                MyApplication.loader.displayImage(Urls.profilemedia_basevideoThumbsUrl + getProfileMediaWrappers.get(position).getThumbPath(), holder.thumbImg, MyApplication.options);
                holder.thumbImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Integer in = (Integer) v.getTag();
                        startActivity(new Intent(PhotosVideosActivity.this, PreviewActivity.class).putExtra("title", Constants.VIDEO).putExtra("pos", in.intValue()).putExtra("activitycode", Constants.ALLPREVIEWACTIVITYCODE));
                    }
                });
            } else if (title_num == Constants.AUDIO) {
                holder.thumbImg.setScaleType(ImageView.ScaleType.FIT_CENTER);
                String imageUri = "drawable://" + R.mipmap.thumb_audio;
                MyApplication.loader.displayImage(imageUri, holder.thumbImg, MyApplication.options);
                holder.thumbImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //     startActivity(new Intent(PhotosVideosActivity.this, PreviewActivity.class).putExtra("title", Constants.AUDIO).putExtra("pos", position));
                        Integer in = (Integer) v.getTag();
                        if (audioLayout.isShown()) {
                            if (mediaPlayer != null) {
                                if (mediaPlayer.isPlaying()) {
                                    mediaPlayer.stop();
                                }
                            }
                            uptoButtom(audioLayout);
                        } else {
                            buttomtoUp(audioLayout);
                            mediaPlayer(getProfileMediaWrappers.get(in.intValue()).getFileName());
                        }
                    }
                });
            }


            holder.deleteImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Integer pos = (Integer) v.getTag();

                    AlertDialog.Builder builder = new AlertDialog.Builder(PhotosVideosActivity.this);
                    builder.setTitle("" + getResources().getString(R.string.app_name));
                    builder.setMessage(getResources().getString(R.string.Areyousureyouwanttodeletemedia))
                            .setCancelable(false)
                            .setPositiveButton(getResources().getString(R.string.no),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    })
                            .setNegativeButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                    deleteNotesitem(getProfileMediaWrappers.get(pos.intValue()).getId());
                                    getProfileMediaWrappers.remove(pos.intValue());

//                                            photoVideoCollection.notifyDataSetChanged();
//                                            galleryGrid.invalidate();
                                    photoVideoCollection = new PhotoVideoCollection(PhotosVideosActivity.this, getProfileMediaWrappers);
                                    galleryGrid.setAdapter(photoVideoCollection);
                                }
                            });
                    builder.show();


//                    getProfileMediaWrappers.get(position)

                }
            });


            return convertView;
        }

        public class ViewHolder {
            ImageView thumbImg, deleteImg;
            ProgressBar loadBar;

        }
    }

    public void mediaPlayer(String str) {


        myHandler = new Handler();

        String fileUrl = Urls.profilemedia_baseaudioUrl + str;
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        try {
            mediaPlayer.setDataSource(fileUrl);
            mediaPlayer.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mediaPlayer.start();
        playImg.setBackgroundResource(R.mipmap.rec_pause);

        progressBar1.setMax(mediaPlayer.getDuration());
        seekUpdation();
//        mediaPlayer.set


    }

    Runnable run = new Runnable() {

        @Override
        public void run() {
            seekUpdation();
        }
    };

    public void seekUpdation() {

        progressBar1.setProgress(mediaPlayer.getCurrentPosition());
        myHandler.postDelayed(run, 1000);
    }


    public void buttomtoUp(View view) {
        Animation bottomUp = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.bottom_up);

        view.startAnimation(bottomUp);
        view.setVisibility(View.VISIBLE);
    }

    public void uptoButtom(View view) {
        Animation bottomUp = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.bottom_down);

        view.startAnimation(bottomUp);
        view.setVisibility(View.GONE);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            languageCode(MyApplication.getLanguagecode());
        } else {
            languageCode(MyApplication.getLanguagecode());
        }
    }

    public void languageCode(String code) {
        Locale locale;
        Configuration lconfig;
        locale = new Locale(code);
        Locale.setDefault(locale);
        lconfig = new Configuration();
        lconfig.locale = locale;
        getBaseContext().getResources().updateConfiguration(lconfig,
                getBaseContext().getResources().getDisplayMetrics());
    }

    /*function to delete the image*/
    public void deleteNotesitem(Integer ItemId) {
        Log.v("deleting_note_id ", ItemId + " ");
        List<NameValuePair> valuePairs = new ArrayList<>();
        valuePairs.add(new BasicNameValuePair("LoginId", MyApplication.getUserID()));
        valuePairs.add(new BasicNameValuePair("MediaId", ItemId.toString()));
        if (MyApplication.isInternetWorking(PhotosVideosActivity.this)) {
            new WebTask(PhotosVideosActivity.this, Urls.deletemedia, valuePairs, PhotosVideosActivity.this, TaskCode.DELETEMEDIA).execute();
        }
    }

    private void logoutAlert() {

    }


}
