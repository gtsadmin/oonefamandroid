package com.onefam.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;
import android.widget.RadioButton;


public class RadioButtonProximaNovaRegular extends RadioButton {
    private static final String TAG = "RadioButton";

    public RadioButtonProximaNovaRegular(Context context) {
        super(context);
    }

    public RadioButtonProximaNovaRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public RadioButtonProximaNovaRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
     
        setCustomFont(ctx);
      
    }

    public boolean setCustomFont(Context ctx) {
        Typeface tf = null;
        try {
        tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/ProximaNova-Regular.otf");
        } catch (Exception e) {
            Log.e(TAG, "Could not get typeface: "+e.getMessage());
            return false;
        }

        setTypeface(tf);  
        return true;
    }

}