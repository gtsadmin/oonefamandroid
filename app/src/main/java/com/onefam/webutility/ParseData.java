package com.onefam.webutility;

import android.content.Context;
import android.text.TextUtils;

import com.onefam.R;
import com.onefam.datacontroller.AppGlobalData;
import com.onefam.datacontroller.Constants;
import com.onefam.wrapper.GetEventWrapper;
import com.onefam.wrapper.GetProfileMediaWrapper;
import com.onefam.wrapper.InvitationsFriendsWrapper;
import com.onefam.wrapper.MemberInfo;
import com.onefam.wrapper.MemberInfoWrapper;
import com.onefam.wrapper.NotificationWrapper;
import com.onefam.wrapper.SearchMemberWrapper;
import com.onefam.wrapper.TreeWrapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class ParseData {
    Context context;
    String response;
    int taskcode;

    public ParseData(Context context, String response, int taskcode) {
        this.context = context;
        this.response = response;
        this.taskcode = taskcode;

    }

    public HashMap<String, ArrayList<GetProfileMediaWrapper>> getProfileMediaWrappers() {
        HashMap<String, ArrayList<GetProfileMediaWrapper>> mapgetprofilemedia = new HashMap<>();
        ArrayList<GetProfileMediaWrapper> getProfileMediaWrappers = new ArrayList<>();
        try {
            JSONObject responseObj = new JSONObject(response);
            if (responseObj.has("MediaItems")) {
                JSONArray mediaitemsArr = responseObj.getJSONArray("MediaItems");
                for (int i = 0; i < mediaitemsArr.length(); i++) {
                    GetProfileMediaWrapper getProfileMediaWrapper = new GetProfileMediaWrapper();
                    JSONObject subObj = mediaitemsArr.getJSONObject(i);
                    if (subObj.has("Id")) {
                        getProfileMediaWrapper.setId(subObj.getInt("Id"));
                    }
                    if (subObj.has("MediaType")) {

                        getProfileMediaWrapper.setMediaType(subObj.getInt("MediaType"));
                    }
                    if (subObj.has("FileName")) {
                        getProfileMediaWrapper.setFileName(subObj.getString("FileName"));
                    }
                    if (subObj.has("OriginalFileName")) {
                        getProfileMediaWrapper.setOriginalFileName(subObj.getString("OriginalFileName"));
                    }
                    if (subObj.has("UploadedDate")) {
                        getProfileMediaWrapper.setUploadedDate(subObj.getString("UploadedDate"));
                    }
                    if (subObj.has("AlbumID")) {

                        getProfileMediaWrapper.setAlbumID(subObj.getString("AlbumID"));
                    }
                    if (subObj.has("ThumbPath")) {
                        getProfileMediaWrapper.setThumbPath(subObj.getString("ThumbPath"));
                    }
                    if (subObj.has("Description")) {
                        getProfileMediaWrapper.setDescription(subObj.getString("Description"));
                    }
                    getProfileMediaWrappers.add(getProfileMediaWrapper);

                }
                Collections.reverse(getProfileMediaWrappers);

                for (int k = 0; k < getProfileMediaWrappers.size(); k++) {
                    if (getProfileMediaWrappers.get(k).getMediaType() != Constants.DOC) {
                        if (mapgetprofilemedia.get(String.valueOf(getProfileMediaWrappers.get(k).getMediaType())) == null) {
                            ArrayList<GetProfileMediaWrapper> getProfileMediaWrappers1 = new ArrayList<>();
                            getProfileMediaWrappers1.add(getProfileMediaWrappers.get(k));
                            mapgetprofilemedia.put(String.valueOf(getProfileMediaWrappers.get(k).getMediaType()), getProfileMediaWrappers1);
                        } else {
                            ArrayList<GetProfileMediaWrapper> getProfileMediaWrappers1 = mapgetprofilemedia.get(String.valueOf(getProfileMediaWrappers.get(k).getMediaType()));
                            getProfileMediaWrappers1.add(getProfileMediaWrappers.get(k));
                            mapgetprofilemedia.put(String.valueOf(getProfileMediaWrappers.get(k).getMediaType()), getProfileMediaWrappers1);
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mapgetprofilemedia;
    }

    public ArrayList<GetProfileMediaWrapper> getAllImage() {
        ArrayList<GetProfileMediaWrapper> getProfileMediaWrappers = new ArrayList<>();
        try {
            JSONObject responseObj = new JSONObject(response);
            if (responseObj.has("MediaItems")) {
                JSONArray mediaitemsArr = responseObj.getJSONArray("MediaItems");
                for (int i = 0; i < mediaitemsArr.length(); i++) {
                    GetProfileMediaWrapper getProfileMediaWrapper = new GetProfileMediaWrapper();
                    JSONObject subObj = mediaitemsArr.getJSONObject(i);
                    if (subObj.has("Id")) {
                        getProfileMediaWrapper.setId(subObj.getInt("Id"));
                    }
                    if (subObj.has("MediaType")) {
                        getProfileMediaWrapper.setMediaType(subObj.getInt("MediaType"));
                    }
                    if (subObj.has("FileName")) {
                        getProfileMediaWrapper.setFileName(subObj.getString("FileName"));
                    }
                    if (subObj.has("OriginalFileName")) {
                        getProfileMediaWrapper.setOriginalFileName(subObj.getString("OriginalFileName"));
                    }
                    if (subObj.has("UploadedDate")) {
                        getProfileMediaWrapper.setUploadedDate(subObj.getString("UploadedDate"));
                    }
                    if (subObj.has("AlbumID")) {

                        getProfileMediaWrapper.setAlbumID(subObj.getString("AlbumID"));
                    }
                    if (subObj.has("ThumbPath")) {
                        getProfileMediaWrapper.setThumbPath(subObj.getString("ThumbPath"));
                    }
                    if (subObj.has("Description")) {
                        getProfileMediaWrapper.setDescription(subObj.getString("Description"));
                    }
                    getProfileMediaWrappers.add(getProfileMediaWrapper);

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return getProfileMediaWrappers;
    }

    public ArrayList<GetProfileMediaWrapper> getAllVideos() {
        ArrayList<GetProfileMediaWrapper> getProfileMediaWrappers = new ArrayList<>();
        try {
            JSONObject responseObj = new JSONObject(response);
            if (responseObj.has("MediaItems")) {
                JSONArray mediaitemsArr = responseObj.getJSONArray("MediaItems");
                for (int i = 0; i < mediaitemsArr.length(); i++) {
                    GetProfileMediaWrapper getProfileMediaWrapper = new GetProfileMediaWrapper();
                    JSONObject subObj = mediaitemsArr.getJSONObject(i);
                    if (subObj.has("Id")) {
                        getProfileMediaWrapper.setId(subObj.getInt("Id"));
                    }
                    if (subObj.has("MediaType")) {
                        getProfileMediaWrapper.setMediaType(subObj.getInt("MediaType"));
                    }
                    if (subObj.has("FileName")) {
                        getProfileMediaWrapper.setFileName(subObj.getString("FileName"));
                    }
                    if (subObj.has("OriginalFileName")) {
                        getProfileMediaWrapper.setOriginalFileName(subObj.getString("OriginalFileName"));
                    }
                    if (subObj.has("UploadedDate")) {
                        getProfileMediaWrapper.setUploadedDate(subObj.getString("UploadedDate"));
                    }
                    if (subObj.has("AlbumID")) {

                        getProfileMediaWrapper.setAlbumID(subObj.getString("AlbumID"));
                    }
                    if (subObj.has("ThumbPath")) {
                        getProfileMediaWrapper.setThumbPath(subObj.getString("ThumbPath"));
                    }
                    if (subObj.has("Description")) {
                        getProfileMediaWrapper.setDescription(subObj.getString("Description"));
                    }
                    getProfileMediaWrappers.add(getProfileMediaWrapper);

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return getProfileMediaWrappers;
    }

    public ArrayList<GetProfileMediaWrapper> getAllAudio() {
        ArrayList<GetProfileMediaWrapper> getProfileMediaWrappers = new ArrayList<>();
        try {
            JSONObject responseObj = new JSONObject(response);
            if (responseObj.has("MediaItems")) {
                JSONArray mediaitemsArr = responseObj.getJSONArray("MediaItems");
                for (int i = 0; i < mediaitemsArr.length(); i++) {
                    GetProfileMediaWrapper getProfileMediaWrapper = new GetProfileMediaWrapper();
                    JSONObject subObj = mediaitemsArr.getJSONObject(i);
                    if (subObj.has("Id")) {
                        getProfileMediaWrapper.setId(subObj.getInt("Id"));
                    }
                    if (subObj.has("MediaType")) {
                        getProfileMediaWrapper.setMediaType(subObj.getInt("MediaType"));
                    }
                    if (subObj.has("FileName")) {
                        getProfileMediaWrapper.setFileName(subObj.getString("FileName"));
                    }
                    if (subObj.has("OriginalFileName")) {
                        getProfileMediaWrapper.setOriginalFileName(subObj.getString("OriginalFileName"));
                    }
                    if (subObj.has("UploadedDate")) {
                        getProfileMediaWrapper.setUploadedDate(subObj.getString("UploadedDate"));
                    }
                    if (subObj.has("AlbumID")) {

                        getProfileMediaWrapper.setAlbumID(subObj.getString("AlbumID"));
                    }
                    if (subObj.has("ThumbPath")) {
                        getProfileMediaWrapper.setThumbPath(subObj.getString("ThumbPath"));
                    }
                    if (subObj.has("Description")) {
                        getProfileMediaWrapper.setDescription(subObj.getString("Description"));
                    }
                    getProfileMediaWrappers.add(getProfileMediaWrapper);

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return getProfileMediaWrappers;
    }

    public ArrayList<MemberInfoWrapper> getViewTreeFamilyData() {
        ArrayList<MemberInfoWrapper> memberInfoWrapperArrayList = new ArrayList<>();
        try {
            JSONObject responseObj = new JSONObject(response);
            if (responseObj.has("Members")) {
                JSONArray membersArr = responseObj.getJSONArray("Members");
                for (int i = 0; i < membersArr.length(); i++) {
                    MemberInfoWrapper memberInfoWrapper = new MemberInfoWrapper();
                    JSONObject subObj = membersArr.getJSONObject(i);
                    if (subObj.has("NodeId")) {
                        memberInfoWrapper.setNodeId(subObj.getString("NodeId"));
                    }
                    if (subObj.has("Name")) {
                        memberInfoWrapper.setName(subObj.getString("Name"));
                    }
                    if (subObj.has("RelationShip")) {
                        memberInfoWrapper.setRelation(subObj.getString("RelationShip"));
                    }
                    memberInfoWrapper.setDOB(subObj.getString("DateOfBirth"));
                    if (!subObj.getString("Name").equalsIgnoreCase("")) {
                        memberInfoWrapperArrayList.add(memberInfoWrapper);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return memberInfoWrapperArrayList;
    }

    public ArrayList<TreeWrapper>   getTreeMember() {
        ArrayList<TreeWrapper> treeWrapperArrayList = new ArrayList<>();
        ArrayList<String> memberList = new ArrayList<>();
        HashMap<String, String> memberlistMap = new HashMap<>();
        HashMap<String, String> membernamelistMap = new HashMap<>();
        try {
            JSONObject responseObj = new JSONObject(response);
            if (responseObj.has("Tree")) {
                String teamId = "", level = "", parentTeam = "", relationLevel = "";
                JSONArray treeArray = responseObj.getJSONArray("Tree");
                for (int i = 0; i < treeArray.length(); i++) {
                    JSONObject treeObj = treeArray.getJSONObject(i);
                    teamId = treeObj.getString("TeamId");
                    level = treeObj.getString("level");
                    relationLevel = treeObj.getString("RelationLevel");
                    parentTeam = treeObj.getString("ParentTeam");
                    if (treeObj.has("Nodes")) {
                        JSONArray nodesArray = treeObj.getJSONArray("Nodes");
                        for (int j = 0; j < nodesArray.length(); j++) {
                            TreeWrapper treeWrapper = new TreeWrapper();
                            JSONObject nodesObj = nodesArray.getJSONObject(j);
                            treeWrapper.setTeamId(teamId);
                            treeWrapper.setParentTeam(parentTeam);
                            treeWrapper.setLevel(level);
                            treeWrapper.setNodeId(nodesObj.getString("NodeId"));
                            treeWrapper.setRelation(nodesObj.getString("Relation"));
                            treeWrapper.setBirthAddress(nodesObj.getString("BirthAddress"));
                            treeWrapper.setRelationLevel(relationLevel);
                            treeWrapper.setDOB(nodesObj.getString("DOB"));
                            treeWrapper.setGender(nodesObj.getString("Gender"));
                            treeWrapper.setImageUrl(nodesObj.getString("ImageUrl"));
                            treeWrapper.setCanDelete(nodesObj.getString("CanDelete"));
                            treeWrapper.setName(nodesObj.getString("Name"));
                            treeWrapper.setCanInvite(nodesObj.getString("CanInvite"));
                            if (!TextUtils.isEmpty(nodesObj.getString("Name"))) {
                                treeWrapperArrayList.add(treeWrapper);
                                memberList.add(nodesObj.getString("Name"));
                                memberlistMap.put(nodesObj.getString("Name"), nodesObj.getString("NodeId"));
                                membernamelistMap.put(nodesObj.getString("NodeId"), nodesObj.getString("Name"));
                            }
                        }
                        AppGlobalData.appGlobalData.getInstance().setMamberListMap(memberlistMap);
                        AppGlobalData.appGlobalData.getInstance().setMemberList(memberList);
                        AppGlobalData.appGlobalData.getInstance().setMembernamelistMap(membernamelistMap);
                        AppGlobalData.getInstance().setTreeWrapperArrayList(treeWrapperArrayList);
                    }
                }
                AppGlobalData.getInstance().getMemberList().add(0,context.getResources().getString(R.string.select_member));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return treeWrapperArrayList;
    }

    public ArrayList<GetEventWrapper> getEventList() {
        ArrayList<GetEventWrapper> getEventWrapperArrayList = new ArrayList<>();
        ArrayList<String> eventList = new ArrayList<>();
        HashMap<String, String> eventlistMap = new HashMap<>();
        try {
            JSONObject responseObj = new JSONObject(response);
            if (responseObj.getInt("StatusCode") == 1) {
                if (responseObj.has("Events")) {
                    JSONArray eventArr = responseObj.getJSONArray("Events");
                    for (int i = 0; i < eventArr.length(); i++) {
                        JSONObject eventsubObj = eventArr.getJSONObject(i);
                        GetEventWrapper getEventWrapper = new GetEventWrapper();
                        getEventWrapper.setDate(eventsubObj.getString("Date"));
                        getEventWrapper.setEventID(eventsubObj.getString("EventID"));
                        getEventWrapper.setLocation(eventsubObj.getString("Location"));
                        getEventWrapper.setTitle(eventsubObj.getString("Title"));
                        eventList.add(eventsubObj.getString("Title"));
                        getEventWrapperArrayList.add(getEventWrapper);
                        eventlistMap.put(eventsubObj.getString("Title"), eventsubObj.getString("EventID"));

                    }
                }
                AppGlobalData.getInstance().setEventlistMap(eventlistMap);
                eventList.add(context.getResources().getString(R.string.TagFamilyEvent));
                Collections.reverse(eventList);
                AppGlobalData.getInstance().setEventList(eventList);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return getEventWrapperArrayList;
    }

    public MemberInfo getMemberInfo() {
        MemberInfo memberInfo = new MemberInfo();
        try {
            ArrayList<String> fathersArrayList = new ArrayList<>();
            ArrayList<String> mothersArrayList = new ArrayList<>();
            ArrayList<String> spousesArrayList = new ArrayList<>();
            HashMap<String, String> fathersHashMap = new HashMap<>();
            HashMap<String, String> mothersHashMap = new HashMap<>();
            HashMap<String, String> spousesHashMap = new HashMap<>();
            JSONObject responseObj = new JSONObject(response);
            memberInfo.setNodeId(responseObj.getString("NodeId"));
            memberInfo.setFirstName(responseObj.getString("FirstName"));
            memberInfo.setLastName(responseObj.getString("LastName"));
            memberInfo.setEmailId(responseObj.getString("EmailId"));
            memberInfo.setDateOfBirth(responseObj.getString("DateOfBirth"));
            memberInfo.setBirthPlace(responseObj.getString("BirthPlace"));
            memberInfo.setIsAlive(responseObj.getString("IsAlive"));
            memberInfo.setDateOfDeath(responseObj.getString("DateOfDeath"));
            memberInfo.setDeathPlace(responseObj.getString("DeathPlace"));
            memberInfo.setDeathCause(responseObj.getString("DeathCause"));
            memberInfo.setSelectedFather(responseObj.getString("SelectedFather"));
            memberInfo.setSelectedMother(responseObj.getString("SelectedMother"));
            memberInfo.setSelectedSpouse(responseObj.getString("SelectedSpouse"));
            JSONArray fathersArray = responseObj.getJSONArray("Fathers");
            for (int f = 0; f < fathersArray.length(); f++) {
                JSONObject fatherObj = fathersArray.getJSONObject(f);
                fathersArrayList.add(fatherObj.getString("Name"));
                fathersHashMap.put(fatherObj.getString("Name"), fatherObj.getString("Id"));
            }
            JSONArray mothersrray = responseObj.getJSONArray("Mothers");
            for (int f = 0; f < mothersrray.length(); f++) {
                JSONObject fatherObj = mothersrray.getJSONObject(f);
                mothersArrayList.add(fatherObj.getString("Name"));
                mothersHashMap.put(fatherObj.getString("Name"), fatherObj.getString("Id"));
            }
            JSONArray spousesrray = responseObj.getJSONArray("Spouses");
            for (int f = 0; f < spousesrray.length(); f++) {
                JSONObject fatherObj = spousesrray.getJSONObject(f);
                spousesArrayList.add(fatherObj.getString("Name"));
                spousesHashMap.put(fatherObj.getString("Name"), fatherObj.getString("Id"));
            }
            memberInfo.setFathersArrayList(fathersArrayList);
            memberInfo.setFathersHashMap(fathersHashMap);
            memberInfo.setMothersArrayList(mothersArrayList);
            memberInfo.setMothersHashMap(mothersHashMap);
            memberInfo.setSpousesArrayList(spousesArrayList);
            memberInfo.setSpousesHashMap(spousesHashMap);

        } catch (Exception e) {
            e.printStackTrace();
        }


        return memberInfo;
    }

    public ArrayList<NotificationWrapper> getNotifications() {
        ArrayList<NotificationWrapper> notificationWrappers = new ArrayList<>();

        try {
            JSONObject responseObj = new JSONObject(response);
            if (responseObj.has("Notifications")) {
                JSONArray notificationsArr = responseObj.getJSONArray("Notifications");
                for (int i = 0; i < notificationsArr.length(); i++) {
                    NotificationWrapper notificationWrapper = new NotificationWrapper();
                    JSONObject notificationsubObj = notificationsArr.getJSONObject(i);
                    notificationWrapper.setID(notificationsubObj.getString("NodeId"));
                    notificationWrapper.setCreatedOn(notificationsubObj.getString("CreatedOn"));
                    notificationWrapper.setMediaID(notificationsubObj.getString("MediaID"));
                    notificationWrapper.setName(notificationsubObj.getString("Name"));
                    notificationWrapper.setProfilePic(notificationsubObj.getString("ProfilePic"));
                    notificationWrapper.setNotificationMessage(notificationsubObj.getString("NotificationMessage"));
                    notificationWrapper.setType(notificationsubObj.getString("Type"));
                    notificationWrappers.add(notificationWrapper);


                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return notificationWrappers;
    }

    public ArrayList<SearchMemberWrapper> getSearchMember() {
        ArrayList<SearchMemberWrapper> searchMemberWrappers = new ArrayList<>();
        try {
            JSONObject responseObj = new JSONObject(response);
            JSONArray membersJsonArray = responseObj.getJSONArray("Members");
            for (int i = 0; i < membersJsonArray.length(); i++) {
                JSONObject subObject = membersJsonArray.getJSONObject(i);
                SearchMemberWrapper searchMemberWrapper = new SearchMemberWrapper();
//                searchMemberWrapper.setGender(subObject.getString(""));
                searchMemberWrapper.setImage(subObject.getString("Image"));
                searchMemberWrapper.setName(subObject.getString("Name"));
                searchMemberWrapper.setNodeId(subObject.getString("NodeId"));
                searchMemberWrapper.setGender(subObject.getString("Gender"));
                searchMemberWrapper.setDate(subObject.getString("DateOfBirth"));
                searchMemberWrappers.add(searchMemberWrapper);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return searchMemberWrappers;
    }

    public ArrayList<InvitationsFriendsWrapper> getInvitationFriend() {
        ArrayList<InvitationsFriendsWrapper> invitationsFriendsWrappers = new ArrayList<>();

        try {
            JSONObject responseObj = new JSONObject(response);
            JSONArray invitees = responseObj.getJSONArray("Invitees");
            for (int i = 0; i < invitees.length(); i++) {
                JSONObject subObj = invitees.getJSONObject(i);
                InvitationsFriendsWrapper invitationsFriendsWrapper = new InvitationsFriendsWrapper();
                if (subObj.has("Brother")) {
                    invitationsFriendsWrapper.setBrother(subObj.getString("Brother"));
                }
                if (subObj.has("NodeId")) {
                    invitationsFriendsWrapper.setNodeId(subObj.getString("NodeId"));
                }
                if (subObj.has("Name")) {
                    invitationsFriendsWrapper.setName(subObj.getString("Name"));
                }
                if (subObj.has("Image")) {
                    invitationsFriendsWrapper.setImage(subObj.getString("Image"));
                }
                if (subObj.has("Gender")) {
                    invitationsFriendsWrapper.setGender(subObj.getString("Gender"));
                }
                if (subObj.has("Email")) {
                    invitationsFriendsWrapper.setEmail(subObj.getString("Email"));
                }
                if (subObj.has("IsInvited")) {
                    invitationsFriendsWrapper.setIsInvited(subObj.getString("IsInvited"));
                }
                if (subObj.has("ToNodeId")) {
                    invitationsFriendsWrapper.setToNodeId(subObj.getString("ToNodeId"));
                }
                invitationsFriendsWrappers.add(invitationsFriendsWrapper);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return invitationsFriendsWrappers;
    }
}
