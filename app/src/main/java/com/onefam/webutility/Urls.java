package com.onefam.webutility;

public class Urls {
    //    public static String base_Url = "http://108.60.209.75/onefam/";

    public static String base_Url = "https://www.onefam.com/";
    public static String registerUrl = base_Url + "apiRegister.ashx";
    public static String loginUrl = base_Url + "apiLogin.ashx";
    public static String resetpasswordUrl = base_Url + "apiForgotPassword.ashx";
    public static String addmemberUrl = base_Url + "apiAddMember.ashx";
    public static String webtreeviewUrl = base_Url + "apiWebViewTree.ashx";
    public static String treeviewUrl = base_Url + "viewtree2.aspx";
    public static String getprofilemediaUrl = base_Url + "apiGetProfileMedia.ashx";
    public static String profilemedia_basevideoThumbsUrl = base_Url + "source/upload/video/Thumbs/";
    public static String profilemedia_baseaudioThumbsUrl = base_Url + "source/upload/audio/Thumbs/";
    public static String profilemedia_basedocThumbsUrl = base_Url + "source/upload/othermedia/Thumbs/";
    public static String profilemedia_basephotoThumbsUrl = base_Url + "source/upload/album/Thumbs/";
    public static String profilemedia_basevideoUrl = base_Url + "source/upload/video/mp4/";
    public static String profilemedia_baseaudioUrl = base_Url + "source/upload/audio/";
    public static String profilemedia_basedocUrl = base_Url + "source/upload/othermedia/";
    public static String profilemedia_basephotoUrl = base_Url + "source/upload/album/";
    public static String updateprofileUrl = base_Url + "apiUpdateProfilePic.ashx";
    public static String ViewAllVideos = base_Url + "apiViewAllVideos.ashx";
    public static String ViewAllImages = base_Url + "apiViewAllImages.ashx";
    public static String ViewAllFiles = base_Url + "apiViewAllFiles.ashx";
    public static String getMemberlistUrl = base_Url + "apiGetMemberList.ashx";
    public static String createEventApi = base_Url + "apiAddEvent.ashx";
    public static String uploadProfileImage = base_Url + "apiUpdateProfilePic.ashx";
    public static String getFamalyViewTreeUrl = base_Url + "apiViewTree.ashx";
    public static String getProfiledataUrl = base_Url + "apiGetProfileData.ashx";
    public static String profileimagebasicUrl = base_Url + "source/upload/profile/";
    public static String deleteMember = base_Url + "apiDeleteMember.ashx";
    public static String editmemberapiMember = base_Url + "apiEditMember.ashx";
    public static String getMember = base_Url + "apiGetMember.ashx?NodeId=";
    public static String getEventListAPI = base_Url + "apiGetEvents.ashx";
    public static String uploadImageAPI = base_Url + "apiUploadImages.ashx";
    public static String uploadVideoAPI = base_Url + "apiUploadVideo.ashx";
    public static String getAlbumIDApi = base_Url + "apiGetAlbums.ashx?UserId=";
    public static String viewAllAudios = base_Url + "apiViewAllAudios.ashx";
    public static String uploadAudioAPI = base_Url + "apiUploadAudio.ashx";
    public static String sendinvitationAPI = base_Url + "apiSendInvitation.ashx";
    public static String sendInvitationApi = base_Url + "apiSendInvitation.ashx";
    public static String searchmemberAPI = base_Url + "apiSearch.ashx";
    public static String getPeddingInvitation = base_Url + "apiPendingInvitations.ashx";
    public static String declineInviteUrl = base_Url + "apiDeclineInvite.ashx";
    public static String acceptInvitation = base_Url + "apiAcceptInvite.ashx";
    public static String getpersondetailUrl = base_Url + "apiInviteDetail.ashx";
    public static String sendLanguageCode = base_Url + "apiUpdateToken.ashx";
    public static String mobileinviteUrl = base_Url + "apiMobileInvite.ashx";
    public static String mobileinvationsignupURL = base_Url + "apiAcceptInvitation.ashx";
    public static String privacyURL = base_Url + "privacy.html";
    public static String termsconditionsURL = base_Url + "terms-of-service.html";
    public static String registerInviteURL = base_Url + "apiRegisterInvitation.ashx";
    public static String deletemedia = base_Url + "apiDeleteMedia.ashx";
    public static String tagmember = base_Url + "apiSearch.ashx";



}
