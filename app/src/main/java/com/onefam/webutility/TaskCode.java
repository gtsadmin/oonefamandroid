package com.onefam.webutility;


public class TaskCode {
    public static int REGISTERCODE = 1;
    public static int LOGINCODE =2;
    public static int RESETPASSWORDCODE =3;
    public static int ADDMEMBERCODE =4;
    public static int TREEVIEWCODE=5;
    public static int GETPROFILEMEDIA=6;
    public static int ALLVIDEOSCODE =7;
    public static int ALLIMAGECODE = 8;
    public static int ALLFILESCODE= 9;
    public static int GETMEMBERLISTCODE =10;
    public static int CREATEEVENTCODE =11;
    public static int FAMILYVIEWTREECODE=12;
    public static int GETPROFILEDATACODE = 13;
    public static int DELETEMEMBERCODE=14;
    public static int EDITMEMBERAPI=15;
    public static int GETEVENTCODE = 16;
    public static int GETALBUMAPI=17;
    public static int GETALLAUDIO=18;
    public static int GETMEMBERCODE=19;
    public static int SENDINVITATION=20;
    public static int NOTIFICATIONCODE =21;
    public static int SEARCHCODE =22;
    public static int SENDLANGUAGECODE=23;
    public static int GETINVITATIONS=24;
    public static int GETPEDDINGINVITATIONS=25;
    public static int ACCEPTREQUEST=26;
    public static int PERSONREQUEST=27;
    public static int DECLINEINVITE=28;
    public static int REGISTERINVITE=29;
    public static int DELETEMEDIA=30;
    public static int TAGMEMBER=31;

}
