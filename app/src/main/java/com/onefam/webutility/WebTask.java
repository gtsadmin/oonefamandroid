package com.onefam.webutility;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.onefam.R;

import org.apache.http.NameValuePair;
import org.apache.http.util.TextUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;


public class WebTask extends AsyncTask<String, String, String> {
    String url;
    List<NameValuePair> param;
    WebTaskComplete webCompleteTask;
    int taskcode;
    Context context;
    ProgressDialog progressDialog;
    boolean showProgress = true;

    public WebTask(Context context, String url, List<NameValuePair> param, WebTaskComplete webCompleteTask, int taskcode) {
        this.url = url;
        this.param = param;
        this.webCompleteTask = webCompleteTask;
        this.taskcode = taskcode;
        this.context = context;
    }

    public WebTask(Context context, String url, List<NameValuePair> param, WebTaskComplete webCompleteTask, int taskcode, boolean showProgress) {
        this.url = url;
        this.param = param;
        this.webCompleteTask = webCompleteTask;
        this.taskcode = taskcode;
        this.context = context;
        this.showProgress = showProgress;

    }


    @Override
    protected String doInBackground(String... params) {
        URL url1;
        String response = "";
        try {
            url1 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
            conn.setReadTimeout(60000);
            conn.setConnectTimeout(60000);
//            conn.
            if (param != null) {
                conn.setRequestMethod("POST");
            } else {
                conn.setRequestMethod("GET");
            }
            conn.setDoInput(true);
            conn.setDoOutput(true);
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            if (param != null) {
                writer.write(getQuery(param));
            }
            writer.flush();
            writer.close();
            os.close();
            int responseCode = conn.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (showProgress) {
            progressDialog = ProgressDialog.show(context, "", context.getResources().getString(R.string.Pleasewait));
        }
    }

    @Override
    protected void onPostExecute(String response) {
        super.onPostExecute(response);
        if(progressDialog!=null) {
            progressDialog.dismiss();
        }
        if (!TextUtils.isEmpty(response)) {
            webCompleteTask.onComplete(response, taskcode);
        } else {
            Toast.makeText(context, "Please try again", Toast.LENGTH_LONG).show();
        }
    }

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (NameValuePair pair : params) {
            if (first)
                first = false;
            else
                result.append("&");
            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }
        return result.toString();
    }


}
