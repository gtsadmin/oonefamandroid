package com.onefam.applications;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.crittercism.app.Crittercism;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.onefam.R;
import com.onefam.activitys.AnalyticsTrackers;
import com.onefam.datacontroller.Constants;

import java.io.File;
import java.util.Locale;


public class MyApplication extends Application {
    public static MyApplication myApplication = null;
    public static ImageLoader loader;

    public ImageLoaderConfiguration config;
    public static SharedPreferences sp;
    protected File extStorageAppBasePath;
    //    public static String GOOGLE_SENDER_ID = "162341587422";
    public static String GOOGLE_SENDER_ID = "973989754647";
    protected File extStorageAppCachePath;


    boolean load_profile = true;

    @Override
    public void onCreate() {
        super.onCreate();
        myApplication = new MyApplication();
        loader = ImageLoader.getInstance();
        loader.init(ImageLoaderConfiguration.createDefault(this));
        sp = getSharedPreferences("OneFam", 0);
        //  setImageLoaderConfig();
        AnalyticsTrackers.initialize(this);
        AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);
        Crittercism.initialize(getApplicationContext(), "5699f1bc6c33dc0f00f11616");

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            // Retrieve the base path for the application in the external storage
            File externalStorageDir = Environment.getExternalStorageDirectory();

            if (externalStorageDir != null) {
                // {SD_PATH}/Android/data/com.devahead.androidwebviewcacheonsd
                extStorageAppBasePath = new File(externalStorageDir.getAbsolutePath() +
                        File.separator + "Android" + File.separator + "data" +
                        File.separator + getPackageName());
            }

            if (extStorageAppBasePath != null) {
                // {SD_PATH}/Android/data/com.devahead.androidwebviewcacheonsd/cache
                extStorageAppCachePath = new File(extStorageAppBasePath.getAbsolutePath() +
                        File.separator + "cache");

                boolean isCachePathAvailable = true;

                if (!extStorageAppCachePath.exists()) {
                    // Create the cache path on the external storage
                    isCachePathAvailable = extStorageAppCachePath.mkdirs();
                }

                if (!isCachePathAvailable) {
                    // Unable to create the cache path
                    extStorageAppCachePath = null;
                }
            }
        }
    }

    public synchronized Tracker getGoogleAnalyticsTracker() {
        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
        return analyticsTrackers.get(AnalyticsTrackers.Target.APP);
    }

    /***
     * Tracking screen view
     *
     * @param screenName screen name to be displayed on GA dashboard
     */
    public void trackScreenView(String screenName) {
        Tracker t = getGoogleAnalyticsTracker();

        // Set screen name.
        t.setScreenName(screenName);

        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        GoogleAnalytics.getInstance(this).dispatchLocalHits();
    }

    /***
     * Tracking exception
     *
     * @param e exception to be tracked
     */
    public void trackException(Exception e) {
        if (e != null) {
            Tracker t = getGoogleAnalyticsTracker();

            t.send(new HitBuilders.ExceptionBuilder()
                    .setDescription(
                            new StandardExceptionParser(this, null)
                                    .getDescription(Thread.currentThread().getName(), e))
                    .setFatal(false)
                    .build()
            );
        }
    }

    /***
     * Tracking event
     *
     * @param category event category
     * @param action   action of the event
     * @param label    label
     */
    public void trackEvent(String category, String action, String label) {
        Tracker t = getGoogleAnalyticsTracker();

        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).build());
    }


    public static MyApplication getInstance() {
        if (myApplication == null) {
            myApplication = new MyApplication();
        }
        return myApplication;
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public void produceAnimation(View v) {
        Animation alphaDown = new AlphaAnimation(1.0f, 0.3f);
        Animation alphaUp = new AlphaAnimation(0.3f, 1.0f);
        alphaDown.setDuration(1000);
        alphaUp.setDuration(500);
        alphaDown.setFillAfter(true);
        alphaUp.setFillAfter(true);
        v.startAnimation(alphaUp);
    }

    public static DisplayImageOptions options = new DisplayImageOptions.Builder()
            .showImageForEmptyUri(0).cacheInMemory(true)
            .showImageOnFail(0).cacheOnDisc(true).considerExifParams(true)
            .imageScaleType(ImageScaleType.EXACTLY)
            .bitmapConfig(Bitmap.Config.RGB_565).build();
    public static DisplayImageOptions option2 = new DisplayImageOptions.Builder()
            .showImageForEmptyUri(0).cacheInMemory(true)
            .showImageOnFail(0).cacheOnDisc(true).considerExifParams(true)
            .imageScaleType(ImageScaleType.EXACTLY)
            .bitmapConfig(Bitmap.Config.RGB_565).displayer(new RoundedBitmapDisplayer(1000)).build();
    public static DisplayImageOptions option3 = new DisplayImageOptions.Builder().displayer(new RoundedBitmapDisplayer(100))
            .showImageForEmptyUri(0).cacheInMemory(true)
            .showImageOnFail(0).cacheOnDisc(true).considerExifParams(true)
            .imageScaleType(ImageScaleType.EXACTLY)
            .bitmapConfig(Bitmap.Config.RGB_565).build();
    public static DisplayImageOptions option4 = new DisplayImageOptions.Builder()
            .showImageForEmptyUri(0).cacheInMemory(true)
            .showImageOnFail(R.drawable.icon).cacheOnDisc(true).considerExifParams(true)
            .imageScaleType(ImageScaleType.EXACTLY)
            .bitmapConfig(Bitmap.Config.RGB_565).displayer(new RoundedBitmapDisplayer(1000)).build();

    public void setImageLoaderConfig() {

        config = new ImageLoaderConfiguration.Builder(getApplicationContext())

                .memoryCacheSize(20 * 1024 * 1024)
                // 20 Mb
                .memoryCache(new LruMemoryCache(20 * 1024 * 1024))
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .tasksProcessingOrder(QueueProcessingType.LIFO).build();

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config);
    }

    public static boolean isInternetWorking(Activity activity) {
        if (!isConnectingToInternet(activity)) {

            Toast.makeText(activity,
                    activity.getResources().getString(R.string.Internetconnectionissloworoff),
                    Toast.LENGTH_LONG).show();
            return false;

        }
        return true;
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;

    }

    public static void saveUserId(String userid) {
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(Constants.USERID, userid);
        edit.commit();

    }

    public static String getUserID() {
        String userid = sp.getString(Constants.USERID, "");
        return userid;
    }

    public static void saveNotification(boolean key) {
        SharedPreferences.Editor edit = sp.edit();
        edit.putBoolean(Constants.PUSHKEY, key);
        edit.commit();

    }


    public static Boolean getNotification() {
        boolean pushON = sp.getBoolean(Constants.PUSHKEY, true);
        return pushON;
    }

    public static void saveLanguagecode(String userid) {
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(Constants.LANGUAGECODE, userid);
        edit.commit();

    }

    public static String getLanguagecode() {
        String userid = sp.getString(Constants.LANGUAGECODE, "en");
        return userid;
    }

    public static void saveTokenID(String userid) {
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(Constants.GCMTOKEN, userid);
        edit.commit();

    }

    public static String getTokenID() {
        String userid = sp.getString(Constants.GCMTOKEN, "");
        return userid;
    }

    public static void saveEmailId(String userid) {
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(Constants.EMAILID, userid);
        edit.commit();

    }

    public static String getEmailID() {
        String userid = sp.getString(Constants.EMAILID, "");
        return userid;
    }

    public static void savePassword(String userid) {
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(Constants.PASSWORD, userid);
        edit.commit();

    }

    public static String getPassword() {
        String userid = sp.getString(Constants.PASSWORD, "");
        return userid;
    }


    public static void saveUserFirstName(String userid) {
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(Constants.FIRSTNAME, userid);
        edit.commit();

    }

    public static String getUserFirstName() {
        String userid = sp.getString(Constants.FIRSTNAME, "");
        return userid;
    }

    public static void setViewFamilyTreeId(String viewfamilyid) {
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(Constants.VIEW_FAMILY_ID, viewfamilyid);
        edit.commit();

    }

    public static String getViewFamilyTreeId() {
        String userid = sp.getString(Constants.VIEW_FAMILY_ID, "");
        return userid;
    }


    public void setLanguage(String languagecode) {
        Locale locale = new Locale(languagecode);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
    }

    public void hideSoftKeyBoard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = activity.getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }


    @Override
    public File getCacheDir() {
        // NOTE: this method is used in Android 2.2 and higher

        if (extStorageAppCachePath != null) {
            // Use the external storage for the cache
            return extStorageAppCachePath;
        } else {
            // /data/data/com.devahead.androidwebviewcacheonsd/cache
            return super.getCacheDir();
        }
    }

    public boolean isLoad_profile() {
        return load_profile;
    }

    public void setLoad_profile(boolean load_profile) {
        this.load_profile = load_profile;
    }


    public String getCreateMemoryNodID() {
        return createMemoryNodID;
    }

    public void setCreateMemoryNodID(String createMemoryNodID) {
        this.createMemoryNodID = createMemoryNodID;
    }

    String createMemoryNodID = "";


    public static void showUnreadNotificationStatus(boolean key) {
        SharedPreferences.Editor edit = sp.edit();
        edit.putBoolean(Constants.UNREADNOTIFICATION, key);
        edit.commit();

    }

    public static Boolean getShowUnreadNotificationStatus() {
        boolean pushON = sp.getBoolean(Constants.UNREADNOTIFICATION, false);
        return pushON;
    }

    public static void hideKeyBoard(Activity mActivity) {
        if (mActivity == null)
            return;
        else {
            InputMethodManager inputManager = (InputMethodManager) mActivity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);

            // check if no view has focus:
            View v = mActivity.getCurrentFocus();
            if (v == null)
                return;

            inputManager.hideSoftInputFromWindow(v.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

}
